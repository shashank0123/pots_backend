<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::get('dashboard', 'DashboardController@index');

    CRUD::resource('restaurants', 'RestaurantCrudController');
    CRUD::resource('customers', 'CustomerCrudController');
    CRUD::resource('menu-items', 'MenuItemCrudController');
    CRUD::resource('orders', 'OrderCrudController');
    CRUD::resource('todays-orders', 'TodayOrderCrudController');
    CRUD::resource('offline-orders', 'OfflineOrderCrudController');
    CRUD::resource('order-history', 'OrderHistoryCrudController');
    CRUD::resource('cancelled-orders', 'CancelledOrderCrudController');
    CRUD::resource('earnings', 'EarningCrudController');
    CRUD::resource('toppings', 'ToppingCrudController');
    CRUD::resource('delivery-boys', 'DeliveryBoyCrudController');
    CRUD::resource('cities', 'CityCrudController');
    CRUD::resource('home-banners', 'HomeBannerCrudController');
    CRUD::resource('offer-banners', 'OfferBannerCrudController');
    CRUD::resource('cuisines', 'CuisineCrudController');
    CRUD::resource('coupons', 'CouponCrudController');

}); // this should be the absolute last line of this file