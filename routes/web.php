<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/reset', function(){
    session()->flush();

    return redirect('/');
});

Auth::routes();

Route::post('/password/phone', 'Auth\ForgotPasswordController@phone')->name('password.phone');
Route::get('/password/change', 'Auth\ResetPasswordController@change')->name('password.change');
Route::post('/password/update', 'Auth\ResetPasswordController@update')->name('password.update');

Route::get('/register/success', 'Auth\RegisterController@success');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/restaurants/request', 'RestaurantRequestController@store');
Route::post('/restaurants/request/bulk', 'RestaurantRequestController@bulk');
Route::get('/restaurants/explore', 'RestaurantsController@index');
Route::get('/restaurants/{city}/{restaurantSlug}', 'RestaurantsController@show');

Route::post('/cart/add', 'CartController@store');
Route::post('/cart/custom/add', 'CartController@storeCustom');
Route::post('/cart/remove/{itemId}', 'CartController@destroy');
Route::post('/cart/update/{itemId}', 'CartController@update');
Route::get('/cart', 'CartController@index');

Route::get('/search', 'SearchController@index');

Route::get('/checkout', 'CheckoutController@index');

Route::post('/orders', 'OrdersController@store');
Route::get('/orders/{order}', 'OrdersController@show');
Route::get('/orders/{order}/invoice', 'OrdersController@invoice');
Route::get('/orders/{order}/confirm', 'OrdersController@confirm');
Route::get('/orders/{order}/ready', 'OrdersController@ready');
Route::get('/orders/{order}/picked', 'OrdersController@picked');
Route::get('/orders/{order}/delivered', 'OrdersController@delivered');
Route::get('/orders/{order}/cancel', 'OrdersController@cancel');
Route::get('/thankyou', 'ThankyouController@index');
Route::get('/pay', 'PaymentsController@pay');

Route::post('/payments/response/', 'PaymentsController@response');
Route::post('/payments/cancel/', 'PaymentsController@cancel');

Route::post('/addresses', 'AddressController@store');
Route::get('/addresses/{address}/delete', 'AddressController@destroy');
Route::post('/addresses/{address}/update', 'AddressController@update');

Route::post('user/location', 'Api\UserLocationController@store');

Route::get('/my-account', 'AccountController@profile');
Route::get('/my-account/orders', 'AccountController@orders');
Route::get('/my-account/offers', 'AccountController@offers');
Route::get('/my-account/payments', 'AccountController@payments');
Route::get('/my-account/bookmarks', 'AccountController@bookmarks');
Route::get('/my-account/addresses', 'AccountController@addresses');

Route::post('/account/update', 'AccountController@updateProfile');
Route::post('/account/update/password', 'AccountController@updatePassword');
Route::post('/account/phone/update', 'AccountController@updatePhone');

Route::get('/bookmarks/{restaurant}', 'BookmarkController@store');
Route::get('/bookmarks/{restaurant}/remove', 'BookmarkController@destroy');

Route::get('/coupons', 'CouponsController@index');
Route::post('/coupons/apply', 'CouponsController@apply');
Route::get('/coupons/remove', 'CouponsController@destroy');
Route::post('/coupons/apply/foodoor', 'CouponsController@applyFoodoor');

Route::post('/cart/clear', 'CartController@clear');


Route::get('/company/about', 'CompanyController@about');
Route::get('/company/contact', 'CompanyController@contact');
Route::get('/company/careers', 'CompanyController@careers');
Route::get('/company/team', 'CompanyController@team');
Route::get('/company/terms', 'CompanyController@terms');
Route::get('/company/privacy-policy', 'CompanyController@privacy');

Route::post('/contact', 'ContactController@store');
