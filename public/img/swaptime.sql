-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 08, 2018 at 04:58 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swaptime`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adm_id` bigint(20) UNSIGNED NOT NULL,
  `adm_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adm_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adm_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `adm_confirm_psw` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `adm_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adm_id`, `adm_username`, `adm_phone`, `adm_email`, `adm_password`, `adm_confirm_psw`, `adm_date`) VALUES
(1, 'admin', '9404050990', 'admin@admin.com', 'admin', 'admin', '2018-09-01 12:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `admin_delivery_charges`
--

CREATE TABLE `admin_delivery_charges` (
  `adc_id` bigint(20) UNSIGNED NOT NULL,
  `adc_amount` int(50) DEFAULT '0',
  `adc_charges` int(50) DEFAULT '0',
  `adc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_delivery_charges`
--

INSERT INTO `admin_delivery_charges` (`adc_id`, `adc_amount`, `adc_charges`, `adc_date`) VALUES
(1, 300, 30, '2018-09-04 15:11:34'),
(2, 600, 30, '2018-09-04 15:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `admin_delivery_time`
--

CREATE TABLE `admin_delivery_time` (
  `adt_id` bigint(20) UNSIGNED NOT NULL,
  `adt_time` int(20) DEFAULT '1',
  `adt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_delivery_time`
--

INSERT INTO `admin_delivery_time` (`adt_id`, `adt_time`, `adt_date`) VALUES
(1, 0, '2018-09-04 11:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `banquet`
--

CREATE TABLE `banquet` (
  `bq_id` bigint(20) UNSIGNED NOT NULL,
  `bq_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bq_contact_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bq_phone` mediumtext COLLATE utf8_unicode_ci,
  `bq_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bq_photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bq_price` int(20) NOT NULL DEFAULT '0',
  `bq_min_capacity` int(20) NOT NULL DEFAULT '0',
  `bq_max_capacity` int(20) NOT NULL DEFAULT '0',
  `bq_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bq_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bq_pincode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bq_term_condition` text COLLATE utf8_unicode_ci NOT NULL,
  `bq_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bq_confirm_psw` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bq_status` int(11) NOT NULL DEFAULT '0',
  `bq_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banquet`
--

INSERT INTO `banquet` (`bq_id`, `bq_name`, `bq_contact_person`, `bq_phone`, `bq_email`, `bq_photo`, `bq_price`, `bq_min_capacity`, `bq_max_capacity`, `bq_address`, `bq_city`, `bq_pincode`, `bq_term_condition`, `bq_password`, `bq_confirm_psw`, `bq_status`, `bq_date`) VALUES
(6, 'Swap Time', 'Swap Time ', '', 'swaptimeb@gmail.com', 'uploads/banquet/profile/bq_20180915111007_6.jpg', 25000, 250, 500, 'Shanti Nagar Amba Mata Madir Near Nagar Highway Road', 'Pune', '411020', '<p><b><u></u><u>हॉलच्या वापरासाठी नियम व विनियम </u><u></u></b><b><u> </u></b><u>\r\n\r\n</u></p><p>कोणत्याही\r\nकार्याचे सुलभतेने पालन करण्यासाठी, होस्ट आणि मॅनेजमेंटने एकत्रितपणे कार्य केले पाहिजे. आम्ही प्रत्येक बाबतीत आपल्याबरोबर\r\nसहकार्य करण्यासाठी नेहमीच तयार असतो म्हणून आपल्याकडे कोणत्याही विशिष्ट गरजा किंवा गरजांविषयी चर्चा करण्यासाठी अजिबात संकोच करू नका. </p>\r\n\r\n<p><b>ताबा आणि परतावा</b> <br>\r\nआपण लग्न आणि\r\nरिसेप्शनसाठी हॉल बुक केले असल्यास, लग्नाच्या सोहळ्याच्या आधीच्या दिवशी संध्याकाळी 7 वाजता तुम्हाला ताब्यात घेता येते आणि पुढील दिवशी 9 वाजता हॉल रिक्त केले गेले पाहिजे. हॉल रिक्त क </p>\r\n\r\nजर आपण लग्नासाठी हॉल बुक केले असेल तर लग्नाच्या\r\nसोहळ्यापूर्वी सायंकाळी 7 वाजता ताब्यात घेता येईल आणि पुढील दिवशी संध्याकाळी 5 वाजता रणे\r\n\r\n\r\n\r\n<br><p>आवश्यक आहे. </p>\r\n\r\n<p>जर आपण फक्त\r\nरिसेप्शनसाठी हॉल बुक केले असल्यास, रिसेप्शनच्या दिवशी दुपारी 6 वाजता आपण ताब्यात घ्याल आणि त्याच दिवशी\r\n9 वाजता हॉल रिक्त करणे आवश्यक आहे. </p>\r\n\r\n<p>जर आपण हॉलच्या\r\nबाहेर या वेळेच्या बाहेर किंवा दीर्घ कालावधीसाठी बुक करणे आवडत असेल (उदाहरणार्थ आपण रिसेप्शन 9 वाजता पुढे चालवू इच्छित असल्यास) कृपया आम्हाला कळवा आणि आम्ही आपल्या\r\nविनंतीचा प्रयत्न व सामावून घेण्यास आनंदी आहोत. <br></p>\r\n\r\n<p><b>बुकिंग आणि पेमेंट</b> <br>\r\nबुकिंगच्या वेळी\r\nहॉल भाड्याने देण्याची आवश्यकता आहे. पूर्ण भरणा केल्याशिवाय बुकिंगची पुष्टी केली नाही. बुक केलेल्या एखाद्या हॉल ज्यासाठी अद्याप दिलेला नाही, तो जर दुस-या पक्षाने बुकिंग धारण केलेल्या व्यक्तीच्या अगोदर पैसे दिले\r\nअसतील तर दुसर्या पक्षाला दिले जाऊ शकते. </p>\r\n\r\n<p><b>रद्दीकरण आणि बदल\r\nतारीख</b> <br>\r\nएकदा हॉल बुक\r\nकेलेले आणि त्यासाठी पैसे दिले गेले की, तो रद्द केला जाऊ शकत नाही. </p>\r\n\r\n<p>जर आपण बुकिंग\r\nरद्द करू इच्छित असाल तर इतर पक्षांकरिता ती तारीख / हॉल उपलब्ध करून देऊ. त्याच तारखेस / हॉलसाठी पर्यायी बुकिंग\r\nमिळविली तर आपल्या 75% बुकिंग पेमेंट परत मिळवू. आम्ही वैकल्पिक बुकिंग मिळविण्यास अक्षम\r\nअसल्यास, आपल्या बुकिंगची पूर्ण रक्कम जप्त केली\r\nजाईल. </p>\r\n\r\n<p>तारखेच्या तारखेस\r\nरद्दीकरण मानले जाईल आणि त्याच नियम लागू होतील. बुकिंगच्या वेळी एक नवीन तारखेची बुकिंग पूर्ण भरून द्यावी\r\nलागेल. </p>\r\n\r\n<p><b>सामान्य</b> <br>\r\nएखाद्या कामात\r\nवापरल्या जाणार्या वस्तू आणि साहित्याचा भार कमी करण्यासाठी आणि न सोडण्यासाठी आपण हॉलच्या कर्मचार्यांची सेवा वापरू शकणार नाही. </p>\r\n\r\n<p>&nbsp;हॉलची मालकी असलेली कोणतीही मालमत्ता आपण येथून\r\nकाढू शकत नाही. </p>\r\n\r\n<p>10.30 वाजता किंवा कायद्यानुसार लागू होणारी\r\nकोणतीही वाद्य वाजविण्यास कोणतेही वाद्य वाजविले जाणार नाही. </p>\r\n\r\n<p>इलेक्ट्रिक\r\nडिव्हाइसेस आणि विद्युत जोडणी आणि फिटिंग हलविल्या जाऊ शकत नाहीत किंवा त्यात बदल\r\nकेले जाऊ शकत नाहीत अशा प्रकारच्या\r\nकृतीतून उद्भवणारे कोणतेही नुकसान व्यवस्थापन विधेयकानुसारच बिलमध्ये जोडले जातील. </p>\r\n\r\n<p>व्यवस्थापनांच्या\r\nविवेकबुद्धीमुळे भिंती, खिडक्या, फर्निचर, फिक्स्चर्स आणि\r\nफिटिंग्जचा कोणताही तोटा बिलमध्ये जोडला जाईल. </p>\r\n\r\n<p>हॉल मध्ये आयोजित\r\nकेलेल्या प्रत्येक विवाह विधिवत कायद्याने ठरवून दिलेल्या नियमांनुसार असणे आवश्यक\r\nआहे. हॉलची नोंदणी करणा-या व्यक्तीची आणि\r\nविवाह परवानाची एक प्रत ही कार्याच्या तारखेच्या आधी व्यवस्थापकास देणे आवश्यक\r\nआहे. </p>\r\n\r\n<p>रात्रभर राहणा-या\r\nअतिथींसाठी 30 पेक्षा जास्त बेडची आवश्यकता असल्यास, रु .10 च्या दराने अतिरिक्त बेड दिले जाईल. 40 / - प्रत्येक किमान 8 दिवस आधी आगाऊ\r\nअतिरिक्त बेडची माहिती आम्हाला देण्यात यावी. </p>\r\n\r\n<p>आंघोळीसाठी गरम\r\nपाणी सकाळी 6 ते 8.30 च्या दरम्यान पुरविले जाते. कृपया गरम पाण्याच्या गीझर आणि\r\nबॉयलर स्विचसह छेडछाड करू नये. </p>\r\n\r\n<p>हॉलमध्ये बाहेरील\r\nकोणतेही अन्न आणि पेय किंवा कॅटरिंगची अनुमती नाही सर्व खाद्यपदार्थ आणि पेय पदार्थ आमच्या कॅटररद्वारे\r\nबनवावेत. हॉलमध्ये शाकाहारी पदार्थ नसतात. </p>\r\n\r\n<p>रोखाने भरलेली\r\nकोणतीही बिले संबंधित व्यवस्थापक आणि योग्य रितीने प्राप्त झालेली अधिकृत पावती </p>\r\n\r\n<p>हॉलच्या बाहेरील\r\nभिंतीवर आवश्यक असलेल्या कोणत्याही प्रकाश सजावटसाठी व्यवस्थापकाने आधीपासून सल्ला\r\nघेतला पाहिजे. </p>\r\n\r\n<p><b>सुरक्षितता आणि\r\nसुरक्षा</b> <br>\r\nआपण हॉलमध्ये\r\nसुरक्षित ठेवण्यात खूप काळजी घेत असताना हॉलच्या कर्मचा-यांना याची जाणीव होणे अशक्य आहे की फंक्शनमध्ये खरा\r\nअतिथी कोण आहे आणि कोण नाही. दिलेल्या सर्व लॉकर्समध्ये तुमचे सर्व सामान, मौल्यवान आणि रोख सुरक्षितपणे लॉक केले\r\nपाहिजे. रोख आणि मौल्यवान वस्तूंची चोरी किंवा\r\nचुकीच्या स्थानांपासून उद्भवणारी कोणतीही हानी किंवा क्षति यासाठी व्यवस्थापन\r\nजबाबदार नाही. </p>\r\n\r\n<p>आपण फंक्शन\r\nदरम्यान उपलब्ध केलेल्या खाजगी खोल्यांसाठी रूम लॉक प्रदान करण्यासाठी आणि सुरक्षित नसताना खोली सुरक्षितपणे लॉक\r\nकरण्यासाठी जबाबदार आहात. </p>\r\n\r\n<p><b>वाद निराकरण</b> <br>\r\nआम्ही नेहमी सर्व\r\nवाद-दोस्तांचे आणि मैत्रिणींचे प्रयत्न करतो. ज्याप्रसंगी आम्ही असे करण्यास असमर्थ आहे, त्या सर्व विवाद ठराव पुणे न्यायालयाच्या\r\nअधिकारक</p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n&lt;!--\r\n /* Font Definitions */\r\n @font-face\r\n	{font-family:Mangal;\r\n	panose-1:2 4 5 3 5 2 3 3 2 2;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:roman;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:32771 0 0 0 1 0;}\r\n@font-face\r\n	{font-family:Mangal;\r\n	panose-1:2 4 5 3 5 2 3 3 2 2;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:roman;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:32771 0 0 0 1 0;}\r\n@font-face\r\n	{font-family:Calibri;\r\n	panose-1:2 15 5 2 2 2 4 3 2 4;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:swiss;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:-536859905 -1073732485 9 0 511 0;}\r\n@font-face\r\n	{font-family:\"Arial Black\";\r\n	panose-1:2 11 10 4 2 1 2 2 2 4;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:swiss;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:-1610612049 1073772795 0 0 159 0;}\r\n /* Style Definitions */\r\n p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n	{mso-style-unhide:no;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:\"\";\r\n	margin-top:0cm;\r\n	margin-right:0cm;\r\n	margin-bottom:10.0pt;\r\n	margin-left:0cm;\r\n	line-height:115%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	mso-bidi-font-size:10.0pt;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Mangal;\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n.MsoChpDefault\r\n	{mso-style-type:export-only;\r\n	mso-default-props:yes;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Mangal;\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n.MsoPapDefault\r\n	{mso-style-type:export-only;\r\n	margin-bottom:10.0pt;\r\n	line-height:115%;}\r\n@page WordSection1\r\n	{size:612.0pt 792.0pt;\r\n	margin:72.0pt 72.0pt 72.0pt 72.0pt;\r\n	mso-header-margin:36.0pt;\r\n	mso-footer-margin:36.0pt;\r\n	mso-paper-source:0;}\r\ndiv.WordSection1\r\n	{page:WordSection1;}\r\n--&gt;\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p>Amenities:</p>\r\n\r\n\r\n \r\n  \r\n  <p><img alt=\"Description httpswaptimeoncomuploadsbanquetamenityicon-bikepng\" width=\"56\" height=\"40\"></p>\r\n  \r\n  \r\n  <p>2 Wheeler</p>\r\n  \r\n  \r\n  <p>100-500</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpswaptimeoncomuploadsbanquetamenityicon-carpng\" width=\"56\" height=\"45\"></p>\r\n  \r\n  \r\n  <p>4 Wheeler</p>\r\n  \r\n  \r\n  <p>100-200</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpswaptimeoncomuploadsbanquetamenityicon-mobpng\" width=\"63\" height=\"46\"></p>\r\n  \r\n  \r\n  <p>Charger Point </p>\r\n  \r\n  \r\n  <p>10</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpswaptimeoncomuploadsbanquetamenityicon-tvpng\" width=\"63\" height=\"48\"></p>\r\n  \r\n  \r\n  <p>LED Television</p>\r\n  \r\n  \r\n  <p>1</p>\r\n  \r\n  \r\n  <p>Paid</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"69\" height=\"46\"></p>\r\n  \r\n  \r\n  <p>Power Backup</p>\r\n  \r\n  \r\n  <p>1</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"69\" height=\"47\"></p>\r\n  \r\n  \r\n  <p>Rooms &amp; Bath</p>\r\n  \r\n  \r\n  <p>4-8</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Image result for Kitchen Rooms Marriage Hall\" width=\"69\" height=\"47\"></p>\r\n  \r\n  \r\n  <p>Kitchen Rooms </p>\r\n  \r\n  \r\n  <p>1</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Image result for marriage dining hall cartoon\" width=\"69\" height=\"53\"></p>\r\n  \r\n  \r\n  <p>Dining Table Hall</p>\r\n  \r\n  \r\n  <p>36</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>265 Cap</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpimgsdown1mobilecomgroup3M0076C0S34UR1Tu5RWAX6KkAAvegTfgSY0397png\" width=\"69\" height=\"55\"></p>\r\n  \r\n  \r\n  <p>Marriage Stage Only</p>\r\n  \r\n  \r\n  <p>2</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Image result for marriage hall cartoon\" width=\"69\" height=\"55\"></p>\r\n  \r\n  \r\n  <p>Entrance Gate</p>\r\n  \r\n  \r\n  <p>2</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"69\" height=\"62\"></p>\r\n  \r\n  \r\n  <p>Display Racket</p>\r\n  \r\n  \r\n  <p>1</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"69\" height=\"64\"></p>\r\n  \r\n  \r\n  <p>Grooming Chairs</p>\r\n  \r\n  \r\n  <p>4</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>Sofa Chairs</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpwww10chairsplimageszdjeciaduzeelsol4jpg\" width=\"67\" height=\"45\"></p>\r\n  \r\n  \r\n  <p>Regular Chairs </p>\r\n  \r\n  \r\n  <p>1000</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"63\" height=\"54\"></p>\r\n  \r\n  \r\n  <p>Bath &amp; Toilets</p>\r\n  \r\n  \r\n  <p>4</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"80\" height=\"51\"></p>\r\n  \r\n  \r\n  <p>Mice &amp; Sound</p>\r\n  \r\n  \r\n  <p>3</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpsencrypted-tbn0gstaticcomimagesqtbnANd9GcTm5ecn9-L1PnnbZZgAiNiD7NDeejMNAI6W7Z8lIclbX9WfT8ZoZA\" width=\"74\" height=\"51\"></p>\r\n  \r\n  \r\n  <p>Lawns</p>\r\n  \r\n  \r\n  <p>1</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  <p>&nbsp;</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Image result for cctv camera\" width=\"74\" height=\"56\"></p>\r\n  \r\n  \r\n  <p>C.C.TV Camera</p>\r\n  \r\n  \r\n  <p>Yes</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"74\" height=\"58\"></p>\r\n  \r\n  \r\n  <p>Diner Set</p>\r\n  \r\n  \r\n  <p>2000</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description Related image\" width=\"70\" height=\"52\"></p>\r\n  \r\n  \r\n  <p>Buffet Catering Set</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n  \r\n  <p>Paid </p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n \r\n  \r\n  <p><img alt=\"Description httpsencrypted-tbn0gstaticcomimagesqtbnANd9GcT1El-LcaqS1Sifr7Wl0HuAJ_bt8fZfvEcyn18eZuNZJYmL4PD9\" width=\"90\" height=\"59\"></p>\r\n  \r\n  \r\n  <p>Sleeping Mat</p>\r\n  \r\n  \r\n  <p>8</p>\r\n  \r\n  \r\n  <p>Free</p>\r\n  \r\n  \r\n  <p>&nbsp;</p>\r\n  \r\n \r\n\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>', 'swap@123', 'swap@123', 1, '2018-07-24 05:15:44'),
(7, 'Radhakrishna Garden & Banquet Hall', 'Bipin Dimble', '7447402400,8796614402,7038388161,9097120120,8605478460,8999756077', 'radhakrishnagarden2016@gmail.com', 'uploads/banquet/profile/bq_20180920105826_.JPG', 80000, 500, 2000, 'Near Khed-Shivapur Toll Plaza, Pune ', 'Pune', '412205', '', '3jV33442', '3jV33442', 1, '2018-09-20 10:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `banquet_photos`
--

CREATE TABLE `banquet_photos` (
  `bq_p_id` bigint(20) UNSIGNED NOT NULL,
  `bq_p_photo` varchar(128) DEFAULT NULL,
  `bq_id` int(20) DEFAULT NULL,
  `bq_p_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banquet_section`
--

CREATE TABLE `banquet_section` (
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `section_bq_id` int(25) DEFAULT NULL,
  `section_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_fee` int(25) NOT NULL DEFAULT '0',
  `section_amenity` mediumtext COLLATE utf8_unicode_ci,
  `section_schedule` mediumtext COLLATE utf8_unicode_ci,
  `section_photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banquet_section`
--

INSERT INTO `banquet_section` (`section_id`, `section_bq_id`, `section_title`, `section_fee`, `section_amenity`, `section_schedule`, `section_photo`) VALUES
(1, 2, 'Marriage Hall', 0, '1,2,3', '[[\"3\",\"3\"]]', 'uploads/banquet/section/bqsec_20180713062407_2.jpg'),
(2, 4, 'Marriage Hall', 0, '1,2,3', '[[\"1\",\"46\"]]', 'uploads/banquet/section/bqsec_20180718103819_4.jpg'),
(3, 1, 'मंगलकार्यालय', 0, '1,2,3,4', '[[\"19\",\"25\"],[\"29\",\"37\"]]', 'uploads/banquet/section/bqsec_20180720082341_1.jpg'),
(4, 5, 'Marriage Hall', 0, '1,2,3,4', '[[\"16\",\"40\"]]', 'uploads/banquet/section/bqsec_20180720024647_5.png'),
(5, 5, 'Birthday Party ', 0, '1,2,3,4', '[[\"31\",\"47\"]]', 'uploads/banquet/section/bqsec_20180720030242_5.jpeg'),
(6, 6, 'Marriage Hall', 25000, '1,2,3,4', '[[\"1\",\"48\"]]', 'uploads/banquet/section/bqsec_20180724053259_6.jpg'),
(7, 6, 'Birthday hall', 10000, '1,2,3,4', '[[\"38\",\"43\"]]', 'uploads/banquet/section/bqsec_20180729045103_6.png'),
(8, 7, 'Marrige Hall', 80000, '1,2,3', '[[\"1\",\"1\"]]', 'uploads/banquet/section/bqsec_20180920110314_7.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `bq_section_amenities`
--

CREATE TABLE `bq_section_amenities` (
  `sec_amenity_id` bigint(20) UNSIGNED NOT NULL,
  `sec_amenity_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_amenity_icon` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sec_amenity_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bq_section_amenities`
--

INSERT INTO `bq_section_amenities` (`sec_amenity_id`, `sec_amenity_name`, `sec_amenity_icon`, `sec_amenity_date`) VALUES
(1, '2 व्हीलर पार्किंग ', 'uploads/banquet/amenity/icon-bike.png', '2018-07-16 08:24:36'),
(2, '4 व्हीलर पार्किंग ', 'uploads/banquet/amenity/icon-car.png', '2018-07-16 08:24:36'),
(3, 'मोबाईल चार्जिंग', 'uploads/banquet/amenity/icon-mob.png', '2018-07-16 08:24:59'),
(4, 'LED Television', 'uploads/banquet/amenity/icon-tv.png', '2018-07-16 08:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `bq_section_appointment`
--

CREATE TABLE `bq_section_appointment` (
  `app_id` bigint(20) UNSIGNED NOT NULL,
  `app_section_id` int(20) DEFAULT NULL,
  `app_banquet_id` int(25) NOT NULL,
  `app_user_id` int(25) DEFAULT NULL,
  `app_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_user_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_date` date DEFAULT NULL,
  `app_slot` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_reason` mediumtext COLLATE utf8_unicode_ci,
  `app_status` int(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bq_section_appointment`
--

INSERT INTO `bq_section_appointment` (`app_id`, `app_section_id`, `app_banquet_id`, `app_user_id`, `app_user_name`, `app_user_phone`, `app_user_email`, `app_date`, `app_slot`, `app_reason`, `app_status`) VALUES
(1, 2, 0, 2, ' Swapnil ', '9503475803', 'snil.mahapure@gmail.com', '2018-12-18', '12.00 am-10.30 pm', 'Marriage Function ', 0),
(7, 4, 0, 0, 'Sidhart ', '9658771258', 'snil.mahapure@gmail.com', '2018-12-18', '07.30 am-07.30 pm', 'Marrige Hall Booking ', 0),
(8, 5, 0, 3, ' Swap', 'raj', 'swaptimeg@gmail.com', '2018-10-18', '03.00 pm-11.00 pm', 'Birthday party ', 0),
(9, 5, 0, 3, ' Swap', 'raj', 'swaptimeg@gmail.com', '2018-12-18', '03.00 pm-11.00 pm', 'Birthday party ', 0),
(10, 4, 0, 3, ' Swara Mahapure ', '8308266277', 'ashwinisonawane1137@gmail.com', '2018-07-23', '07.30 am-07.30 pm', 'Marriage ', 0),
(11, 6, 0, 0, 'Rajesh Kale', '7583242563', 'snil.mahapure@gmail.com', '2018-12-18', '12.00 am-11.30 pm', 'Marriage Ceremony\r\nKale & Gite\r\nAddress :   Baramati Road Saswad Pune 21', 0),
(12, 6, 0, 0, 'Admin ', '4587145258', 'snil.mahapure@gmail.com', '2018-08-15', '12.00 am-11.30 pm', 'Marriage Function of ', 0),
(13, 7, 0, 0, ' Swara Mahapure ', '8308266277', 'ashwinisonawane1137@gmail.com', '2018-07-31', '06.30 pm-09.00 pm', 'Birthday  party ', 0),
(14, 6, 0, 4, ' Namdep Mahapure', '9545281422', 'shubhamenterprises34@gmail.com', '2018-11-15', '12.00 am-11.30 pm', 'Marriage Function  ', 0),
(15, 6, 0, 4, 'swap', '9503475803', 'swaptimev@gmail.com', '2018-08-31', '12.00 am-11.30 pm', 'marriage hall ', 0),
(23, 3, 1, 1, ' Sagar Satpute', '9404050990', 'rushikesh.trumpets@gmail.com', '2018-08-30', '09.00 am-12.00 pm', 'asdasd asdasd asd dasdasd asd asd sd', 0),
(24, 3, 1, 1, ' AKshara', '9404050990', 'rushikesh.trumpets@gmail.com', '2018-09-01', '09.00 am-12.00 pm', 'sdasd asdas dasd asd ', 0),
(25, 3, 1, 1, ' Akshara Jadhav', '9876543210', 'rushikesh.trumpets@gmail.com', '2018-09-02', '02.00 pm-06.00 pm', 'sadkl asdkjlad akldla ndas nasdnd ', 0),
(26, 6, 6, 5, 'VISAHL MAHAPURE', '9503475803', 'snil.mahapure@gmail.com', '2018-11-23', '12.00 am-11.30 pm', 'Marriage Hall ', 0),
(27, 7, 6, 4, ' Namdeo Mahapure', '9503475803', 'swaptimev@gmail.com', '2018-12-18', '06.30 pm-09.00 pm', 'g', 0),
(28, 6, 6, 6, ' df', 'fdf', 'fd', '2018-09-11', '12.00 am-11.30 pm', 'gf', 0),
(31, 8, 0, 0, 'Hande', '7447402400', 'dr.bipindimble@gmail.com', '2018-12-13', '12.00 am-12.00 am', 'Marriage Hall    60000 adv 21000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bq_section_photos`
--

CREATE TABLE `bq_section_photos` (
  `sec_photo_id` bigint(20) UNSIGNED NOT NULL,
  `sec_photo_sec_id` int(25) DEFAULT NULL,
  `sec_photo_path` varchar(128) DEFAULT NULL,
  `sec_photo_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bq_section_photos`
--

INSERT INTO `bq_section_photos` (`sec_photo_id`, `sec_photo_sec_id`, `sec_photo_path`, `sec_photo_date`) VALUES
(1, 3, 'uploads/banquet/section/section_20180720111430_13.jpg', '2018-07-20 11:14:30'),
(5, 3, 'uploads/banquet/section/section_20180720111617_13.jpg', '2018-07-20 11:16:17'),
(3, 3, 'uploads/banquet/section/section_20180720111447_13.jpg', '2018-07-20 11:14:47'),
(4, 3, 'uploads/banquet/section/section_20180720111454_13.jpg', '2018-07-20 11:14:54'),
(8, 4, 'uploads/banquet/section/section_20180721072056_54.jpg', '2018-07-21 07:20:56'),
(9, 4, 'uploads/banquet/section/section_20180721072109_54.jpg', '2018-07-21 07:21:09'),
(10, 4, 'uploads/banquet/section/section_20180721072131_54.jpg', '2018-07-21 07:21:31'),
(14, 6, 'uploads/banquet/section/section_20180724055448_66.png', '2018-07-24 05:54:48'),
(13, 6, 'uploads/banquet/section/section_20180724055335_66.jpg', '2018-07-24 05:53:35'),
(15, 6, 'uploads/banquet/section/section_20180724055636_66.jpg', '2018-07-24 05:56:36'),
(16, 6, 'uploads/banquet/section/section_20180724055728_66.jpg', '2018-07-24 05:57:28'),
(17, 6, 'uploads/banquet/section/section_20180724055839_66.jpg', '2018-07-24 05:58:39'),
(18, 7, 'uploads/banquet/section/section_20180729045608_67.jpg', '2018-07-29 16:56:08'),
(19, 7, 'uploads/banquet/section/section_20180729045625_67.jpg', '2018-07-29 16:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `dr_id` bigint(20) UNSIGNED NOT NULL,
  `dr_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dr_category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dr_phone` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dr_headline` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_pincode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dr_services` mediumtext COLLATE utf8_unicode_ci,
  `dr_medi_facility` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dr_fee` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `dr_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dr_confirm_psw` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dr_schedule` tinyint(1) DEFAULT '0',
  `dr_status` int(25) NOT NULL DEFAULT '0',
  `dr_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`dr_id`, `dr_name`, `dr_category`, `dr_email`, `dr_phone`, `dr_headline`, `dr_photo`, `dr_address`, `dr_city`, `dr_pincode`, `dr_services`, `dr_medi_facility`, `dr_fee`, `dr_password`, `dr_confirm_psw`, `dr_schedule`, `dr_status`, `dr_date`) VALUES
(5, 'SwapTime (M.B.B. S) Demo', '4', 'snil.mahapure@gmail.com', '7028465803', 'Demo Clinic', '', 'Pune City Road (Demo Adress )', 'Pune ', '411046', 'Family Fission ', 'Medical Facility Available ', '200.00', 'MfZ51128', 'MfZ51128', 1, 1, '2018-06-29 18:54:57'),
(7, 'Sanket Tidke', '4', 'Sankettidke3@gmail.com', '752000853', 'Shree Naryan Clinik', 'uploads/doctor/profile/dr_20180919045223.png', 'Sukhsagr Nagr', 'Pune', '411046', 'Family Fission', '', '100', 'Sanket@3', 'Sanket@3', 1, 1, '2018-09-19 16:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `dr_appointment`
--

CREATE TABLE `dr_appointment` (
  `app_id` bigint(20) UNSIGNED NOT NULL,
  `app_dr_id` int(20) DEFAULT NULL,
  `app_user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `app_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_user_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_date` date DEFAULT NULL,
  `app_slot` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `app_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_reason` mediumtext COLLATE utf8_unicode_ci,
  `app_status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dr_appointment`
--

INSERT INTO `dr_appointment` (`app_id`, `app_dr_id`, `app_user_email`, `app_user_name`, `app_user_phone`, `app_date`, `app_slot`, `app_time`, `app_reason`, `app_status`) VALUES
(1, 7, 'snil.mahapure@gmail.com', ' Swanil Mahapure Demo', '9503475803', '2018-09-24', '11.00 am-02.30 pm', '11:00 AM', 'Demo Your ID Conifirm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dr_category`
--

CREATE TABLE `dr_category` (
  `cat_id` bigint(20) UNSIGNED NOT NULL,
  `cat_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dr_category`
--

INSERT INTO `dr_category` (`cat_id`, `cat_title`, `cat_date`) VALUES
(1, 'Anaesthesia', '2018-06-27 13:15:43'),
(2, 'Andrology', '2018-06-27 13:15:43'),
(3, 'Audiology &amp; Speech Therapy', '2018-06-27 13:15:43'),
(4, 'Ayurvedic', '2018-06-27 13:15:43'),
(5, 'Cardiologist', '2018-06-27 13:15:43'),
(6, 'Dentist', '2018-06-27 13:15:43'),
(7, 'Dermatologist', '2018-06-27 13:15:43'),
(8, 'Diabetologist', '2018-06-27 13:15:43'),
(9, 'Dietician', '2018-06-27 13:15:43'),
(10, 'Endocrinologist', '2018-06-27 13:15:43'),
(11, 'ENT', '2018-06-27 13:15:43'),
(12, 'Gastroenterologist', '2018-06-27 13:15:43'),
(13, 'Gynaecologist', '2018-06-27 13:15:43'),
(14, 'Haematology', '2018-06-27 13:15:43'),
(15, 'Homoeopathy', '2018-06-27 13:15:43'),
(16, 'Medicine', '2018-06-27 13:15:43'),
(17, 'Nephrologist', '2018-06-27 13:15:43'),
(18, 'Neurologist', '2018-06-27 13:15:43'),
(19, 'Neurosurgeon', '2018-06-27 13:15:43'),
(20, 'Nutritionist', '2018-06-27 13:15:43'),
(21, 'Oncologist', '2018-06-27 13:15:43'),
(22, 'Ophthalmologist', '2018-06-27 13:15:43'),
(23, 'Orthopaedics', '2018-06-27 13:15:43'),
(24, 'Orthopedic Surgeon', '2018-06-27 13:15:43'),
(25, 'Paediatrics', '2018-06-27 13:15:43'),
(26, 'Pathologist', '2018-06-27 13:15:43'),
(27, 'Pediatric Cardiac Surgeon', '2018-06-27 13:15:43'),
(28, 'Physiotherapist', '2018-06-27 13:15:43'),
(29, 'Plastic Surgery', '2018-06-27 13:15:43'),
(30, 'Psychiatrist', '2018-06-27 13:15:43'),
(31, 'Pulmonary Medicine', '2018-06-27 13:15:43'),
(32, 'Radiologist', '2018-06-27 13:15:43'),
(33, 'Rheumatologist', '2018-06-27 13:15:43'),
(34, 'Sexologist', '2018-06-27 13:15:43'),
(35, 'Skin', '2018-06-27 13:15:43'),
(36, 'Somnologist', '2018-06-27 13:15:43'),
(37, 'Surgeon', '2018-06-27 13:15:43'),
(38, 'Urologist', '2018-06-27 13:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `dr_clinic_photo`
--

CREATE TABLE `dr_clinic_photo` (
  `dr_cl_id` bigint(20) UNSIGNED NOT NULL,
  `dr_cl_photo` varchar(128) DEFAULT NULL,
  `dr_cl_dr_id` int(20) DEFAULT NULL,
  `dr_cl_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dr_clinic_photo`
--

INSERT INTO `dr_clinic_photo` (`dr_cl_id`, `dr_cl_photo`, `dr_cl_dr_id`, `dr_cl_date`) VALUES
(9, 'uploads/doctor/clinic/clinic_20180719114144_5.jpg', 5, '2018-07-19 11:41:44'),
(10, 'uploads/doctor/clinic/clinic_20180719114216_5.jpeg', 5, '2018-07-19 11:42:16'),
(8, 'uploads/doctor/clinic/clinic_20180719113551_5.jpg', 5, '2018-07-19 11:35:51'),
(6, 'uploads/doctor/clinic/clinic_20180718061045_1.jpg', 1, '2018-07-18 06:10:45'),
(7, 'uploads/doctor/clinic/clinic_20180718061056_1.jpg', 1, '2018-07-18 06:10:56');

-- --------------------------------------------------------

--
-- Table structure for table `dr_schedule`
--

CREATE TABLE `dr_schedule` (
  `sc_id` bigint(20) UNSIGNED NOT NULL,
  `sc_dr_id` int(20) DEFAULT NULL,
  `sc_schedule` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dr_schedule`
--

INSERT INTO `dr_schedule` (`sc_id`, `sc_dr_id`, `sc_schedule`) VALUES
(1, 1, '{\"sun\":[[\"19\",\"25\",\"9\"],[\"33\",\"41\",\"9\"]],\"mon\":[[\"19\",\"25\",\"5\"],[\"25\",\"31\",\"9\"]],\"fri\":[[\"19\",\"25\",\"9\"],[\"29\",\"41\",\"9\"]],\"sat\":[[\"19\",\"25\",\"9\"],[\"33\",\"41\",\"9\"]]}'),
(2, 5, '{\"mon\":[[\"20\",\"30\",\"50\"],[\"35\",\"45\",\"50\"]]}'),
(3, 4, '{\"sun\":[[\"19\",\"25\"]],\"mon\":[[\"19\",\"27\"],[\"33\",\"41\"]],\"tue\":[[\"19\",\"27\"],[\"33\",\"41\"]],\"wed\":[[\"19\",\"27\"],[\"33\",\"41\"]],\"thu\":[[\"19\",\"27\"],[\"33\",\"41\"]],\"fri\":[[\"19\",\"27\"],[\"33\",\"41\"]],\"sat\":[[\"19\",\"27\"],[\"33\",\"41\"]]}'),
(4, 3, '{\"mon\":[[\"19\",\"25\"]],\"tue\":[[\"19\",\"25\"]],\"wed\":[[\"19\",\"25\"]],\"thu\":[[\"19\",\"25\"]],\"fri\":[[\"19\",\"25\"]]}'),
(5, 2, '{\"sun\":[[\"1\",\"13\"]],\"mon\":[[\"1\",\"13\"]],\"tue\":[[\"1\",\"13\"]],\"wed\":[[\"1\",\"13\"]],\"thu\":[[\"1\",\"13\"]],\"fri\":[[\"1\",\"13\"]],\"sat\":[[\"1\",\"13\"]]}'),
(6, 6, '{\"sun\":[[\"40\",\"2\"]],\"tue\":[[\"23\",\"33\"]]}'),
(7, NULL, '[]'),
(8, 7, '{\"mon\":[[\"23\",\"30\",\"50\"],[\"37\",\"45\",\"50\"]]}');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `o_id` bigint(20) UNSIGNED NOT NULL,
  `u_id` int(25) DEFAULT NULL,
  `fname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_total_amt` double DEFAULT NULL,
  `o_total_item` int(15) DEFAULT NULL,
  `o_status` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0',
  `o_payment_status` int(5) DEFAULT '0',
  `o_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `oi_id` bigint(20) UNSIGNED NOT NULL,
  `oi_o_id` int(25) DEFAULT NULL,
  `oi_p_id` int(25) DEFAULT NULL,
  `oi_pname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oi_qty` int(25) DEFAULT NULL,
  `oi_price` double DEFAULT NULL,
  `oi_dis_amt` double DEFAULT NULL,
  `oi_subtotal` double DEFAULT NULL,
  `oi_status` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` bigint(20) UNSIGNED NOT NULL,
  `about_us` text COLLATE utf8_unicode_ci,
  `privacy_policy` text COLLATE utf8_unicode_ci,
  `terms_conditions` text COLLATE utf8_unicode_ci,
  `shop_return_exchange` text COLLATE utf8_unicode_ci,
  `doctor_join_us` text COLLATE utf8_unicode_ci,
  `vendor_join_us` text COLLATE utf8_unicode_ci,
  `banquet_join_us` text COLLATE utf8_unicode_ci,
  `setting_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `about_us`, `privacy_policy`, `terms_conditions`, `shop_return_exchange`, `doctor_join_us`, `vendor_join_us`, `banquet_join_us`, `setting_date`) VALUES
(1, '<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p><b>Welcome to SwapTimeOn</b></p>\r\n\r\n<p>We are\r\none-of-its-kind online platform, featuring an exclusive range of utility\r\nproducts for all industrial purposes. SWAPTIME ON began its operations during\r\n2005, with an objective to inculcate and introduce the concept of industrial\r\nsupply chains in India. Currently our website maintains a fully catalogued collection\r\nof over 1000 SK U\'s, from brands across global geographic</p>\r\n\r\n<p>We are\r\none platform of Industrial Shop, Doctors Booking &amp; Banquet Booking online\r\nplatform, with all verified Products. SwapTimeOn top links.</p><div><b>Shop</b></div><div><b>Doctor</b></div><div><b>Banquet <br></b></div><div><b><br></b></div><div><b><u>Top Links</u><br></b></div><div><b><br></b></div><div><b> </b><b>E-Commerce Shop </b></div>\r\n\r\n<p>Total one stop arrangement for\r\nall industrial, electrical &amp; hardware online store. An online portal where\r\nyou can view list of multiple products added up by several vendors. You can\r\nchoose products of your choice for your work purpose like manufacturing\r\nproduct. With verified products &amp; multiple products as your size, color, brand &amp; certified products with government approvals. You can choose\r\nproducts of your choice and then buy or add them up into cart as per the\r\ninterest in a particular product and pay shopping amount using payment gateway\r\n(Credit Cards, Debit Cards, Cash On Delivery &amp; etc.) which will be\r\nauthentic and trustful.</p>\r\n\r\n<p><b>Doctor Booking &nbsp; \r\n</b></p>\r\n\r\n<p>Doctor booking is multi serviceable doctor appointment planning software\r\nthat enables patients to book appointments online. Appointment booking is done\r\nin easy steps within no time, wherein you can fill from the mentioning the\r\ndetails like information regarding illness date etc. You will likewise get\r\nconvenient notifications and alarms for the appointments booked. The Service is\r\nprovided 24 x 7. You can also view verified Doctor’s profile accordingly to\r\ntake into consideration the of doctor specialist you wish to visit. You can\r\ndeal with your record profile with time IN and OUT for the specific arrangement\r\nbooked.</p>\r\n\r\n<p><b>Banquet Booking</b></p>\r\n\r\n<p>Complete solution to hassle free banquet booking here. You can book your\r\nown banquet hall online anytime to cherish your happiness with your loved ones.\r\nWe provide a wide range of banquet hall that are completely verified for your\r\nsafety. After opting for a hall, you can fill the from for the same entering\r\nthe details like the event, date, time, etc. You will likewise get convenient\r\nnotifications and alarms for the appointments booked. You can view the pricing,\r\nlocation, capacity of the people and all other aspects required for the banquet\r\nbooking. The best one place solution to all you planning assuring the best\r\nlocation and quality. &nbsp; &nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p><br><br>', '<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n&lt;!--\r\n /* Font Definitions */\r\n @font-face\r\n	{font-family:Wingdings;\r\n	panose-1:5 0 0 0 0 0 0 0 0 0;\r\n	mso-font-charset:2;\r\n	mso-generic-font-family:auto;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:0 268435456 0 0 -2147483648 0;}\r\n@font-face\r\n	{font-family:Mangal;\r\n	panose-1:2 4 5 3 5 2 3 3 2 2;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:roman;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:32771 0 0 0 1 0;}\r\n@font-face\r\n	{font-family:Mangal;\r\n	panose-1:2 4 5 3 5 2 3 3 2 2;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:roman;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:32771 0 0 0 1 0;}\r\n@font-face\r\n	{font-family:Calibri;\r\n	panose-1:2 15 5 2 2 2 4 3 2 4;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:swiss;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:-536859905 -1073732485 9 0 511 0;}\r\n /* Style Definitions */\r\n p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n	{mso-style-unhide:no;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:\"\";\r\n	margin-top:0cm;\r\n	margin-right:0cm;\r\n	margin-bottom:8.0pt;\r\n	margin-left:0cm;\r\n	line-height:107%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	mso-bidi-font-size:10.0pt;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Mangal;\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n.MsoChpDefault\r\n	{mso-style-type:export-only;\r\n	mso-default-props:yes;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Mangal;\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n.MsoPapDefault\r\n	{mso-style-type:export-only;\r\n	margin-bottom:8.0pt;\r\n	line-height:107%;}\r\n@page WordSection1\r\n	{size:612.0pt 792.0pt;\r\n	margin:72.0pt 72.0pt 72.0pt 72.0pt;\r\n	mso-header-margin:36.0pt;\r\n	mso-footer-margin:36.0pt;\r\n	mso-paper-source:0;}\r\ndiv.WordSection1\r\n	{page:WordSection1;}\r\n /* List Definitions */\r\n @list l0\r\n	{mso-list-id:69885418;\r\n	mso-list-template-ids:1855474846;}\r\n@list l0:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:36.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l0:level2\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:o;\r\n	mso-level-tab-stop:72.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:\"Courier New\";\r\n	mso-bidi-font-family:\"Times New Roman\";}\r\n@list l0:level3\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:108.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level4\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:144.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level5\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:180.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level6\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:216.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level7\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:252.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level8\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:288.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l0:level9\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:324.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1\r\n	{mso-list-id:95760626;\r\n	mso-list-template-ids:-450222798;}\r\n@list l1:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:36.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l1:level2\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:o;\r\n	mso-level-tab-stop:72.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:\"Courier New\";\r\n	mso-bidi-font-family:\"Times New Roman\";}\r\n@list l1:level3\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:108.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level4\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:144.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level5\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:180.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level6\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:216.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level7\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:252.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level8\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:288.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l1:level9\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:324.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2\r\n	{mso-list-id:1293830637;\r\n	mso-list-template-ids:2055116294;}\r\n@list l2:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:36.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l2:level2\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:o;\r\n	mso-level-tab-stop:72.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:\"Courier New\";\r\n	mso-bidi-font-family:\"Times New Roman\";}\r\n@list l2:level3\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:108.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level4\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:144.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level5\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:180.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level6\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:216.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level7\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:252.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level8\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:288.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l2:level9\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:324.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3\r\n	{mso-list-id:1488781820;\r\n	mso-list-template-ids:-1460249148;}\r\n@list l3:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:36.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l3:level2\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:o;\r\n	mso-level-tab-stop:72.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:\"Courier New\";\r\n	mso-bidi-font-family:\"Times New Roman\";}\r\n@list l3:level3\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:108.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level4\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:144.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level5\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:180.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level6\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:216.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level7\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:252.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level8\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:288.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\n@list l3:level9\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:324.0pt;\r\n	mso-level-number-position:left;\r\n	text-indent:-18.0pt;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Wingdings;}\r\nol\r\n	{margin-bottom:0cm;}\r\nul\r\n	{margin-bottom:0cm;}\r\n--&gt;\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p><b>Privacy Policy</b></p>\r\n\r\n<p>Effective\r\ndate: August 31, 2018</p>\r\n\r\n<p>SWAPTIME\r\n(\"us\", \"we\", or \"our\") operates the\r\n<a target=\"_blank\" rel=\"nofollow\" href=\"http://www.swaptimeon.com\">http://www.swaptimeon.com</a> website (the \"Service\").</p>\r\n\r\n<p>This page\r\ninforms you of our policies regarding the collection, use, and disclosure of\r\npersonal data when you use our Service and the choices you have associated with\r\nthat data. This Privacy Policy of SWAPTIME.</p>\r\n\r\n<p>We use\r\nyour data to provide and improve the Service. By using the Service, you agree\r\nto the collection and use of information in accordance with this policy. Unless\r\notherwise defined in this Privacy Policy, terms used in this Privacy Policy\r\nhave the same meanings as in our Terms and Conditions, accessible from\r\n<a target=\"_blank\" rel=\"nofollow\" href=\"http://www.swaptimeon.com\">http://www.swaptimeon.com</a></p>\r\n\r\n<p><b>Information Collection and Use</b></p>\r\n\r\n<p>We\r\ncollect several different types of information for various purposes to provide\r\nand improve our Service to you.</p>\r\n\r\n<p><b>Types of Data Collected</b></p>\r\n\r\n<p><b>Personal Data</b></p>\r\n\r\n<p>While\r\nusing our Service, we may ask you to provide us with certain personally\r\nidentifiable information that can be used to contact or identify you\r\n(\"Personal Data\"). Personally identifiable information may include,\r\nbut is not limited to:</p>\r\n\r\n<ul>\r\n <li>Email address</li>\r\n <li>First name and last name</li>\r\n <li>Phone number</li>\r\n <li>Address, State, Province,\r\n     ZIP/Postal code, City</li>\r\n <li>Cookies and Usage Data</li>\r\n</ul>\r\n\r\n<p><b>Usage Data</b></p>\r\n\r\n<p>We may\r\nalso collect information how the Service is accessed and used (\"Usage\r\nData\"). This Usage Data may include information such as your computer\'s\r\nInternet Protocol address (e.g. IP address), browser type, browser version, the\r\npages of our Service that you visit, the time and date of your visit, the time\r\nspent on those pages, unique device identifiers and other diagnostic data.</p>\r\n\r\n<p><b>Tracking &amp; Cookies Data</b></p>\r\n\r\n<p>We use\r\ncookies and similar tracking technologies to track the activity on our Service\r\nand hold certain information.</p>\r\n\r\n<p>Cookies\r\nare files with small amount of data which may include an anonymous unique\r\nidentifier. Cookies are sent to your browser from a website and stored on your\r\ndevice. Tracking technologies also used are beacons, tags, and scripts to\r\ncollect and track information and to improve and analyse our Service.</p>\r\n\r\n<p>You can\r\ninstruct your browser to refuse all cookies or to indicate when a cookie is\r\nbeing sent. However, if you do not accept cookies, you may not be able to use\r\nsome portions of our Service.</p>\r\n\r\n<p>Examples\r\nof Cookies we use:</p>\r\n\r\n<ul>\r\n <li><b>Session Cookies.</b> We use Session Cookies to\r\n     operate our Service.</li>\r\n <li><b>Preference Cookies.</b> We use Preference Cookies\r\n     to remember your preferences and various settings.</li>\r\n <li><b>Security Cookies.</b> We use Security Cookies for\r\n     security purposes.</li>\r\n</ul>\r\n\r\n<p><b>Use of Data</b></p>\r\n\r\n<p>SWAPTIME\r\nuses the collected data for various purposes:</p>\r\n\r\n<ul>\r\n <li>To provide and maintain the\r\n     Service</li>\r\n <li>To notify you about changes\r\n     to our Service</li>\r\n <li>To allow you to participate\r\n     in interactive features of our Service when you choose to do so</li>\r\n <li>To provide customer care and\r\n     support</li>\r\n <li>To provide analysis or\r\n     valuable information so that we can improve the Service</li>\r\n <li>To monitor the usage of the\r\n     Service</li>\r\n <li>To detect, prevent and\r\n     address technical issues</li>\r\n</ul>\r\n\r\n<p><b>Transfer of Data</b></p>\r\n\r\n<p>Your\r\ninformation, including Personal Data, may be transferred to — and maintained on\r\n— computers located outside of your state, province, country or other\r\ngovernmental jurisdiction where the data protection laws may differ than those\r\nfrom your jurisdiction.</p>\r\n\r\n<p>If you\r\nare located outside India and choose to provide information to us, please note\r\nthat we transfer the data, including Personal Data, to India and process it\r\nthere.</p>\r\n\r\n<p>Your\r\nconsent to this Privacy Policy followed by your submission of such information\r\nrepresents your agreement to that transfer.</p>\r\n\r\n<p>SWAPTIME\r\nwill take all steps reasonably necessary to ensure that your data is treated\r\nsecurely and in accordance with this Privacy Policy and no transfer of your\r\nPersonal Data will take place to an organization or a country unless there are\r\nadequate controls in place including the security of your data and other\r\npersonal information.</p>\r\n\r\n<p><b>Disclosure of Data</b></p>\r\n\r\n<p><b>Legal Requirements</b></p>\r\n\r\n<p>SWAPTIME\r\nmay disclose your Personal Data in the good faith belief that such action is\r\nnecessary to:</p>\r\n\r\n<ul>\r\n <li>To comply with a legal\r\n     obligation</li>\r\n <li>To protect and defend the rights\r\n     or property of SWAPTIME</li>\r\n <li>To prevent or investigate\r\n     possible wrongdoing in connection with the Service</li>\r\n <li>To protect the personal\r\n     safety of users of the Service or the public</li>\r\n <li>To protect against legal\r\n     liability</li>\r\n</ul>\r\n\r\n<p><b>Security of Data</b></p>\r\n\r\n<p>The\r\nsecurity of your data is important to us, but remember that no method of\r\ntransmission over the Internet, or method of electronic storage is 100% secure.\r\nWhile we strive to use commercially acceptable means to protect your Personal\r\nData, we cannot guarantee its absolute security.</p>\r\n\r\n<p><b>Service Providers</b></p>\r\n\r\n<p>We may\r\nemploy third party companies and individuals to facilitate our Service\r\n(\"Service Providers\"), to provide the Service on our behalf, to\r\nperform Service-related services or to assist us in analysing </p>\r\n\r\n<p>how our\r\nService is used.</p>\r\n\r\n<p>These third\r\nparties have access to your Personal Data only to perform these tasks on our\r\nbehalf and are obligated not to disclose or use it for any other purpose.</p>\r\n\r\n<p><b>Links to Other Sites</b></p>\r\n\r\n<p>Our\r\nService may contain links to other sites that are not operated by us. If you\r\nclick on a third party link, you will be directed to that third party\'s site.\r\nWe strongly advise you to review the Privacy Policy of every site you visit.</p>\r\n\r\n<p>We have\r\nno control over and assume no responsibility for the content, privacy policies\r\nor practices of any third party sites or services.</p>\r\n\r\n<p><b>Children\'s Privacy</b></p>\r\n\r\n<p>Our\r\nService does not address anyone under the age of 18 (\"Children\").</p>\r\n\r\n<p>We do not\r\nknowingly collect personally identifiable information from anyone under the age\r\nof 18. If you are a parent or guardian and you are aware that your Children has\r\nprovided us with Personal Data, please contact us. If we become aware that we\r\nhave collected Personal Data from children without verification of parental\r\nconsent, we take steps to remove that information from our servers.</p>\r\n\r\n<p><b>Changes to This Privacy Policy</b></p>\r\n\r\n<p>We may\r\nupdate our Privacy Policy from time to time. We will notify you of any changes\r\nby posting the new Privacy Policy on this page.</p>\r\n\r\n<p>We will\r\nlet you know via email and/or a prominent notice on our Service, prior to the\r\nchange becoming effective and update the \"effective date\" at the top\r\nof this Privacy Policy.</p>\r\n\r\n<p>You are\r\nadvised to review this Privacy Policy periodically for any changes. Changes to\r\nthis Privacy Policy are effective when they are posted on this page.</p>\r\n\r\n<p><b>Contact Us</b></p>\r\n\r\n<p>If you\r\nhave any questions about this Privacy Policy, please contact us:</p>\r\n\r\nBy email: support@swaptime.com\r\n\r\n\r\n\r\n<br><p></p>', '<p></p><p><b>Introduction</b></p>\r\n\r\n<p>These\r\nWebsite Standard Terms and Conditions written on this webpage shall manage your\r\nuse of our website, swaptimeon.com accessible at <a target=\"_blank\" rel=\"nofollow\" href=\"http://www.swaptimeon.com\">http://www.swaptimeon.com</a>.</p>\r\n\r\n<p>These\r\nTerms will be applied fully and affect to your use of this Website. By using\r\nthis Website, you agreed to accept all terms and conditions written in here.\r\nYou must not use this Website if you disagree with any of these Website\r\nStandard Terms and Conditions.</p>\r\n\r\n<p>Minors or\r\npeople below 18 years old are not allowed to use this Website.</p>\r\n\r\n<p><b>Intellectual Property Rights</b></p>\r\n\r\n<p>Other\r\nthan the content you own, under these Terms, SWAPTIME and/or its license own\r\nall the intellectual property rights and materials contained in this Website.</p>\r\n\r\n<p>You are\r\ngranted limited license only for purposes of viewing the material contained on\r\nthis Website.</p>\r\n\r\n<p><b>Restrictions</b></p>\r\n\r\n<p>You are\r\nspecifically restricted from all of the following:</p>\r\n\r\n<ul>\r\n <li>publishing any Website\r\n     material in any other media;</li>\r\n <li>selling, sub licensing and/or\r\n     otherwise commercializing any Website material;</li>\r\n <li>publicly performing and/or\r\n     showing any Website material;</li>\r\n <li>using this Website in any\r\n     way that is or may be damaging to this Website;</li>\r\n <li>using this Website in any\r\n     way that impacts user access to this Website;</li>\r\n <li>using this Website contrary\r\n     to applicable laws and regulations, or in any way may cause harm to the\r\n     Website, or to any person or business entity;</li>\r\n <li>engaging in any data mining,\r\n     data harvesting, data extracting or any other similar activity in relation\r\n     to this Website;</li>\r\n <li>using this Website to engage\r\n     in any advertising or marketing.</li>\r\n</ul>\r\n\r\n<p>Certain\r\nareas of this Website are restricted from being access by you and SWAPTIME may\r\nfurther restrict access by you to any areas of this Website, at any time, in\r\nabsolute discretion. Any user ID and password you may have for this Website are\r\nconfidential and you must maintain confidentiality as well.</p>\r\n\r\n<p><b>Your Content</b></p>\r\n\r\n<p>In these\r\nWebsite Standard Terms and Conditions, \"Your Content\" shall mean any\r\naudio, video text, images or other material you choose to display on this\r\nWebsite. By displaying Your Content, you grant SWAPTIME a non-exclusive,\r\nworldwide irrevocable, sub licensee, license to use, reproduce, adapt,\r\npublish, translate and distribute it in any and all media.</p>\r\n\r\n<p>Your\r\nContent must be your own and must not be invading any third-party’s rights.\r\nSWAPTIME reserves the right to remove any of Your Content from this Website at\r\nany time without notice.</p>\r\n\r\n<p><b>No warranties</b></p>\r\n\r\n<p>This\r\nWebsite is provided \"as is,\" with all faults, and SWAPTIME express no\r\nrepresentations or warranties, of any kind related to this Website or the\r\nmaterials contained on this Website. Also, nothing contained on this Website\r\nshall be interpreted as advising you.</p>\r\n\r\n<p><b>Limitation of liability</b></p>\r\n\r\n<p>In no\r\nevent shall SWAPTIME, nor any of its officers, directors and employees, shall\r\nbe held liable for anything arising out of or in any way connected with your\r\nuse of this Website whether such liability is under contract. &nbsp;SWAPTIME,\r\nincluding its officers, directors and employees shall not be held liable for\r\nany indirect, consequential or special liability arising out of or in any way\r\nrelated to your use of this Website.</p>\r\n\r\n<p><b>Indemnification</b></p>\r\n\r\n<p>You\r\nhereby indemnify to the fullest extent SWAPTIME from and against any and/or all\r\nliabilities, costs, demands, causes of action, damages and expenses arising in\r\nany way related to your breach of any of the provisions of these Terms.</p>\r\n\r\n<p><b>Sever ability</b></p>\r\n\r\n<p>If any\r\nprovision of these Terms is found to be invalid under any applicable law, such\r\nprovisions shall be deleted without affecting the remaining provisions herein.</p>\r\n\r\n<p><b>Variation of Terms</b></p>\r\n\r\n<p>SWAPTIME\r\nis permitted to revise these Terms at any time as it sees fit, and by using\r\nthis Website you are expected to review these Terms on a regular basis.</p>\r\n\r\n<p><b>Assignment</b></p>\r\n\r\n<p>The\r\nSWAPTIME is allowed to assign, transfer, and subcontract its rights and/or\r\nobligations under these Terms without any notification. However, you are not\r\nallowed to assign, transfer, or subcontract any of your rights and/or\r\nobligations under these Terms.</p>\r\n\r\n<p><b>Entire Agreement</b></p>\r\n\r\n<p>These\r\nTerms constitute the entire agreement between SWAPTIME and you in relation to\r\nyour use of this Website, and supersede all prior agreements and\r\nunderstandings.</p>\r\n\r\n<p><b>Governing Law &amp; Jurisdiction</b></p>\r\n\r\n<p>These\r\nTerms will be governed by and interpreted in accordance with the laws of the\r\nState of in, and you submit to the non-exclusive jurisdiction of the state and\r\nfederal courts located in in for the resolution of any disputes.<a target=\"_blank\" rel=\"nofollow\"></a></p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>', '<p></p><p><b>Free returns in online</b></p>\r\n\r\n<p>Thank you for shopping at swaptimeon.com.</p>\r\n\r\n<p>Please read this policy carefully. This is the Return and Refund Policy\r\nof swaptimeon.com. This Return and Refund Policy for swaptimeon.com.  </p>\r\n\r\n<p><b>Change of Mind Returns:</b></p>\r\n\r\n<p><i>\"Change of mind\" includes purchases you have made in error\r\ne.g. accidentally ordered the wrong size or color, items that don\'t fit the way\r\nthat you would like or unwanted gifts.</i></p>\r\n\r\n<ul>\r\n <li>We\r\n     will accept a change of mind return provided it meets the following\r\n     guidelines:</li>\r\n <li>✔ Return within 7 days we will accept returns\r\n     within 60 days for Industrial Electrical &amp; Hardware provided all other\r\n     conditions are met</li>\r\n <li>✔ Return with proof of purchase</li>\r\n <li>✔ Item must be unworn, unwashed, unused with\r\n     all original tags/labels attached.</li>\r\n <li>✔ Protective hygiene sticker must be in place\r\n     for swimwear returns</li>\r\n <li>✔ Returns must be made within the country of\r\n     purchase</li>\r\n <li>✔ Purchases must be returned to a store of the\r\n     same brand unless the store is a multi-store including that brand or the\r\n     product was ordered through Click &amp; Collect</li>\r\n <li>✔ Item is not listed as an excluded item as\r\n     outlined below</li>\r\n</ul>\r\n\r\n<p><b>Excluded items (for a change of mind return):</b></p>\r\n\r\n<ul>\r\n <li>✘Size Wise Items.</li>\r\n <li><a target=\"_blank\" rel=\"nofollow\">✘</a> Industrial Electrical &amp;\r\n     Hardware.</li>\r\n <li>✘ Control Panel Accessories.</li>\r\n <li>✘ Cosmetics.</li>\r\n <li>✘ Gift Cards.</li>\r\n <li>✘ Sale/Clearance.</li>\r\n <li>✘ Seconds.</li>\r\n <li>✘ Charity Items.</li>\r\n <li>✘Personalized product.</li>\r\n</ul>\r\n\r\n<p><b>Faulty Product Returns:</b></p>\r\n\r\n<p>If\r\nsomething is faulty, incorrectly described or different from the sample shown\r\nwe will provide an exchange or refund provided the item is returned within a\r\nreasonable time with proof of purchase.</p>\r\n\r\n<p>Can\'t\r\nfind your receipt? A bank statement will be fine. If you don\'t have that, get\r\nin touch and we\'ll do our best to track it down.</p>\r\n\r\n<p><b>Returns</b><b> Process</b></p>\r\n\r\n<ol>\r\n <li>Take\r\n     your item(s) to a Industrial Electrical&amp; Co store in India that stocks\r\n     the brand of the item(s) you’d like to return. To find your nearest\r\n     store, visit our Online Store &amp; Customer Care No (7263-803-803)</li>\r\n <li>Provide\r\n     the team member in store your unwanted item, proof of purchase and if you\r\n     paid using a credit card please bring the same card with you. </li>\r\n <li>Once\r\n     the returns policy has been met, we\'ll offer an exchange or refund on the\r\n     spot.<br>\r\n     &nbsp;</li>\r\n</ol>\r\n\r\n<p><b>Exchanges </b></p>\r\n\r\n<p>Exchanges\r\nfor the same item in a different size will not incur any additional charges. If\r\nthe new item is a different product (including a different color)<a target=\"_blank\" rel=\"nofollow\"></a> and there is a price difference you may need to pay the\r\ndifference or receive a partial refund. </p>\r\n\r\n<p><b>Refunds</b></p>\r\n\r\n<p>If you\r\nrequest a refund, the purchase price (excluding delivery charges for online\r\norders) will be refunded to you using the original payment method so if you\r\npaid cash we can offer a cash refund. If you paid using a card, please bring\r\nthe same card with you as we will refund back to your card. </p>\r\n\r\n<p><b>Returns Process (Online)</b></p>\r\n\r\n<p>If your\r\npurchase was made online (including Click &amp; Collect) you may choose to\r\nreturn to our online store. In store purchases must be returned in store. </p>\r\n\r\n<ol>\r\n <li>Visit SwapTimeOn (\r\n     Please Fill Correct Mail ID  )to\r\n     print your FREE domestic returns label</li>\r\n <li>Complete\r\n     your returns form and pack together with the items you would like to\r\n     return</li>\r\n <li>Attach\r\n     your returns label and post &nbsp;</li>\r\n</ol>\r\n\r\n<p>Once\r\nreceived, we\'ll process the refund and notify you via email. Your refund will\r\nbe credited back to you in the next 1-7 days, depending on the payment method\r\nused to place your order.</p>\r\n\r\n<p>If the\r\nitem returned does not meet our Returns and Exchange Policy above your order\r\nwill be sent back to you</p>\r\n\r\n<p><b>Contact us</b></p>\r\n\r\n<p>If you have any questions about our Returns and Refunds\r\nPolicy, <br></p><p>please contact us: <b>ONLY FOR SHOP</b><br></p>\r\n\r\nBy\r\nemail: swaptimev@gmail.com\r\n\r\n\r\n\r\n<br><p></p><br>', '<p>\r\n\r\n</p><p>Facilities required by Patients.</p><p>Booking of Patients to be recorded serially.</p><p>If the Booking is not to be recorded, then the booking is to be shifted to next working day.</p><p>In case of need, online support will be available.</p><p>We will try our best to convince patient to book online in order to prompt &amp; easy Services.</p><p>Booking will be done in English &amp; Marathi on our website.</p><p>Booking Facilities are available to 24x7.</p><p>You will be facilitated to work according to bookings done.</p><p>If, you wish to discontinue, you must giving 5 days written notice.We will ensure to stop our services after that date.</p><p>You need to clear our dues prior to effect the closure of our service contract.</p><p><b><u>Requirement </u><u>for becoming our partner</u></b><u>.</u></p><ol><li>Doctor Name.<br></li><li>Doctor’s practice certificate.</li><li>Clinic address.</li><li>Android phone/Tablet/Computer with internet connection.</li><li>Saving / Current account number.</li><li>Mobile No./ Landline No.</li><li>Email address.</li></ol>\r\n\r\n<br><p></p><br><br>', '<p></p><p>The quality of all material must\r\nbe as per specification / drawings specified in the order or samples provided\r\nby us. The material acceptance will be subject to our inspection &amp;approval\r\nat any time within 30 days after delivery. If the goods are rejected it will be\r\nthe liability of the supplier to remove the rejected material within two weeks.\r\nSupplier will be held responsible for disposition or removal of goods at his\r\nown risk &amp; expenses.</p>\r\n\r\n<p>&nbsp;The quantity of the material must\r\nnot be exceeded than the quantity specified in our purchase order without our\r\npermission in writing.</p>\r\n\r\n<p>Delivery schedules must be\r\nstrictly adhered to as per the purchase order, where ever applicable. Each\r\ndelivery &nbsp; Challan should relate to only one purchase order &amp; must clearly\r\nmention our purchase order number &amp; quantity supplied.<br></p>\r\n\r\n<p>All consignments should be\r\nconsigned to <b>M/s. SwapTime </b>Only.<br></p>\r\n\r\n<p>The warranty of performance of the\r\ngoods supplied shall be for a period of 12 months from the date of acceptance\r\nof goods in our stores or 15 months from the invoice date, whichever is\r\nearlier.<br></p>\r\n\r\n<p>Payment terms will be as mentioned\r\non the purchase order. All the invoice must accompany necessary Government Tax\r\nRule declaration.<br></p>\r\n\r\n<p>Payments will be withheld if these\r\ndeclarations are not received and period of 15 days will be counted from the\r\ndate of such declarations. The payment will be withheld if purchase order\r\nreference is not given.<br></p>\r\n\r\n<p>All the terms of rate and\r\ndiscounts shall be according to the purchase order only.<br></p>\r\n\r\n<p>All bills must be sent in DUPLICATE mentioning our order numbers\r\nand your challan number.<br></p>\r\n\r\n<p>Every packing must contain packing\r\nslip showing material quantity and our order number. Proper packing must be\r\nprovided to the material. If any transit damages occurred due to improper packing\r\nof material, supplier shall bear rejection &amp; replacement charges.</p><p>Every Single Product With Defer-ant Commission Charge after sales,our sales adviser talking with you after we decide commission charge.</p><p>You join with us any time with us after you join than our adviser calling you,first join with us.&nbsp; &nbsp; <br></p>\r\n\r\n<p><u><b>Requirement for becoming our partner :</b> </u></p>\r\n\r\n<p>1. &nbsp; &nbsp; &nbsp; Company Name.</p>\r\n\r\n<p>2. &nbsp; &nbsp; &nbsp; Contact Person Name.</p>\r\n\r\n<p>3. &nbsp; &nbsp; &nbsp;\r\nGST No.</p>\r\n\r\n<p>4. &nbsp; &nbsp; &nbsp;\r\nGST Register Address.</p>\r\n\r\n<p>5. &nbsp; &nbsp; &nbsp;\r\nCompany Address.</p>\r\n\r\n<p>6. &nbsp; &nbsp; &nbsp;\r\nAndroid phone/Tablet/Computer\r\nwith internet connection.</p>\r\n\r\n<p>7. &nbsp; &nbsp; &nbsp;\r\nSaving / Current account\r\nnumber.</p>\r\n\r\n<p>8. &nbsp; &nbsp; &nbsp;\r\nMobile No./ Landline No.</p>\r\n\r\n<p>9. &nbsp; &nbsp; &nbsp;\r\nEmail address.\r\n</p><p><b>Contact Us</b></p>\r\n\r\n<p>If you\r\nhave any questions about this Vendor Join, please contact us:</p>\r\n\r\nBy email: swaptimev@gmail.com\r\n\r\n\r\n\r\n\r\n\r\n<p></p><p><br></p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p><br>', '<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p>Facilities\r\nrequired by Banquet Clint.</p>\r\n\r\n<p>Booking\r\nof Banquet Clint to be recorded serially.</p>\r\n\r\n<p>In case\r\nof need, online support will be available.</p>\r\n\r\n<p>We will\r\ntry our best to convince to book online in order to prompt &amp; easy\r\nServices.</p>\r\n\r\n<p>Booking\r\nwill be done in English &amp; Marathi on our website.</p>\r\n\r\n<p>Booking\r\nFacilities are available to 24x7.</p>\r\n\r\n<p>You will\r\nbe facilitated to work according to bookings done.</p>\r\n\r\n<p>If, you\r\nwish to discontinue, you must giving 15 days written notice. We will ensure to\r\nstop our services after that date.</p>\r\n\r\n<p>You need\r\nto clear our dues prior to affect the closure of our service contract.</p>\r\n\r\n<p><b><u>Requirement\r\nfor becoming our partner</u></b><u>.</u></p>\r\n\r\n<ol>\r\n <li>Banquet Name.</li>\r\n <li>Banquet Registries Prof. </li>\r\n <li>Banquet GST No.</li>\r\n <li>Contact Person Name.</li>\r\n <li>Contact Person Mobile No.</li>\r\n <li>Banquet address.</li>\r\n <li>Android\r\n     phone/Tablet/Computer with internet connection.</li>\r\n <li>Saving / Current account\r\n     number.</li>\r\n <li>Mobile No. / Landline No.</li>\r\n <li>Email address.</li></ol>\r\n<p><b>Contact us</b></p>\r\n\r\n<p>If you have any questions about our Banquet Join us Policy, <br></p><p>please contact us: <b>ONLY FOR BANQUET</b><br></p>\r\n\r\nBy\r\nemail: swaptimeb@gmail.com\r\n\r\n\r\n\r\n<br><p></p>\r\n\r\n<br><br><ol>\r\n</ol>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>', '2018-09-03 12:44:29');

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE `timeslot` (
  `time_id` bigint(20) UNSIGNED NOT NULL,
  `time_title` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslot`
--

INSERT INTO `timeslot` (`time_id`, `time_title`) VALUES
(1, '12.00 am'),
(2, '12.30 am'),
(3, '01.00 am'),
(4, '01.30 am'),
(5, '02.00 am'),
(6, '02.30 am'),
(7, '03.00 am'),
(8, '03.30 am'),
(9, '04.00 am'),
(10, '04.30 am'),
(11, '05.00 am'),
(12, '05.30 am'),
(13, '06.00 am'),
(14, '06.30 am'),
(15, '07.00 am'),
(16, '07.30 am'),
(17, '08.00 am'),
(18, '08.30 am'),
(19, '09.00 am'),
(20, '09.30 am'),
(21, '10.00 am'),
(22, '10.30 am'),
(23, '11.00 am'),
(24, '11.30 am'),
(25, '12.00 pm'),
(26, '12.30 pm'),
(27, '01.00 pm'),
(28, '01.30 pm'),
(29, '02.00 pm'),
(30, '02.30 pm'),
(31, '03.00 pm'),
(32, '03.30 pm'),
(33, '04.00 pm'),
(34, '04.30 pm'),
(35, '05.00 pm'),
(36, '05.30 pm'),
(37, '06.00 pm'),
(38, '06.30 pm'),
(39, '07.00 pm'),
(40, '07.30 pm'),
(41, '08.00 pm'),
(42, '08.30 pm'),
(43, '09.00 pm'),
(44, '09.30 pm'),
(45, '10.00 pm'),
(46, '10.30 pm'),
(47, '11.00 pm'),
(48, '11.30 pm');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `v_id` bigint(20) UNSIGNED NOT NULL,
  `v_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `v_phone` mediumtext COLLATE utf8_unicode_ci,
  `v_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `v_photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_pincode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `v_confirm_psw` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `v_status` int(25) NOT NULL DEFAULT '0',
  `v_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`v_id`, `v_name`, `v_phone`, `v_email`, `v_photo`, `v_address`, `v_city`, `v_pincode`, `v_password`, `v_confirm_psw`, `v_status`, `v_date`) VALUES
(4, 'Namdeo Laxman Mahapure', '9503475803', 'Shubhamenterprises34@gmail.com', 'uploads/vendor/profile/v_20180912020716_4.jpeg', 'Sai Nisarg Apt., 2nd Floor, Shop No 22,Near Good luck Motor .Pune-Satara Road, Katraj Pune', 'Pune', '411046', 'e9319540', 'e9319540', 1, '2018-07-24 06:19:30'),
(7, 'Rahul Brothers', ' +91-22-2201 1140,+91-22-22011141,+91-22-22082782,09867590809', 'mumbai@rahulbros.com', 'uploads/vendor/profile/v_20180924120937.JPG', 'Krishna Bhavan, #57, Lohar Chawl, KM Sharma Marg', 'Mumbai', '400002', 'Rahul@1', 'Rahul@1', 0, '2018-09-24 12:09:37');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `v_id` bigint(20) UNSIGNED NOT NULL,
  `v_fname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_lname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `v_photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v_paddress` mediumtext COLLATE utf8_unicode_ci,
  `v_deli_address` mediumtext COLLATE utf8_unicode_ci,
  `v_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `v_confirm_psw` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `v_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`v_id`, `v_fname`, `v_lname`, `v_phone`, `v_email`, `v_photo`, `v_paddress`, `v_deli_address`, `v_password`, `v_confirm_psw`, `v_date`) VALUES
(1, 'Rushikesh', 'Wakchaure', '9404050990', 'rushikeshw1@gmail.com', 'uploads/user/profile/usr_20180709011013_1.jpg', '', '', '1234', '1234', '2018-07-02 12:39:19'),
(2, 'Sing ', 'Happy ', NULL, 'snil.mahapure@gmail.com', NULL, NULL, NULL, '9503475803', '9503475803', '2018-07-19 17:24:39'),
(3, 'swap', 'Raj ', NULL, 'swaptimeg@gmail.com', NULL, NULL, NULL, '25802580', '25802580', '2018-07-20 15:08:56'),
(5, 'Vishal', 'Mahapure', NULL, 'Vishal123@gmail.com', NULL, NULL, NULL, '123456', '123456', '2018-08-26 06:48:17'),
(6, 'Namdeo', 'Mahapure', NULL, 'Shubhamenterprises34@gmail.com', NULL, NULL, NULL, 'e9319540', 'e9319540', '2018-08-30 16:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_company`
--

CREATE TABLE `visitor_company` (
  `uc_id` bigint(20) UNSIGNED NOT NULL,
  `v_id` int(25) DEFAULT NULL,
  `uc_name` varchar(255) DEFAULT NULL,
  `uc_gst` varchar(25) DEFAULT NULL,
  `uc_address` text,
  `uc_account` varchar(100) DEFAULT NULL,
  `uc_bankname` varchar(150) DEFAULT NULL,
  `uc_bankbranch` varchar(100) DEFAULT NULL,
  `uc_bankifsc` varchar(28) DEFAULT NULL,
  `uc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vndr_brand`
--

CREATE TABLE `vndr_brand` (
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_v_id` int(25) DEFAULT NULL,
  `brand_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vndr_brand`
--

INSERT INTO `vndr_brand` (`brand_id`, `brand_name`, `brand_v_id`, `brand_date`) VALUES
(2, '3D', 4, '2018-08-29 16:53:30'),
(3, 'Shubham Enterprises', 4, '2018-08-29 16:53:51'),
(4, 'Jainson', 4, '2018-08-29 16:54:11'),
(5, 'Jigo', 4, '2018-08-29 16:54:36'),
(6, 'EPC ', 4, '2018-09-14 14:42:05'),
(7, 'KSS', 4, '2018-09-15 13:02:51'),
(9, 'Jainson', 7, '2018-09-24 13:05:36'),
(10, 'Swap', 4, '2018-09-28 07:03:26'),
(11, 'Anand ', 4, '2018-10-03 05:50:26'),
(12, 'Anand ', 4, '2018-10-03 05:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_categoty`
--

CREATE TABLE `vndr_categoty` (
  `cat_id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_v_id` int(25) DEFAULT NULL,
  `cat_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vndr_categoty`
--

INSERT INTO `vndr_categoty` (`cat_id`, `cat_name`, `cat_v_id`, `cat_date`) VALUES
(11, 'Air Filter & Finger Guard', 4, '2018-08-15 06:49:44'),
(13, 'Cable Lugs,Terminals & Connectors', 4, '2018-09-10 04:15:18'),
(14, 'PVC Sleeves Wiring & Marking Printable Sleeves ', 4, '2018-09-14 13:20:22'),
(15, 'Cable Tie & Marker Tie Accessories ', 4, '2018-09-15 12:34:08'),
(16, 'Nylon Cable Gland', 4, '2018-09-19 15:03:29'),
(17, 'Crimping Tools', 7, '2018-09-24 12:52:19'),
(18, 'Epoxy / DMC Products', 4, '2018-09-28 07:16:28'),
(19, 'Nylon Spacer ', 4, '2018-10-03 06:25:33'),
(20, 'Ceramic Beads ', 4, '2018-10-05 07:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_company`
--

CREATE TABLE `vndr_company` (
  `vc_id` bigint(20) UNSIGNED NOT NULL,
  `v_id` int(25) DEFAULT NULL,
  `vc_name` varchar(255) DEFAULT NULL,
  `vc_gstno` varchar(25) DEFAULT NULL,
  `vc_accountno` varchar(28) DEFAULT NULL,
  `vc_bankname` varchar(100) DEFAULT NULL,
  `vc_bankbranch` varchar(50) DEFAULT NULL,
  `vc_bankifsc` varchar(25) DEFAULT NULL,
  `vc_address` text,
  `vc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vndr_company`
--

INSERT INTO `vndr_company` (`vc_id`, `v_id`, `vc_name`, `vc_gstno`, `vc_accountno`, `vc_bankname`, `vc_bankbranch`, `vc_bankifsc`, `vc_address`, `vc_date`) VALUES
(1, 4, 'Shubham Enterprises', '27BBWPM1223K1ZA', '1911638269', 'Kotak Mahindra Bank', 'Sighgad Road Pune', 'KKBK00001764', 'Sai Nisarg Apt., 2nd Floor, Shop No 22,Near Good luck Motor .Pune-Satara Road, Katraj Pune', '2018-08-29 16:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_order`
--

CREATE TABLE `vndr_order` (
  `o_id` bigint(20) UNSIGNED NOT NULL,
  `o_customer` bigint(25) DEFAULT NULL,
  `o_item_count` bigint(25) DEFAULT NULL,
  `o_basicamt` double DEFAULT NULL,
  `o_dis_amt` double DEFAULT NULL,
  `o_tot_amt` double DEFAULT NULL,
  `o_status` varchar(10) DEFAULT '0',
  `o_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vndr_order_item`
--

CREATE TABLE `vndr_order_item` (
  `o_item_id` bigint(20) UNSIGNED NOT NULL,
  `o_item_o_id` bigint(25) DEFAULT NULL,
  `o_item_p_id` bigint(25) DEFAULT NULL,
  `o_item_qty` bigint(25) DEFAULT NULL,
  `o_item_amt` double DEFAULT NULL,
  `o_item_dis_amt` double DEFAULT NULL,
  `o_item_tot_amt` double DEFAULT NULL,
  `o_item_status` varchar(10) DEFAULT '0',
  `o_item_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vndr_product`
--

CREATE TABLE `vndr_product` (
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `p_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `p_stock` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `p_category` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_hsncode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_brand` int(25) NOT NULL DEFAULT '0',
  `p_sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `p_price` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `p_dis_price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_description` mediumtext COLLATE utf8_unicode_ci,
  `p_specification` mediumtext COLLATE utf8_unicode_ci,
  `p_type` int(25) NOT NULL,
  `p_delivery_time` int(25) NOT NULL DEFAULT '1',
  `p_v_id` bigint(25) NOT NULL,
  `p_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vndr_product`
--

INSERT INTO `vndr_product` (`p_id`, `p_name`, `p_stock`, `p_category`, `p_hsncode`, `p_brand`, `p_sku`, `p_price`, `p_dis_price`, `p_description`, `p_specification`, `p_type`, `p_delivery_time`, `p_v_id`, `p_date`) VALUES
(2, 'Industrial Valves & Valve Fittings', '500', '1', NULL, 0, 'Sku', '199', '0', 'Description', '[[\"sp name\",\"120\"]]', 0, 1, 1, '2018-07-08 12:37:25'),
(3, 'Gearbox, Axle, Sprocket & Gear Parts', '20', '1', NULL, 0, 'Sku1', '249', '0', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '[[\"sp name\",\"50\"]]', 0, 1, 1, '2018-07-08 12:39:01'),
(4, 'Dip Dyed Sweater', '5', '2', NULL, 0, 'Sku1', '249', '0', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '[[\"name1\",\"12\"]]', 0, 1, 1, '2018-07-08 12:40:12'),
(5, 'Scarf Ring Corner', '200', '2', NULL, 0, 'Sku', '179', '0', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)', '[[\"sp name 2\",\"50\"]]', 0, 1, 1, '2018-07-09 03:51:17'),
(6, 'MS CH.HD SCREW M4x10', '1000', '3', NULL, 0, 'YELLOW PLATED', '100', '80', 'MACHINE SCREW ITS FOR INDUSTRIAL USING \r\nITS IRON MARTIAL (MS)  ', '[[\"MS CH.HD SCREW \",\"200\"],[\"MS CH.HD SCREW \",\"200\"]]', 0, 1, 2, '2018-07-16 04:43:46'),
(7, 'LUGS No 7249', '1000', '4', NULL, 0, 'Ring Type', '200.00', '180.00', 'Make - 3D\r\nRing Type Lugs Size : 4.5 - 6 Sq.mm\r\nPress Tool No : 7249\r\nPer Pkt In : (200 Nos)\r\n3D No. : 1247\r\nIndustrial Use Only\r\nNon Expiry Product ', '[[\"LUGS NO 7249\",\"180.00\"]]', 0, 1, 2, '2018-07-19 12:19:14'),
(8, 'Self Taping Screw 6.3x55 (8x55) 500 Nos', '1000', '5', NULL, 0, 'Self Taping Screw', '3000', '2500', 'Quantity Per Pack     -  100 Piece\r\nType of Bolts 	          -   Hexagonal Head\r\nSurface Finishing 	  -   Powder Coated\r\nMaterial 	                  -   MS\r\nPackaging Type 	  -   Box \r\n\r\nProduct description:\r\nSize	Rate (Per 100 Pcs.)\r\n4.8mm X 20mm (10X20)	144\r\n4.8mm x 25mm (10x25)	152\r\n5.5mm x 19mm (12x19)	176\r\n5.5mm x 25mm (12x25)	190\r\n5.5mm x 35mm (12x35)	220\r\n5.5mm x 45mm (12x45)	255\r\n5.5mm x 55mm (12x55)	279\r\n5.5mm x 63mm (12x63)	310\r\n5.5mm x 68mm (12x68)	320\r\n5.5mm x 75mm (12x75)	373\r\n5.5mm x 90mm (12x90)	609\r\n5.5mm x 100mm (12x100)	770\r\n5.5mm x 120mm (12x120)	1001\r\n6.3mm x 25mm (14x25)	345\r\n6.3mm x 38mm (14x38)	436\r\n6.3mm x 50mm (14x50)	507\r\n6.3mm x 63mm (14x63)	586\r\n6.3mm x 75mm (14x75)	629\r\n6.3mm x 90mm (14x90	713\r\n\r\nA Hexagonal Head Self Drilling Screws is a screw that can tap its own hole as it is driven into the material. Some self-tapping screws are also self-drilling, which means that, in addition to the tap-like flute in the leading threads, there is also a preliminary drill-like fluted tip that looks much like the tip of a center drill.\r\n\r\n\r\n', '[[\"6.3x25 (500 nos) Box Packing\",\"1650\"],[\"6.3x55 (500 nos) Box Packing\",\"3000\"],[\"6.3x90 (500 nos) Box Packing\",\"4000\"]]', 0, 1, 4, '2018-07-24 06:32:04'),
(9, 'Valves1', '500', NULL, NULL, 0, 'Sku1', '200', '180', NULL, '[[\"Valves 1inch\",\"100\"],[\"Valves 2inch\",\"200\"],[\"Valves 2.5inch\",\"250\"]]', 1, 1, 2, '2018-07-26 07:27:34'),
(10, 'Self Taping Screw 6.3x55 (8x55) 500 Nos', '1000', NULL, NULL, 0, 'Sku1', '50', '50', NULL, '[[\"Specification 1\",\"12\"],[\"Specification 2\",\"12\"],[\"Specification 3\",\"12\"]]', 2, 1, 2, '2018-07-26 07:33:32'),
(11, 'Self Taping Screw 12x55', '500', NULL, NULL, 0, 'sku123', '200', '180', NULL, '[[\"Specification 1\",\"121\"],[\"Specification 2\",\"121\"],[\"Specification 3\",\"123\"],[\"Specification 4\",\"123\"]]', 4, 1, 2, '2018-07-26 07:34:36'),
(12, 'MS CH. HD Screw 4x100', '500', NULL, NULL, 0, 'MS Yellow Plated ', '500', '490', NULL, '[[\"4x10\",\"4x10\"],[\"CH.HD Screw 4x10\",\"500\"],[\"200 nos\",\".30\"],[\"0.30\",\"150\"],[\"60.00\",\"27\"]]', 5, 1, 4, '2018-07-26 12:58:13'),
(13, 'MS CHHD Screw ', '500 Nos', NULL, NULL, 0, 'MSCH-410', '560.00', '580.00', NULL, '[[\"Head x Length \",\"4x15\"],[\"Nos Pkt \",\"500 nos\"],[\"Per Eech \",\"1.00\"],[\"Discount \",\"10 %\"],[\"GST\",\"135.50\"],[\"Total \",\"450.00\"]]', 6, 1, 4, '2018-07-26 15:16:38'),
(14, 'MS CHHD Screw ', '500', NULL, NULL, 0, 'MS', '637.00', '600.00', NULL, '[[\"Head x Length \",\"4x20\"],[\"Nos Pkt \",\"500 nos\"],[\"Per Eech \",\"1.20\"],[\"Discount \",\"10 %\"],[\"GST\",\"97.20\"],[\"Total \",\" 540.00\"]]', 6, 1, 4, '2018-07-26 15:28:30'),
(15, 'Copper Lugs CUS - 05', '200', NULL, NULL, 0, '3D2501-CUS05', '379.00', '', NULL, '[[\"Cable MM - Stud Hole\",\"2.5 - 5\"],[\"Lugs No.\",\"CUS -05\"],[\"3D Cat.No.\",\"3D - 2501\"],[\"Price \\/ Each\",\"2.14\"],[\"Discount\",\"25 %\"],[\"Packing Oty Pkt\",\"200 Nos\"],[\"Line Total\",\"321.00\"],[\"GST Tax 18 %\",\"57.78\"]]', 8, 1, 4, '2018-07-27 06:46:38'),
(16, 'Copper Lugs CUS - 06', '200', NULL, NULL, 0, '3D2502-CUS06', '563.00', '', NULL, '[[\"Cable MM - Stud Hole\",\"4 - 6\"],[\"Lugs No.\",\"CUS -06\"],[\"3D Cat.No.\",\"3D - 2502\"],[\"Price \\/ Each\",\"3.18\"],[\"Discount\",\"25 %\"],[\"Packing Oty Pkt\",\"200 Nos\"],[\"Line Total\",\"477.00\"],[\"GST Tax 18 %\",\"85.86\"]]', 8, 1, 4, '2018-07-27 06:54:28'),
(17, 'Copper Lugs CUS - 07', '200', NULL, NULL, 0, '3D2503 - CUS 07', '722.00', '', NULL, '[[\"Cable MM - Stud Hole\",\"6 - 6\"],[\"Lugs No.\",\"CUS -07\"],[\"3D Cat.No.\",\"3D - 2503\"],[\"Price \\/ Each\",\"4.08\"],[\"Discount\",\"25 %\"],[\"Packing Oty Pkt\",\"200 Nos\"],[\"Line Total\",\"612.00\"],[\"GST Tax 18 %\",\"110.00\"]]', 8, 1, 4, '2018-07-27 06:57:09'),
(18, 'MS CH.HD SCREW M3x6', '1000 Nos', NULL, NULL, 0, 'MSCHHD-0306', '142.00', '', NULL, '[[\"Size Head x Lenth\",\"M3x6\"],[\"Per Pic Price\",\"0.30\"],[\"Discount\",\"20 %\"],[\"Packing Oty Pkt\",\"500 nos\"],[\"Line Total\",\"120.00\"],[\"GST Tax 18 %\",\"21.60\"]]', 9, 1, 4, '2018-07-29 12:47:10'),
(19, 'MS CH.HD SCREW M3x8', '1000 Nos', NULL, NULL, 0, 'MSCHHD-0308', '132.00', '', NULL, '[[\"Size Head x Lenth\",\"M3x8\"],[\"Per Pic Price\",\"0.28\"],[\"Discount\",\"20 %\"],[\"Packing Oty Pkt\",\"500 nos\"],[\"Line Total\",\"112.00\"],[\"GST Tax 18 %\",\"20.16\"]]', 9, 1, 4, '2018-07-29 12:50:29'),
(20, 'MS CH.HD SCREW M3x10', '1000 Nos', NULL, NULL, 0, 'MSCHHD-0310', '118.00', '', NULL, '[[\"Size Head x Lenth\",\"M3x10\"],[\"Per Pic Price\",\"0.25\"],[\"Discount\",\"20 %\"],[\"Packing Oty Pkt\",\"500 nos\"],[\"Line Total\",\"100.00\"],[\"GST Tax 18 %\",\"18.00\"]]', 9, 1, 4, '2018-07-29 12:56:03'),
(21, 'MS CH.HD SCREW M3x12', '1000 Nos', NULL, NULL, 0, 'MSCHHD-0312', '104.00', '', NULL, '[[\"Size Head x Lenth\",\"M3x12\"],[\"Per Pic Price\",\"0.22\"],[\"Discount\",\"20 %\"],[\"Packing Oty Pkt\",\"500 nos\"],[\"Line Total\",\"88.00\"],[\"GST Tax 18 %\",\"104.00\"]]', 9, 1, 4, '2018-07-29 13:33:36'),
(22, 'Fan Grill 3\'', '10', NULL, NULL, 0, 'MFG03', '236.00', '', NULL, '[[\"Inches x MM\",\"3\'\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"10.00\"],[\"Disscount 20%\",\"8.00\"],[\"Line Total\",\"200.00\"],[\"GST 18%\",\"36.00\"]]', 10, 1, 4, '2018-08-15 07:06:00'),
(23, 'Fan Grill 4\'', '10', NULL, NULL, 0, 'MFG04', '354.00', '', NULL, '[[\"Inches x MM\",\"4\'\\\"\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"15.00\"],[\"Disscount 20%\",\"12.00\"],[\"Line Total\",\"300.00\"],[\"GST 18%\",\"54.00\"]]', 10, 1, 4, '2018-08-15 16:16:02'),
(24, 'KSS MARKER TIE 100 MM', '10 PKT', NULL, NULL, 0, 'MCV100', '123.00', '', NULL, '[[\"CODE\",\"MCV100\"],[\"WIDTH\",\"2.5 MM\"],[\"LENGTH\",\"100 MM\"],[\"RS PER 100 Nos\",\"130.00\"],[\"DISS 20%\",\"104.00\"],[\"Line Total\",\"18.72\"],[\"GST 18%\",\"\"]]', 11, 1, 4, '2018-08-19 15:31:24'),
(25, 'Fan Grill 3\' Matel ', '50 Nos', NULL, '7323', 3, 'MFG03', '295.00', '236.00', NULL, '[[\"Size\",\"3\' Inch\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"10.00\"],[\"Disscount 20%\",\"8.00\"],[\"Line Total\",\"200.00\"],[\"GST 18%\",\"36.00\"]]', 12, 1, 4, '2018-08-23 12:08:23'),
(26, 'Fan Grill 4\' Matel', '200 nos', NULL, '7323', 3, 'MFG04', '443.00', '354.00', NULL, '[[\"Size\",\"4\' Inch\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"15.00\"],[\"Disscount 20%\",\"12.00\"],[\"Line Total\",\"300.00\"],[\"GST 18%\",\"54.00\"]]', 12, 1, 4, '2018-08-30 06:15:28'),
(27, 'Fan Grill 6\' Matel (2 Hole)', '200 nos', NULL, '7323', 3, 'MFG06', '885.00', '708.00', NULL, '[[\"Size\",\"6\'\' Inch \"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"30.00\"],[\"Disscount 20%\",\"24.00\"],[\"Line Total\",\"600.00\"],[\"GST 18%\",\"108.00\"]]', 12, 1, 4, '2018-08-30 06:19:55'),
(28, 'Lugs No 8822', '200 nos', NULL, '8536', 2, '3D1704', '335.00', '209.00', NULL, '[[\"Marking Size\",\"1.5 - 4\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.42\"],[\"Diss 37.50%\",\"0.89\"],[\"Line Total\",\"177.50\"],[\"GST 18%\",\"31.95\"]]', 13, 2, 4, '2018-09-10 05:00:40'),
(29, 'Lugs No 8949', '200 nos', NULL, '8536', 2, '3D1875', '363.00', '227.00', NULL, '[[\"Marking Size\",\"1.5 - 5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.54\"],[\"Diss 37.50%\",\"0.96\"],[\"Line Total\",\"192.50\"],[\"GST 18%\",\"34.65\"]]', 13, 2, 4, '2018-09-10 05:20:07'),
(30, 'Lugs No 8635', '200 nos', NULL, '8536', 2, '3D1923', '326.00', '204.00', NULL, '[[\"Marking Size\",\"1.5 - 3\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.38\"],[\"Diss 37.50%\",\"0.86\"],[\"Line Total\",\"172.50\"],[\"GST 18%\",\"31.05\"]]', 13, 2, 4, '2018-09-10 05:25:12'),
(31, 'Lugs No 8636', '200 nos', NULL, '8536', 2, '3D1700', '321.00', '201.00', NULL, '[[\"Marking Size\",\"1.5 - 3.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.36\"],[\"Diss 37.50%\",\"0.85\"],[\"Line Total\",\"170.00\"],[\"GST 18%\",\"30.60\"]]', 13, 2, 4, '2018-09-10 05:35:04'),
(32, 'Lugs No 8637', '200 nos', NULL, '8536', 2, '3D1701', '382.00', '239.00', NULL, '[[\"Marking Size\",\"1.5 - 4\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.62\"],[\"Diss 37.50%\",\"1.01\"],[\"Line Total\",\"202.50\"],[\"GST 18%\",\"36.45\"]]', 13, 2, 4, '2018-09-10 05:44:34'),
(33, 'Lugs No 7249', '200 nos', NULL, '8536', 2, '3D1699', '274.00', '171.00', NULL, '[[\"Marking Size\",\"1.5 - 3.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.16\"],[\"Diss 37.50%\",\"0.73\"],[\"Line Total\",\"145.00\"],[\"GST 18%\",\"26.10\"]]', 13, 1, 4, '2018-09-10 05:48:19'),
(34, 'Lugs No 8641', '200 nos', NULL, '8536', 2, '3D1899', '430.00', '268.00', NULL, '[[\"Marking Size\",\"2.5 - 4\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.82\"],[\"Diss 37.50%\",\"1.14\"],[\"Line Total\",\"227.50\"],[\"GST 18%\",\"268.00\"]]', 13, 2, 4, '2018-09-14 04:59:33'),
(35, 'Lugs No 8642', '200 nos', NULL, '8536', 2, '3D1892', '529.00', '330.00', NULL, '[[\"Marking Size\",\"2.5 - 5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"2.24\"],[\"Diss 37.50%\",\"1.40\"],[\"Line Total\",\"280.00\"],[\"GST 18%\",\"330.00\"]]', 13, 2, 4, '2018-09-14 05:05:11'),
(36, 'Lugs No 8167', '200 nos', NULL, '8536', 2, '3D1902', '363.00', '227.00', NULL, '[[\"Marking Size\",\"2.5 - 3\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.54\"],[\"Diss 37.50%\",\"0.96\"],[\"Line Total\",\"192.50\"],[\"GST 18%\",\"34.65\"]]', 13, 2, 4, '2018-09-14 05:10:34'),
(37, 'Lugs No 7251', '200 nos', NULL, '8536', 2, '3D1884', '328.00', '205.00', NULL, '[[\"Marking Size\",\"2.5 - 3.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.39\"],[\"Diss 37.50%\",\"0.87\"],[\"Line Total\",\"173.75\"],[\"GST 18%\",\"31.58\"]]', 13, 2, 4, '2018-09-14 05:13:40'),
(38, 'Lugs No 7280', '200 nos', NULL, '8536', 2, '3D1903', '758.00', '473.00', NULL, '[[\"Marking Size\",\"2.5 - 5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"3.21\"],[\"Diss 37.50%\",\"2.01\"],[\"Line Total\",\"401.25\"],[\"GST 18%\",\"72.23\"]]', 13, 2, 4, '2018-09-14 05:16:39'),
(39, 'Lugs No 7268', '200 nos', NULL, '8536', 2, '3D1733', '663.00', '414.00', NULL, '[[\"Marking Size\",\"4.6 - 4\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"2.81\"],[\"Diss 37.50%\",\"1.76\"],[\"Line Total\",\"351.25\"],[\"GST 18%\",\"63.23\"]]', 13, 2, 4, '2018-09-14 05:20:59'),
(40, 'Lugs No 7253', '200 nos', NULL, '8536', 2, '3D1907', '474.00', '350.00', NULL, '[[\"Marking Size\",\"4-6 - 3.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"2.37\"],[\"Diss 37.50%\",\"1.48\"],[\"Line Total\",\"296.25\"],[\"GST 18%\",\"53.33\"]]', 13, 2, 4, '2018-09-14 05:23:55'),
(41, 'Lugs No 8643', '200 nos', NULL, '8536', 2, '3D1914', '720.00', '450.00', NULL, '[[\"Marking Size\",\"4-6 - 6\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"3.05\"],[\"Diss 37.50%\",\"1.91\"],[\"Line Total\",\"381.25\"],[\"GST 18%\",\"68.63\"]]', 13, 2, 4, '2018-09-14 05:26:08'),
(42, 'Lugs No 8672', '200 nos', NULL, '8536', 2, '3D1922', '1548.00', '968.00', NULL, '[[\"Marking Size\",\"16 - 5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"6.56\"],[\"Diss 37.50%\",\"4.10\"],[\"Line Total\",\"820.00\"],[\"GST 18%\",\"147.60\"]]', 13, 2, 4, '2018-09-14 05:32:07'),
(43, 'Lugs No CP-9', '200 nos', NULL, '8536', 2, '3D1012', '295.00', '184.00', NULL, '[[\"Marking Size\",\"0.5 - 1.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.25\"],[\"Diss 37.50%\",\"0.78\"],[\"Line Total\",\"156.13\"],[\"GST 18%\",\"28.13\"]]', 14, 2, 4, '2018-09-14 05:52:33'),
(44, 'Lugs No CP -1', '200 nos', NULL, '8536', 2, '3D1021', '335.00', '209.00', NULL, '[[\"Marking Size\",\"2.5\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"1.42\"],[\"Diss 37.50%\",\"0.89\"],[\"Line Total\",\"177.50\"],[\"GST 18%\",\"31.95\"]]', 14, 2, 4, '2018-09-14 05:55:44'),
(45, 'Lugs No CP -3', '200 nos', NULL, '8536', 2, '3D1030', '644.00', '403.00', NULL, '[[\"Marking Size\",\"4\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"2.73\"],[\"Diss 37.50%\",\"1.71\"],[\"Line Total\",\"341.25\"],[\"GST 18%\",\"61.43\"]]', 14, 2, 4, '2018-09-14 12:44:41'),
(46, 'Lugs No CP -5', '200 nos', NULL, '8536', 2, '3D1036', '725.00', '453.00', NULL, '[[\"Marking Size\",\"6\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"3.07\"],[\"Diss 37.50%\",\"1.92\"],[\"Line Total\",\"383.75\"],[\"GST 18%\",\"69.08\"]]', 14, 2, 4, '2018-09-14 12:46:30'),
(47, 'PVC Sleeve 0.5 Sq mm Binding Coil Black', '5 Coil', NULL, '3917', 6, 'EPC005', '218.00', '153.00', NULL, '[[\"1 Coil MTRS\",\"200 mtr coil\"],[\"Per Coil Each \",\"185.00\"],[\"Diss 30%\",\"129.50\"],[\"GST 18%\",\"23.31\"]]', 15, 2, 4, '2018-09-14 14:55:01'),
(48, 'PVC Sleeve 1.0 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC100', '127.00', '89.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"108.00\"],[\"Diss 30%\",\"75.60\"],[\"GST 18%\",\"13.61\"]]', 15, 2, 4, '2018-09-14 15:25:05'),
(49, 'PVC Sleeve 1.5 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC105', '144.00', '101.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"122.00\"],[\"Diss 30%\",\"85.40\"],[\"GST 18%\",\"15.40\"]]', 15, 2, 4, '2018-09-15 11:24:37'),
(50, 'PVC Sleeve 2.0 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC200', '155.00', '108.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"131.00\"],[\"Diss 30%\",\"91.70\"],[\"GST 18%\",\"16.51\"]]', 15, 2, 4, '2018-09-15 11:30:07'),
(51, 'PVC Sleeve 2.5 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC205', '175.00', '122.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"148.00\"],[\"Diss 30%\",\"103.60\"],[\"GST 18%\",\"18.65\"]]', 15, 2, 4, '2018-09-15 11:47:40'),
(52, 'PVC Sleeve 3.0 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC300', '177.00', '124.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"150.00\"],[\"Diss 30%\",\"105.00\"],[\"GST 18%\",\"18.90\"]]', 15, 2, 4, '2018-09-15 11:54:04'),
(53, 'PVC Sleeve 3.5 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC305', '201.00', '140.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"170.00\"],[\"Diss 30%\",\"119.00\"],[\"GST 18%\",\"21.42\"]]', 15, 2, 4, '2018-09-15 11:56:26'),
(54, 'PVC Sleeve 4.0 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC400', '230.00', '161.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"195.00\"],[\"Diss 30%\",\"136.50\"],[\"GST 18%\",\"24.57\"]]', 15, 2, 4, '2018-09-15 12:00:38'),
(55, 'PVC Sleeve 5.0 Sq mm Black', '5 Coil', NULL, '3917', 6, 'EPC500', '328.00', '230.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each \",\"278.00\"],[\"Diss 30%\",\"194.60\"],[\"GST 18%\",\"35.03\"]]', 15, 2, 4, '2018-09-15 12:05:11'),
(56, 'Marker Tie 100 mm ', '5 Pkt', NULL, '3926', 7, 'KSSMCV100', '165.00', '116.00', NULL, '[[\"Length mm X Width mm\",\"100 mm X 2.5 mm (MCV100)\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Pkt \",\"140.00\"],[\"Diss 30%\",\"98.00\"],[\"GST 18%\",\"17.64\"]]', 16, 2, 4, '2018-09-15 13:11:37'),
(57, 'Printable Sleeve 2.5 mm White ', '5 Coil', NULL, '39172390', 6, 'EPCP205', '195.00', '122.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each\",\"165.00\"],[\"Diss 37.50%\",\"103.13\"],[\"GST 18%\",\"18.56\"]]', 17, 2, 4, '2018-09-15 14:26:12'),
(58, 'Printable Sleeve 3.0 mm White', '5 Coil', NULL, '39172390', 6, 'EPCP300', '208.00', '130.00', NULL, '[[\"1 Coil MTRS\",\"100 mtr coil\"],[\"Per Coil Each\",\"176.00\"],[\"Diss 37.50%\",\"110.00\"],[\"GST 18%\",\"19.80\"]]', 17, 2, 4, '2018-09-15 14:31:19'),
(59, 'Lugs No CP -7', '200 nos', NULL, '8536', 2, '3D1041', '1227.00', '767.00', NULL, '[[\"Marking Size\",\"10 Sq mm\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"5.20\"],[\"Diss 37.50%\",\"3.25\"],[\"Line Total\",\"650.00\"],[\"GST 18%\",\"117.00\"]]', 18, 2, 4, '2018-09-16 07:57:03'),
(60, 'Lugs No CP -8', '200 nos', NULL, '8536', 2, '3D1043', '1848.00', '1155.00', NULL, '[[\"Marking Size\",\"16 Sq mm\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"7.83\"],[\"Diss 37.50%\",\"4.89\"],[\"Line Total\",\"978.75\"],[\"GST 18%\",\"176.18\"]]', 18, 2, 4, '2018-09-16 07:59:32'),
(61, 'Lugs No CP -86', '200 nos', NULL, '8536', 2, '3D1044', '4215.00', '2634.00', NULL, '[[\"Marking Size\",\"25 sq mm\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"17.86\"],[\"Diss 37.50%\",\"11.16\"],[\"Line Total\",\"2232.50\"],[\"GST 18%\",\"401.85\"]]', 18, 2, 4, '2018-09-16 08:03:33'),
(62, 'Lugs No CP -87', '200 nos', NULL, '8536', 2, '3D1045', '5560.00', '3481.00', NULL, '[[\"Marking Size\",\"35 sq mm\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"23.60\"],[\"Diss 37.50%\",\"14.75\"],[\"Line Total\",\"2950.00\"],[\"GST 18%\",\"531.00\"]]', 18, 2, 4, '2018-09-16 08:06:28'),
(63, 'Lugs No CP -88', '200 nos', NULL, '8536', 2, '3D1046', '7333.00', '4583.00', NULL, '[[\"Marking Size\",\"50 sq mm\"],[\"Nos In Pkt\",\"200 Nos\"],[\"Per Each\",\"31.07\"],[\"Diss 37.50%\",\"19.42\"],[\"Line Total\",\"3883.75\"],[\"GST 18%\",\"699.08\"]]', 18, 2, 4, '2018-09-16 08:09:38'),
(64, 'Lugs No CP -94', '200 nos', NULL, '8536', 2, '3D1047', '10,157.00', '6348.00', NULL, '[[\"Marking Size\",\"70 sq mm\"],[\"Nos In Pkt\",\"200.00\"],[\"Per Each\",\"43.04\"],[\"Diss 37.50%\",\"26.90\"],[\"Line Total\",\"5380.00\"],[\"GST 18%\",\"968.40\"]]', 18, 2, 4, '2018-09-16 08:12:20'),
(65, 'Ferrules Lugs 0.5 sq mm L08', '200 nos', NULL, '3936', 5, 'E0508', '47.00', '29.50', NULL, '[[\"Size MM X Length\",\"0.50 X 8\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.40\"],[\"Diss 37.50%\",\"0.25\"],[\"Line Total\",\"25.00\"],[\"GST 18%\",\"4.50\"]]', 19, 2, 4, '2018-09-16 09:03:53'),
(66, 'Ferrules Lugs 0.5 sq mm L10', '200 nos', NULL, '3936', 5, 'E0510', '59.00', '37.00', NULL, '[[\"Size MM X Length\",\"0.5 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.50\"],[\"Diss 37.50%\",\"0.31\"],[\"Line Total\",\"31.00\"],[\"GST 18%\",\"5.58\"]]', 19, 2, 4, '2018-09-18 10:13:15'),
(67, 'Ferrules Lugs 0.5 sq mm L12', '200 nos', NULL, '3936', 5, 'E0512', '71.00', '44.00', NULL, '[[\"Size MM X Length\",\"0.50 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.60\"],[\"Diss 37.50%\",\"0.38\"],[\"Line Total\",\"37.50\"],[\"GST 18%\",\"6.75\"]]', 19, 3, 4, '2018-09-18 10:23:41'),
(68, 'Ferrules Lugs 0.75 sq mm L8', '200 nos', NULL, '3936', 5, 'E07508', '59.00', '37.00', NULL, '[[\"Size MM X Length\",\"0.75 X L8\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.50\"],[\"Diss 37.50%\",\"0.31\"],[\"Line Total\",\"31.25\"],[\"GST 18%\",\"5.63\"]]', 19, -8, 4, '2018-09-18 12:16:07'),
(69, 'Ferrules Lugs 0.75 sq mm L10', '200 nos', NULL, '8536', 5, 'E07510', '71.00', '44.25', NULL, '[[\"Size MM X Length\",\"0.75 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.60\"],[\"Diss 37.50%\",\"0.38\"],[\"Line Total\",\"37.50\"],[\"GST 18%\",\"6.75\"]]', 19, 2, 4, '2018-09-18 12:19:46'),
(70, 'Ferrules Lugs 0.75 sq mm L12', '200 nos', NULL, '8536', 5, 'E07512', '83.00', '52.00', NULL, '[[\"Size MM X Length\",\"0.75 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.70\"],[\"Diss 37.50%\",\"0.44\"],[\"Line Total\",\"43.75\"],[\"GST 18%\",\"7.88\"]]', 19, 3, 4, '2018-09-18 12:22:50'),
(71, 'Ferrules Lugs 1.0 sq mm L8', '200 nos', NULL, '8536', 5, 'E1008', '59.00', '37.00', NULL, '[[\"Size MM X Length\",\"1.0 X L8\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.50\"],[\"Diss 37.50%\",\"0.31\"],[\"Line Total\",\"31.25\"],[\"GST 18%\",\"5.63\"]]', 19, 3, 4, '2018-09-18 12:27:13'),
(72, 'Ferrules Lugs 1.0 sq mm L10', '200 nos', NULL, '3936', 5, 'E10010', '71.00', '44.00', NULL, '[[\"Size MM X Length\",\"1.0 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.60\"],[\"Diss 37.50%\",\"0.38\"],[\"Line Total\",\"37.50\"],[\"GST 18%\",\"6.75\"]]', 19, 2, 4, '2018-09-18 12:31:24'),
(73, 'Ferrules Lugs 1.0 sq mm L12', '200 nos', NULL, '8536', 5, 'E10012', '94.00', '59.00', NULL, '[[\"Size MM X Length\",\"1.0 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.80\"],[\"Diss 37.50%\",\"0.50\"],[\"Line Total\",\"50.00\"],[\"GST 18%\",\"9.00\"]]', 19, 2, 4, '2018-09-18 12:33:23'),
(74, 'Ferrules Lugs 1.50 sq mm L10', '200 nos', NULL, '8536', 5, 'E1.5-10', '83.00', '52.00', NULL, '[[\"Size MM X Length\",\"1.5 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.70\"],[\"Diss 37.50%\",\"0.44\"],[\"Line Total\",\"43.75\"],[\"GST 18%\",\"7.88\"]]', 19, 2, 4, '2018-09-18 12:37:13'),
(75, 'Ferrules Lugs 1.50 sq mm L12', '200 nos', NULL, '8536', 5, 'E1.5-12', '94.00', '59.00', NULL, '[[\"Size MM X Length\",\"1.5 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.80\"],[\"Diss 37.50%\",\"0.50\"],[\"Line Total\",\"50.00\"],[\"GST 18%\",\"9.00\"]]', 19, 2, 4, '2018-09-18 12:39:23'),
(76, 'Ferrules Lugs 2.5 sq mm L10', '200 nos', NULL, '8536', 5, 'E2.5-10', '94.00', '59.00', NULL, '[[\"Size MM X Length\",\"2.5 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.80\"],[\"Diss 37.50%\",\"0.50\"],[\"Line Total\",\"50.00\"],[\"GST 18%\",\"9.00\"]]', 19, 3, 4, '2018-09-18 12:45:21'),
(77, 'Ferrules Lugs 2.5 sq mm L12', '200 nos', NULL, '8536', 5, 'E2.5-12', '212.00', '133.00', NULL, '[[\"Size MM X Length\",\"2.5 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"0.90\"],[\"Diss 37.50%\",\"0.56\"],[\"Line Total\",\"56.25\"],[\"GST 18%\",\"10.13\"]]', 19, 2, 4, '2018-09-18 12:47:45'),
(78, 'Ferrules Lugs 4.0 sq mm L9', '200 nos', NULL, '8536', 5, 'E4.0-09', '118.00', '74.00', NULL, '[[\"Size MM X Length\",\"4.0 X L9\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.00\"],[\"Diss 37.50%\",\"0.63\"],[\"Line Total\",\"62.50\"],[\"GST 18%\",\"11.25\"]]', 19, 2, 4, '2018-09-18 12:50:16'),
(79, 'Ferrules Lugs 4.0 sq mm L12', '200 nos', NULL, '8536', 5, 'E4.0-12', '130.00', '81.00', NULL, '[[\"Size MM X Length\",\"4.0 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.10\"],[\"Diss 37.50%\",\"0.69\"],[\"Line Total\",\"68.75\"],[\"GST 18%\",\"12.38\"]]', 19, 2, 4, '2018-09-18 12:52:47'),
(80, 'Ferrules Lugs 6.0 sq mm L10', '200 nos', NULL, '8536', 5, 'E6.0-10', '153.00', '96.00', NULL, '[[\"Size MM X Length\",\"6.0 X L10\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.30\"],[\"Diss 37.50%\",\"0.81\"],[\"Line Total\",\"81.25\"],[\"GST 18%\",\"14.63\"]]', 19, 2, 4, '2018-09-18 12:55:20'),
(81, 'Ferrules Lugs 6.0 sq mm L12', '200 nos', NULL, '8536', 5, 'E6.0-12', '177.00', '111.00', NULL, '[[\"Size MM X Length\",\"6.0 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.50\"],[\"Diss 37.50%\",\"0.94\"],[\"Line Total\",\"93.75\"],[\"GST 18%\",\"16.88\"]]', 19, 2, 4, '2018-09-18 12:58:35'),
(82, 'Ferrules Lugs 10.0 sq mm L12', '200 nos', NULL, '8536', 5, 'E10-12', '330.0', '207.00', NULL, '[[\"Size MM X Length\",\"10 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"2.80\"],[\"Diss 37.50%\",\"1.75\"],[\"Line Total\",\"175.00\"],[\"GST 18%\",\"31.50\"]]', 19, 2, 4, '2018-09-18 13:06:26'),
(83, 'Ferrules Lugs 16.0 sq mm L12', '200 nos', NULL, '8536', 5, 'E16-12', '413', '258.00', NULL, '[[\"Size MM X Length\",\"16.0 X L12\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"3.50\"],[\"Diss 37.50%\",\"2.19\"],[\"Line Total\",\"218.75\"],[\"GST 18%\",\"39.38\"]]', 19, 2, 4, '2018-09-18 13:09:46'),
(84, 'Nylon Cable Gland PG 7', '100 Nos', NULL, '3926', 5, 'JIGOPG7', '236.00', '165.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"3.5 - 6 X 12 mm\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"8.00\"],[\"Diss 30%\",\"5.60\"],[\"Line Total\",\"140.00\"],[\"GST 18%\",\"25.20\"]]', 23, 2, 4, '2018-09-19 16:08:59'),
(85, 'Nylon Cable Gland PG 9', '100 Nos', NULL, '3926', 5, 'JIGOPG9', '266.00', '186.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"4-8 X 16 MM\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"9.00\"],[\"Diss 30%\",\"6.30\"],[\"Line Total\",\"157.80\"],[\"GST 18%\",\"28.35\"]]', 23, 2, 4, '2018-09-19 16:15:32'),
(86, 'Nylon Cable Gland PG 11', '100 Nos', NULL, '3926', 5, 'JIGOPG11', '325.00', '227.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"5-10 X 17.9\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"11.00\"],[\"Diss 30%\",\"7.70\"],[\"Line Total\",\"192.50\"],[\"GST 18%\",\"34.65\"]]', 23, 2, 4, '2018-09-21 08:00:23'),
(87, 'Nylon Cable Gland PG 13.5', '100 Nos', NULL, '3926', 5, 'JIGOPG13.5', '384.00', '268.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"6-12 X 20.2\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"13.00\"],[\"Diss 30%\",\"9.10\"],[\"Line Total\",\"227.50\"],[\"GST 18%\",\"40.95\"]]', 23, -5, 4, '2018-09-21 08:02:24'),
(88, 'Nylon Cable Gland PG 16', '100 Nos', NULL, '3926', 5, 'JIGOPG16', '566.00', '396.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"10-14 X 22\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"16.00\"],[\"Diss 30%\",\"11.20\"],[\"Line Total\",\"336.00\"],[\"GST 18%\",\"60.48\"]]', 23, 2, 4, '2018-09-21 08:04:51'),
(89, 'Nylon Cable Gland PG 19', '100 Nos', NULL, '3926.00', 5, 'JIGOPG19', '649.00', '454.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"12-15 X 24\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"22.00\"],[\"Diss 30%\",\"15.40\"],[\"Line Total\",\"385.00\"],[\"GST 18%\",\"69.30\"]]', 23, 1, 4, '2018-09-21 08:09:32'),
(90, 'Nylon Cable Gland PG 21', '100 Nos', NULL, '3926.00', 5, 'JIGOPG21', '271.00', '190.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"13-18 X 26.9\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"23.00\"],[\"Diss 30%\",\"16.10\"],[\"Line Total\",\"161.00\"],[\"GST 18%\",\"28.98\"]]', 23, 1, 4, '2018-09-21 08:13:11'),
(91, 'Nylon Cable Gland PG 25', '100 Nos', NULL, '3926', 5, 'JIGOPG21', '354.00', '248.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"15-22 X 29.8\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"30.00\"],[\"Diss 30%\",\"21.00\"],[\"Line Total\",\"210.00\"],[\"GST 18%\",\"37.80\"]]', 23, 1, 4, '2018-09-21 08:15:31'),
(92, 'Nylon Cable Gland PG 29', '100 Nos', NULL, '3926.00', 5, 'JIGOPG29', '425.00', '297.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"18-25 X 36\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"36.00\"],[\"Diss 30%\",\"25.20\"],[\"Line Total\",\"252.00\"],[\"GST 18%\",\"45.36\"]]', 23, 1, 4, '2018-09-21 08:19:36'),
(93, 'Nylon Cable Gland PG 36', '100 Nos', NULL, '3926', 5, 'JIGOPG36', '826.00', '578.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"22-32 X 45.7\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"70.00\"],[\"Diss 30%\",\"49.00\"],[\"Line Total\",\"490.00\"],[\"GST 18%\",\"88.20\"]]', 23, 1, 4, '2018-09-21 08:22:02'),
(94, 'Nylon Cable Gland PG 42', '100 Nos', NULL, '3926', 5, 'JIGOPG42', '531.00', '743.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"30-38 X 54\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"90.00\"],[\"Diss 30%\",\"63.00\"],[\"Line Total\",\"630.00\"],[\"GST 18%\",\"113.40\"]]', 23, 1, 4, '2018-09-21 08:28:53'),
(95, 'Nylon Cable Gland PG 48', '100 Nos', NULL, '3926', 5, 'JIGOPG48', '1298.00', '901.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"34-44 X 59\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"110.00\"],[\"Diss 30%\",\"\"],[\"Line Total\",\"770.00\"],[\"GST 18%\",\"138.60\"]]', 23, 1, 4, '2018-09-21 08:31:15'),
(96, 'Nylon Cable Gland PG 63', '100 Nos', NULL, '3926', 5, 'JIGOPG63', '1157.00', '1090.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"42 - 54 X 71\"],[\"Nos In Pkt\",\"6 Nos\"],[\"Per Each\",\"220.00\"],[\"Diss 30%\",\"154.00\"],[\"Line Total\",\"924.00\"],[\"GST 18%\",\"166.32\"]]', 23, 1, 4, '2018-09-21 08:35:34'),
(97, 'Nylon Cable Gland PG 7', '100 Nos', NULL, '3926', 5, 'JIGOPG7B', '236.00', '165.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"3.5 - 6 X 12 mm\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"8.00\"],[\"Diss 30%\",\"5.60\"],[\"Line Total\",\"140.00\"],[\"GST 18%\",\"25.20\"]]', 20, 1, 4, '2018-09-24 11:26:53'),
(98, 'Nylon Cable Gland PG 9', '100 Nos', NULL, '3926', 5, 'JIGOPG9B', '295.00', '207.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"4-8 X 16 MM\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"10.00\"],[\"Diss 30%\",\"7.00\"],[\"Line Total\",\"175.00\"],[\"GST 18%\",\"31.50\"]]', 20, 1, 4, '2018-09-24 11:29:29'),
(99, 'Nylon Cable Gland PG 11', '100 Nos', NULL, '3926', 5, 'JIGOPG11B', '354.00', '248.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"5-10 X 17.9\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"12.00\"],[\"Diss 30%\",\"8.40\"],[\"Line Total\",\"210.00\"],[\"GST 18%\",\"37.80\"]]', 20, 1, 4, '2018-09-24 11:33:07'),
(100, 'Virat-6 ', '1 Nos', NULL, '8536', 9, 'Virat6J', '684.00', '479.00', NULL, '[[\"Nos In Pkt\",\"1 Nos\"],[\"Per Each \",\"580.00\"],[\"Diss 30%\",\"406.00\"],[\"GST 18%\",\"73.08\"]]', 25, 1, 7, '2018-09-24 13:08:23'),
(101, 'Jop-427', '1 Nos', NULL, '8536', 9, 'Jop427J', '944.00', '661.00', NULL, '[[\"Nos In Pkt\",\"1 Nos\"],[\"Per Each \",\"800.00\"],[\"Diss 30%\",\"560.00\"],[\"GST 18%\",\"100.80\"]]', 25, 1, 7, '2018-09-24 13:11:47'),
(102, 'Samrat-16', '1 Nos', NULL, '8536', 9, 'Samrat16J', '963.00', '674.00', NULL, '[[\"Nos In Pkt\",\"1 Nos\"],[\"Per Each \",\"816.00\"],[\"Diss 30%\",\"571.20\"],[\"GST 18%\",\"102.82\"]]', 25, 1, 7, '2018-09-24 13:29:30'),
(103, 'Vikrant-50', '1 Nos', NULL, '8536', 9, 'Vikrant50J', '1451.00', '1016.00', NULL, '[[\"Nos In Pkt\",\"1 Nos\"],[\"Per Each \",\"1230.00\"],[\"Diss 30%\",\"861.00\"],[\"GST 18%\",\"154.98\"]]', 25, 1, 7, '2018-09-24 13:33:34'),
(104, 'Nylon Cable Gland PG 13.5', '100 Nos', NULL, '3926', 5, 'JIGOPG13.5B', '413.00', '289.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"6-12 X 20.2\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"14.00\"],[\"Diss 30%\",\"9.80\"],[\"Line Total\",\"245.00\"],[\"GST 18%\",\"44.10\"]]', 20, 2, 4, '2018-09-28 06:43:16'),
(105, 'Nylon Cable Gland PG 16', '100 Nos', NULL, '3926', 5, 'JIGOPG16B', '531.00', '372.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"10-14 X 22\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"18.00\"],[\"Diss 30%\",\"12.60\"],[\"Line Total\",\"315.00\"],[\"GST 18%\",\"56.70\"]]', 20, 2, 4, '2018-09-28 06:45:54'),
(106, 'Nylon Cable Gland PG 19', '100 Nos', NULL, '3926', 5, 'JIGOPG19B', '708.00', '496.00', NULL, '[[\"Cable Range MM X Panel Hole Size MM\",\"12-15 X 24\"],[\"Nos In Pkt\",\"25 Nos\"],[\"Per Each\",\"24.00\"],[\"Diss 30%\",\"16.80\"],[\"Line Total\",\"420.00\"],[\"GST 18%\",\"75.60\"]]', 20, 2, 4, '2018-09-28 06:49:15'),
(107, 'Insulator 30x6', '100 Nos', NULL, '8546', 3, 'EBS01', '165.00', '124.00', NULL, '[[\"Hight X Dia X Insert\",\"30 X 30 X 6\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"14.00\"],[\"Diss 25%\",\"10.50\"],[\"Line Total\",\"105.00\"],[\"GST 18%\",\"18.90\"]]', 26, 1, 4, '2018-10-02 07:52:12'),
(108, 'Insulator 30x8', '100 Nos', NULL, '8546', 3, 'EBS02', '189.00', '142.00', NULL, '[[\"Hight X Dia X Insert\",\"30 X 30 X 8\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"16.00\"],[\"Diss 25%\",\"12.00\"],[\"Line Total\",\"120.00\"],[\"GST 18%\",\"21.60\"]]', 26, 1, 4, '2018-10-02 07:55:30'),
(109, 'Insulator 40x8', '100 Nos', NULL, '8546', 3, 'EBS03', '295.00', '221.00', NULL, '[[\"Hight X Dia X Insert\",\"40 X 40 X8\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"25.00\"],[\"Diss 25%\",\"18.75\"],[\"Line Total\",\"187.50\"],[\"GST 18%\",\"33.75\"]]', 26, 1, 4, '2018-10-02 07:58:11'),
(110, 'Insulator 40x10', '100 Nos', NULL, '8546', 3, 'EBS04', '354.00', '266.00', NULL, '[[\"Hight X Dia X Insert\",\"40 X 40 X 10\"],[\"Nos In Pkt\",\"10 Nos\"],[\"Per Each\",\"30.00\"],[\"Diss 25%\",\"22.50\"],[\"Line Total\",\"225.00\"],[\"GST 18%\",\"40.50\"]]', 26, 1, 4, '2018-10-02 08:01:36'),
(111, 'Nylon Spacer M3x10', '100 Nos', NULL, '3926', 3, '310', '165.00', '116.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 10 X 6 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.40\"],[\"Diss 30%\",\"0.98\"],[\"Line Total\",\"98.00\"],[\"GST 18%\",\"17.64\"]]', 27, 1, 4, '2018-10-04 06:19:34'),
(112, 'Nylon Spacer M3x12', '100 Nos', NULL, '3926', 3, '312', '156.00', '128.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 12 X 6 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.56\"],[\"Diss 30%\",\"1.09\"],[\"Line Total\",\"109.20\"],[\"GST 18%\",\"19.66\"]]', 27, 1, 4, '2018-10-05 06:35:19'),
(113, 'Nylon Spacer M3x15', '100 Nos', NULL, '3926', 3, '315', '189.00', '133.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 15 X 6 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.61\"],[\"Diss 30%\",\"1.13\"],[\"Line Total\",\"112.70\"],[\"GST 18%\",\"20.29\"]]', 27, 1, 4, '2018-10-05 06:37:46'),
(114, 'Nylon Spacer M3x20', '100 Nos', NULL, '3926', 3, '320', '215.00', '150.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 20 X 6 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.82\"],[\"Diss 30%\",\"1.27\"],[\"Line Total\",\"127.40\"],[\"GST 18%\",\"22.93\"]]', 27, 1, 4, '2018-10-05 06:41:14'),
(115, 'Nylon Spacer M3x25', '100 Nos', NULL, '3926', 3, '325', '234.00', '164.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 25 X 6 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"1.98\"],[\"Diss 30%\",\"1.39\"],[\"Line Total\",\"138.60\"],[\"GST 18%\",\"24.95\"]]', 27, 1, 4, '2018-10-05 06:49:39'),
(116, 'Nylon Spacer M3x30', '100 Nos', NULL, '3926', 3, '330', '413.00', '289.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 30 X  7A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"3.50\"],[\"Diss 30%\",\"2.45\"],[\"Line Total\",\"245.00\"],[\"GST 18%\",\"44.10\"]]', 27, 1, 4, '2018-10-05 06:52:45'),
(117, 'Nylon Spacer M3x35', '100 Nos', NULL, '3926', 3, '335', '426.00', '298.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 35 X 7 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"3.61\"],[\"Diss 30%\",\"2.53\"],[\"Line Total\",\"252.70\"],[\"GST 18%\",\"45.49\"]]', 27, 1, 4, '2018-10-05 06:55:39'),
(118, 'Nylon Spacer M3x40', '100 Nos', NULL, '3926', 3, '340', '459.00', '321.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 40 X 7 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"3.89\"],[\"Diss 30%\",\"2.72\"],[\"Line Total\",\"272.30\"],[\"GST 18%\",\"49.01\"]]', 27, 1, 4, '2018-10-05 06:58:26'),
(119, 'Nylon Spacer M3x45', '100 Nos', NULL, '3926', 3, '345', '490.00', '343.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 45 X 7 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"4.15\"],[\"Diss 30%\",\"2.91\"],[\"Line Total\",\"290.50\"],[\"GST 18%\",\"52.79\"]]', 27, 1, 4, '2018-10-05 07:00:39'),
(120, 'Nylon Spacer M3x50', '100 Nos', NULL, '3926', 3, '350', '531.00', '372.00', NULL, '[[\"Thread X Length X A\\/F\",\"3 X 50 X 7 A\\/F\"],[\"Nos In Pkt\",\"100 Nos\"],[\"Per Each\",\"4.50\"],[\"Diss 30%\",\"3.15\"],[\"Line Total\",\"315.00\"],[\"GST 18%\",\"56.70\"]]', 27, 1, 4, '2018-10-05 07:03:05'),
(121, 'Ceramic Beads 3 MM', '1 Pkt', NULL, '612', 3, '300', '885.00', '620.00', NULL, '[[\"Hight MM\",\"3 mm\"],[\"Packing In Pkt\",\"1000 Nos\"],[\"Per Pkt\",\"750.00\"],[\"Diss 30%\",\"525.00\"],[\"GST 18%\",\"94.50\"]]', 28, 2, 4, '2018-10-05 08:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_product_images`
--

CREATE TABLE `vndr_product_images` (
  `img_id` bigint(20) UNSIGNED NOT NULL,
  `img_p_id` bigint(25) DEFAULT NULL,
  `img_path` varchar(255) DEFAULT NULL,
  `img_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vndr_product_images`
--

INSERT INTO `vndr_product_images` (`img_id`, `img_p_id`, `img_path`, `img_date`) VALUES
(1, 1, 'uploads/vendor/product/item_20180704125548_1.jpg', '2018-07-04 12:55:48'),
(2, 1, 'uploads/vendor/product/item_20180704125600_1.jpg', '2018-07-04 12:56:00'),
(4, 2, 'uploads/vendor/product/item_20180708124212_1.jpg', '2018-07-08 12:42:12'),
(6, 2, 'uploads/vendor/product/item_20180708124232_1.jpg', '2018-07-08 12:42:32'),
(7, 3, 'uploads/vendor/product/item_20180708124251_1.jpg', '2018-07-08 12:42:51'),
(8, 6, 'uploads/vendor/product/item_20180716044430_2.jpg', '2018-07-16 04:44:30'),
(9, 7, 'uploads/vendor/product/item_20180719122121_2.jpg', '2018-07-19 12:21:21'),
(10, 7, 'uploads/vendor/product/item_20180719122140_2.jpg', '2018-07-19 12:21:40'),
(11, 8, 'uploads/vendor/product/item_20180724063251_4.jpg', '2018-07-24 06:32:51'),
(12, 8, 'uploads/vendor/product/item_20180724063307_4.jpg', '2018-07-24 06:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_product_type`
--

CREATE TABLE `vndr_product_type` (
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `type_category` int(25) DEFAULT NULL,
  `type_image` varchar(128) DEFAULT NULL,
  `type_description` text,
  `type_specification` text,
  `type_v_id` int(25) DEFAULT NULL,
  `type_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vndr_product_type`
--

INSERT INTO `vndr_product_type` (`type_id`, `type_name`, `type_category`, `type_image`, `type_description`, `type_specification`, `type_v_id`, `type_date`) VALUES
(23, 'Jigo Nylon PG Thread Cable Gland White', 16, 'uploads/vendor/product/item_20180919034543_4.jpg', '<p>PG Thread Nylon Cable Gland</p><p>\r\n</p><li>Brand:&nbsp;<b>Jigo</b></li><li>Packing Oty: <b>100 Nos (You Buying Ase Per Specification Price)<br></b></li><li>Material:&nbsp;<b>Nylon</b></li><li>Color Finish: <b>White<br></b></li><li>Condition:<b> Brand New Certified Approval</b></li><li><b>\r\n</b>Temperature:<b> 20 degree C to 100 degree C</b></li><p></p><p>\r\nWe offers wide range of <b>PG Type Nylon Cable Glands</b> made up of Top\r\n quality raw material. Our range are available at most affordable \r\nprices. These cable glands are flexible and reusable as electrical \r\ninsulating covers for cable connections.<br><br><b>Features:</b> High strength / Excellent finishing / Dimensional accuracy</p><p>\r\n</p><div>For Industrial Use Only.<br></div>', '[\"Cable Range MM X Panel Hole Size MM\",\"Nos In Pkt\",\"Per Each\",\"Diss 30%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-19 15:45:43'),
(20, 'Jigo Nylon PG Thread Cable Gland Black', 16, 'uploads/vendor/product/item_20180919035443_4.jpeg', '<p>PG Thread Nylon Cable Gland</p><p>\r\n</p><li>Brand:&nbsp;<b>Jigo</b></li><li>Packing Oty: <b>100 Nos<br></b></li><li>Material:&nbsp;<b>Nylon</b></li><li>Color Finish: <b>Black<br></b></li><li>Condition:<b> Brand New Certified Approval</b></li><li><b>\r\n</b>Temperature:<b> 20 degree C to 100 degree C</b></li><p></p><p>\r\nWe offers wide range of <b>PG Type Nylon Cable Glands</b> made up of Top\r\n quality raw material. Our range are available at most affordable \r\nprices. These cable glands are flexible and reusable as electrical \r\ninsulating covers for cable connections.<br><br><b>Features:</b> High strength / Excellent finishing / Dimensional accuracy</p><p>\r\n</p><div>For Industrial Use Only.<br></div><div><br></div>\r\n\r\n<br><p></p><br>', '[\"Cable Range MM X Panel Hole Size MM\",\"Nos In Pkt\",\"Per Each\",\"Diss 30%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-19 15:45:42'),
(13, '3D Fork Type Terminals Non-Insulated ', 13, 'uploads/vendor/product/item_20180910052913_4.jpg', '<p>\r\n</p><div>\r\n        <p>Fork Type Tinned Copper Cable Terminal Ends</p><ul><li>Brand: <b>3D</b></li><li>Packing Oty: <b>200 Nos</b></li><li>Material: <b>E-Copper</b></li><li>Finish: <b>Tin Plated</b></li><li>Condition:<b> Brand New Certified Approvals </b><br>\r\n      </li></ul></div>This type of terminal ends are mainly used for urination \r\nflexible wires, Cords, Control cables, Meters of control \r\npanel/switchboard, Contractors etc, according to the requirement\'s. They\r\n are made of High conductivity copper and are for Crimping.\r\n     			 <div> Fork type tinned copper cable terminal ends (non insulated)</div><div><br></div><div>For Industrial Use Only<br></div><div><br></div>\r\n\r\n<br><p></p><br><br><br><br><br>', '[\"Marking Size\",\"Nos In Pkt\",\"Per Each\",\"Diss 37.50%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-10 04:43:50'),
(14, '3D Pin Type Terminals Non-Insulated', 13, 'uploads/vendor/product/item_20180914054714_4.png', '<p>\r\n</p><div><p>Pin Type Tinned Copper Cable Terminal Ends</p><ul><li>Brand: <b>3D</b></li><li>Packing Oty: <b>200 Nos</b></li><li>Material: <b>E-Copper</b></li><li>Finish: <b>Tin Plated</b></li><li>Condition:<b> Brand New Certified Approvals </b><br>\r\n      </li></ul></div>This type of terminal ends are mainly used for urination \r\nflexible wires, Cords, Control cables, Meters of control \r\npanel/switchboard, Contractors etc, according to the requirement\'s. They\r\n are made of High conductivity copper and are for Crimping.\r\n     			 <div>&nbsp; Pin type tinned copper cable terminal ends (non-insulated)</div><div><br></div><div>For Industrial Use Only<br></div><div><br></div>\r\n\r\n\r\n\r\n<br><p></p>', '[\"Marking Size\",\"Nos In Pkt\",\"Per Each\",\"Diss 37.50%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-14 05:41:14'),
(12, 'Fan Grill Matal', 11, 'uploads/vendor/product/item_20180916044503_4.jpg', '<p>PC DC Fan Grill Protector Metal Finger Guard <br></p><p>Material:<b> Matel</b></p><p>Plating: <b>Silver Tone</b> <br></p><p>Packing: <b>25 Nos in</b> <b>(1 Pkt)</b><br></p><p>Condition: <b>Brand New</b></p><p>\r\n															\r\n															\r\n																Brand New : A brand-new, unused, unopened, undamaged item in its original packaging (where packaging is \r\n																\r\n																			applicable).</p><p>\r\n<b>For Loose Qty Buying Discounts will Be Reduced Up-to 3 % Item wise.\r\n\r\n</b><br></p><p> Packaging should be the same as what is\r\n available in a retail store, unless the item was packaged by the \r\nmanufacturer in non-retail packaging, such as an UN printed box or \r\nplastic bag.  See the seller\'s listing for full details</p><br>', '[\"Size\",\"Nos In Pkt\",\"Per Each\",\"Disscount 20%\",\"Line Total\",\"GST 18%\"]', 4, '2018-08-23 11:08:23'),
(15, 'EPC PVC Wiring Sleeves  ', 14, 'uploads/vendor/product/item_20180915111904_4.jpg', '<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p>PVC Wiring Sleeves</p><p>Brand: <b>EPC</b><br></p><p>Packing Roll:\r\n<b>100 Meters Roll (1 Coil)</b></p><p>Material:\r\n<b>PVC Plastic</b></p><p>Color<b> : Red, Yellow, Blue, Black (This Catalog in Black Price)</b><br></p><p>Condition:<b>\r\nBrand New Certified Approvals</b><br></p><p>Flexibility<b>:\r\nHighly Flexible </b><br></p><p>Feature: Optimum design / Durability standard / Easy to use&nbsp; \r\n\r\n</p><ul>\r\n </ul><p>We are a highly dedicated company\r\nin the domain of supply, distribute, export and trade of <b>PVC Sleeves</b>.\r\nThese sleeves are fabricated by high quality of PVC material which is procured\r\nfrom our trusted vendors. Further, these are available in different colors as\r\nper customer specifications and have great flexibility. Apart from this, these\r\nare offered at suitable prices to our beloved clients. </p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p><b>&nbsp;</b></p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>', '[\"1 Coil MTRS\",\"Per Coil Each \",\"Diss 30%\",\"GST 18%\"]', 4, '2018-09-14 14:25:31'),
(16, 'KSS Marker Tie', 15, 'uploads/vendor/product/item_20180915124120_4.jpg', '<p>\r\n</p><p>Marker Ties <br></p><p>Brand: <b>KSS&nbsp;\r\n\r\n</b><br></p><p>Packing PKT:\r\n<b>100 Nos Pkt (1 Pkt)</b></p><p>Material: <b>\r\nUL approved Nylon 66, 94V-2\r\n\r\n<br></b></p><p>Color<b> : White </b><br></p><p>Condition:<b>\r\nBrand New Certified Approvals</b><br></p><p>Feature: Optimum design / Durability standard / Easy to use</p><p>\r\nTie and identify bundles of cable in one operation.Flat areas may be imprinted or written on with a Marking Pen.\r\n\r\n<br></p>\r\n\r\n<p></p>', '[\"Length mm X Width mm\",\"Nos In Pkt\",\"Per Pkt \",\"Diss 30%\",\"GST 18%\"]', 4, '2018-09-15 12:41:20'),
(17, 'EPC Printable Marking Sleeve White', 14, 'uploads/vendor/product/item_20180918034250_4.jpg', '<p>\r\n</p><p>PVC Printable White Soft Sleeves<br></p><p>Brand: <b>EPC</b><br></p><p>Packing Roll:\r\n<b>100 Meter Roll&nbsp; (1 Coil)</b></p><p>Color<b> : White </b><br></p><p>Condition:<b>\r\nBrand New Soft Sleeves</b><br></p><p>Feature: Long Services Life / Compact design / Easy to use</p>\r\n\r\n\r\n<p></p><p>Our organization is known in the industry for catering a distinguished segment of <b>Marking sleeves</b>. These <b>Stickers,White Sleeve</b>\r\n products are manufactured using superior grade raw material with the \r\naid of latest technology in compliance with set international quality \r\nstandards.<br></p>', '[\"1 Coil MTRS\",\"Per Coil Each\",\"Diss 37.50%\",\"GST 18%\"]', 4, '2018-09-15 14:01:00'),
(18, '3D Rectangular Pin Type Terminals Non-Insulated', 13, 'uploads/vendor/product/item_20180916075115_4.png', '<p>\r\n</p><div><p>Rectangular Pin Type Copper Cable Terminal Ends</p><ul><li>Brand: <b>3D</b></li><li>Packing Oty: <b>200 Nos <br></b></li><li>Material: <b>E-Copper</b></li><li>Finish: <b>Tin Plated</b></li><li>Condition:<b> Brand New Certified Approvals&nbsp;&nbsp;</b></li></ul><b><b>For Loose Qty Buying Discounts will Be Reduced Up-to 3 % Item wise.</b>&nbsp;</b></div><div><br><b></b><b></b></div>This type of terminal ends are mainly used for urination \r\nflexible wires, Cords, Control cables, Meters of control \r\npanel/switchboard, Contractors etc, according to the requirement\'s. They\r\n are made of High conductivity copper and are for Crimping.\r\n     			 <div>&nbsp; Rectangular Pin type copper cable terminal ends (non-insulated)</div><div><br></div><div>For Industrial Use Only</div>\r\n\r\n<br><p></p>', '[\"Marking Size\",\"Nos In Pkt\",\"Per Each\",\"Diss 37.50%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-16 07:51:15'),
(19, 'Insulated Ferrules End Cord Terminals', 13, 'uploads/vendor/product/item_20180918041533_4.jpg', '<p>\r\n</p><div><p>Ferrules Insulated Lugs&nbsp; <br></p><ul><li>Brand: <b>Jigo / Control<br></b></li><li>Packing Oty: <b>100 Nos (1 Pkt)<br></b></li><li>Material: <b>E-Copper</b></li><li>Finish: <b>Tin Plated</b></li><li>Condition:<b> Brand New Certified Approvals <br></b></li></ul><b><b></b></b></div><div><br><b></b><b></b></div>This type of terminal ends are mainly used for urination \r\nflexible wires, Cords, Control cables, Meters of control \r\npanel/switchboard, Contractors etc, according to the requirement\'s. They\r\n are made of High conductivity copper and are for Crimping.\r\n     			 <div>&nbsp;End Cord Terminals (Insulated Ferrules)<br></div><div><br></div><div>For Industrial Use Only</div>\r\n\r\n\r\n\r\n<br><p></p>', '[\"Size MM X Length\",\"Nos In Pkt\",\"Per Each\",\"Diss 37.50%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-16 08:55:14'),
(25, 'Jainson Non-Insulated Crimping Tools', 17, 'uploads/vendor/product/item_20180924010344_7.JPG', '<p>Non-Insulated Terminal Crimping Tools <br></p><p><br> \r\n</p><li>Brand: <b>Jainson</b></li><li>Packing Oty: <b>1 Nos (You Buying Ase Per Specification Price)<br></b></li><li>Weight &amp; Capacity: <b>Ase Per Photo View&nbsp; </b></li><li>Color Finish: <b>Ase Per Photo View<br></b></li><li>Condition:<b> Brand New Certified Approval</b></li><p><b>Features:</b> High strength / Excellent finishing / Dimensional accuracy</p><p>\r\n</p><div><b>Usage</b>:  Industrial,  Garage,Workshop.<br></div>\r\n\r\nOur company has been successful in winning appreciation from the clients\r\n as one of the most prominent names in the trading of Jainson Tools.<br><p></p>', '[\"Nos In Pkt\",\"Per Each \",\"Diss 30%\",\"GST 18%\"]', 7, '2018-09-24 13:03:44'),
(26, 'Epoxy / DMC Insulators & Busbar Support', 18, 'uploads/vendor/product/item_20180928074556_4.JPG', '<p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p>Epoxy\r\nInsulators Bus-bar </p>\r\n\r\n\r\n\r\n<p>Type: <b>Cone\r\nType Insulators</b></p>\r\n\r\n<p>Packing\r\nOty: <b>100 Nos (As Per Catalogue Packing Price) </b></p>\r\n\r\n<p>Material:<b>&nbsp;Epoxy\r\nDMC, BMC, SMC </b></p>\r\n\r\n<p>Colour\r\nFinish: <b>Red</b></p>\r\n\r\n<p>Condition:<b>\r\nBrand New Certified Approval </b></p>\r\n\r\n<p>Insert:<b>\r\nBrass </b></p>\r\n\r\n\r\n\r\n<p>EPOXY is\r\na versatile thermo o set polymer that gives excellent properties for various\r\napplications. EPOXY can be used for applications such as flooring, adhesive,\r\nepoxy composites, paint, coatings and many more applications </p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>', '[\"Hight X Dia X Insert\",\"Nos In Pkt\",\"Per Each\",\"Diss 25%\",\"Line Total\",\"GST 18%\"]', 4, '2018-09-28 07:45:56'),
(27, 'Nylon Spacer With Taping Thread', 19, 'uploads/vendor/product/item_20181003065415_4.jpg', '<p>Nylon Hex Spacer Female</p><p>Shape:<b> Hex Type Spacer</b><br></p><p>\r\n</p><p>Color: <b> BLACK</b></p><p>Material: <b>Nylon 66 Black UL 94 - V2\r\n\r\n</b></p><p>Nos In Pkt: <b>100 Nos</b></p><p><b></b>Insert<b>: Only Taping Thread In MM</b><br></p>\r\n\r\n<br><p></p>', '[\"Thread X Length X A\\/F\",\"Nos In Pkt\",\"Per Each\",\"Diss 30%\",\"Line Total\",\"GST 18%\"]', 4, '2018-10-03 06:54:15'),
(28, 'Ceramic Beads Round Type', 20, 'uploads/vendor/product/item_20181005081149_4.jpg', '<p>Ceramic Beads</p><p>Shape:<b> Round Type</b></p><p>Color: <b>White</b></p><p>Packing Qty: <b>1000 Nos In PKT</b></p><p>Size: <b>In MM Hight</b></p><p></p><p>Throw Hole Ceramic Beads<br></p><br>', '[\"Hight MM\",\"Packing In Pkt\",\"Per Pkt\",\"Diss 30%\",\"GST 18%\"]', 4, '2018-10-05 08:11:49');

-- --------------------------------------------------------

--
-- Table structure for table `vndr_product_type_images`
--

CREATE TABLE `vndr_product_type_images` (
  `img_id` bigint(20) UNSIGNED NOT NULL,
  `img_type_id` bigint(25) DEFAULT NULL,
  `img_path` varchar(255) DEFAULT NULL,
  `img_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vndr_product_type_images`
--

INSERT INTO `vndr_product_type_images` (`img_id`, `img_type_id`, `img_path`, `img_date`) VALUES
(1, 5, 'uploads/vendor/product/item_20180726125436_4.jpg', '2018-07-26 12:54:36'),
(2, 8, 'uploads/vendor/product/item_20180727064233_4.jpg', '2018-07-27 06:42:33'),
(3, 9, 'uploads/vendor/product/item_20180729123832_4.jpg', '2018-07-29 12:38:32'),
(4, 10, 'uploads/vendor/product/item_20180815070213_4.jpg', '2018-08-15 07:02:13'),
(5, 10, 'uploads/vendor/product/item_20180815070231_4.jpg', '2018-08-15 07:02:31'),
(6, 11, 'uploads/vendor/product/item_20180819032654_4.jpg', '2018-08-19 15:26:54'),
(7, 11, 'uploads/vendor/product/item_20180819032710_4.JPG', '2018-08-19 15:27:10'),
(8, 11, 'uploads/vendor/product/item_20180819032742_4.jpg', '2018-08-19 15:27:42'),
(9, 13, 'uploads/vendor/product/item_20180910051208_4.jpg', '2018-09-10 05:12:08'),
(10, 12, 'uploads/vendor/product/item_20180911034508_4.jpg', '2018-09-11 15:45:08'),
(11, 14, 'uploads/vendor/product/item_20180914054734_4.jpg', '2018-09-14 05:47:34'),
(12, 15, 'uploads/vendor/product/item_20180915112030_4.jpg', '2018-09-15 11:20:30'),
(15, 16, 'uploads/vendor/product/item_20180915124855_4.JPG', '2018-09-15 12:48:55'),
(17, 16, 'uploads/vendor/product/item_20180915125101_4.JPG', '2018-09-15 12:51:01'),
(18, 17, 'uploads/vendor/product/item_20180915023812_4.jpg', '2018-09-15 14:38:12'),
(19, 25, 'uploads/vendor/product/item_20180924010930_7.JPG', '2018-09-24 13:09:30'),
(20, 25, 'uploads/vendor/product/item_20180924012734_7.JPG', '2018-09-24 13:27:34'),
(21, 25, 'uploads/vendor/product/item_20180924013416_7.JPG', '2018-09-24 13:34:16'),
(23, 27, 'uploads/vendor/product/item_20181004062119_4.JPG', '2018-10-04 06:21:19'),
(24, 28, 'uploads/vendor/product/item_20181005082024_4.jpg', '2018-10-05 08:20:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adm_id`),
  ADD UNIQUE KEY `adm_id` (`adm_id`);

--
-- Indexes for table `admin_delivery_charges`
--
ALTER TABLE `admin_delivery_charges`
  ADD PRIMARY KEY (`adc_id`),
  ADD UNIQUE KEY `adc_id` (`adc_id`);

--
-- Indexes for table `admin_delivery_time`
--
ALTER TABLE `admin_delivery_time`
  ADD PRIMARY KEY (`adt_id`),
  ADD UNIQUE KEY `adt_id` (`adt_id`);

--
-- Indexes for table `banquet`
--
ALTER TABLE `banquet`
  ADD PRIMARY KEY (`bq_id`),
  ADD UNIQUE KEY `bq_id` (`bq_id`);

--
-- Indexes for table `banquet_photos`
--
ALTER TABLE `banquet_photos`
  ADD PRIMARY KEY (`bq_p_id`),
  ADD UNIQUE KEY `bq_p_id` (`bq_p_id`);

--
-- Indexes for table `banquet_section`
--
ALTER TABLE `banquet_section`
  ADD PRIMARY KEY (`section_id`),
  ADD UNIQUE KEY `section_id` (`section_id`);

--
-- Indexes for table `bq_section_amenities`
--
ALTER TABLE `bq_section_amenities`
  ADD PRIMARY KEY (`sec_amenity_id`),
  ADD UNIQUE KEY `sec_amenity_id` (`sec_amenity_id`);

--
-- Indexes for table `bq_section_appointment`
--
ALTER TABLE `bq_section_appointment`
  ADD PRIMARY KEY (`app_id`),
  ADD UNIQUE KEY `app_id` (`app_id`);

--
-- Indexes for table `bq_section_photos`
--
ALTER TABLE `bq_section_photos`
  ADD PRIMARY KEY (`sec_photo_id`),
  ADD UNIQUE KEY `sec_photo_id` (`sec_photo_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`dr_id`),
  ADD UNIQUE KEY `dr_id` (`dr_id`);

--
-- Indexes for table `dr_appointment`
--
ALTER TABLE `dr_appointment`
  ADD PRIMARY KEY (`app_id`),
  ADD UNIQUE KEY `app_id` (`app_id`);

--
-- Indexes for table `dr_category`
--
ALTER TABLE `dr_category`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_id` (`cat_id`);

--
-- Indexes for table `dr_clinic_photo`
--
ALTER TABLE `dr_clinic_photo`
  ADD PRIMARY KEY (`dr_cl_id`),
  ADD UNIQUE KEY `dr_cl_id` (`dr_cl_id`);

--
-- Indexes for table `dr_schedule`
--
ALTER TABLE `dr_schedule`
  ADD PRIMARY KEY (`sc_id`),
  ADD UNIQUE KEY `sc_id` (`sc_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`o_id`),
  ADD UNIQUE KEY `o_id` (`o_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`oi_id`),
  ADD UNIQUE KEY `oi_id` (`oi_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `setting_id` (`setting_id`);

--
-- Indexes for table `timeslot`
--
ALTER TABLE `timeslot`
  ADD PRIMARY KEY (`time_id`),
  ADD UNIQUE KEY `time_id` (`time_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`v_id`),
  ADD UNIQUE KEY `v_id` (`v_id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`v_id`),
  ADD UNIQUE KEY `v_id` (`v_id`);

--
-- Indexes for table `visitor_company`
--
ALTER TABLE `visitor_company`
  ADD PRIMARY KEY (`uc_id`),
  ADD UNIQUE KEY `uc_id` (`uc_id`);

--
-- Indexes for table `vndr_brand`
--
ALTER TABLE `vndr_brand`
  ADD PRIMARY KEY (`brand_id`),
  ADD UNIQUE KEY `brand_id` (`brand_id`);

--
-- Indexes for table `vndr_categoty`
--
ALTER TABLE `vndr_categoty`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_id` (`cat_id`);

--
-- Indexes for table `vndr_company`
--
ALTER TABLE `vndr_company`
  ADD PRIMARY KEY (`vc_id`),
  ADD UNIQUE KEY `vc_id` (`vc_id`);

--
-- Indexes for table `vndr_order`
--
ALTER TABLE `vndr_order`
  ADD PRIMARY KEY (`o_id`),
  ADD UNIQUE KEY `o_id` (`o_id`);

--
-- Indexes for table `vndr_order_item`
--
ALTER TABLE `vndr_order_item`
  ADD PRIMARY KEY (`o_item_id`),
  ADD UNIQUE KEY `o_item_id` (`o_item_id`);

--
-- Indexes for table `vndr_product`
--
ALTER TABLE `vndr_product`
  ADD PRIMARY KEY (`p_id`),
  ADD UNIQUE KEY `p_id` (`p_id`);

--
-- Indexes for table `vndr_product_images`
--
ALTER TABLE `vndr_product_images`
  ADD PRIMARY KEY (`img_id`),
  ADD UNIQUE KEY `img_id` (`img_id`);

--
-- Indexes for table `vndr_product_type`
--
ALTER TABLE `vndr_product_type`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `type_id` (`type_id`);

--
-- Indexes for table `vndr_product_type_images`
--
ALTER TABLE `vndr_product_type_images`
  ADD PRIMARY KEY (`img_id`),
  ADD UNIQUE KEY `img_id` (`img_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_delivery_charges`
--
ALTER TABLE `admin_delivery_charges`
  MODIFY `adc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_delivery_time`
--
ALTER TABLE `admin_delivery_time`
  MODIFY `adt_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banquet`
--
ALTER TABLE `banquet`
  MODIFY `bq_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `banquet_photos`
--
ALTER TABLE `banquet_photos`
  MODIFY `bq_p_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banquet_section`
--
ALTER TABLE `banquet_section`
  MODIFY `section_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bq_section_amenities`
--
ALTER TABLE `bq_section_amenities`
  MODIFY `sec_amenity_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bq_section_appointment`
--
ALTER TABLE `bq_section_appointment`
  MODIFY `app_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `bq_section_photos`
--
ALTER TABLE `bq_section_photos`
  MODIFY `sec_photo_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `dr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `dr_appointment`
--
ALTER TABLE `dr_appointment`
  MODIFY `app_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dr_category`
--
ALTER TABLE `dr_category`
  MODIFY `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `dr_clinic_photo`
--
ALTER TABLE `dr_clinic_photo`
  MODIFY `dr_cl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dr_schedule`
--
ALTER TABLE `dr_schedule`
  MODIFY `sc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `o_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `oi_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `timeslot`
--
ALTER TABLE `timeslot`
  MODIFY `time_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `v_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `v_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `visitor_company`
--
ALTER TABLE `visitor_company`
  MODIFY `uc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vndr_brand`
--
ALTER TABLE `vndr_brand`
  MODIFY `brand_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vndr_categoty`
--
ALTER TABLE `vndr_categoty`
  MODIFY `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vndr_company`
--
ALTER TABLE `vndr_company`
  MODIFY `vc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vndr_order`
--
ALTER TABLE `vndr_order`
  MODIFY `o_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vndr_order_item`
--
ALTER TABLE `vndr_order_item`
  MODIFY `o_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vndr_product`
--
ALTER TABLE `vndr_product`
  MODIFY `p_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `vndr_product_images`
--
ALTER TABLE `vndr_product_images`
  MODIFY `img_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `vndr_product_type`
--
ALTER TABLE `vndr_product_type`
  MODIFY `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `vndr_product_type_images`
--
ALTER TABLE `vndr_product_type_images`
  MODIFY `img_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
