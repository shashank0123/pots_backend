function getLocation() {
  if(navigator.geolocation) {

      $('#userLocation').val('Fetching Your Location..');
      $('#findRestaurants').addClass('is-loading');

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, {maximumAge:60000, timeout:5000, enableHighAccuracy:true});
              } else {
                  $toastr.warning("Geolocation is not supported by this browser.");
              }
}

function geoSuccess(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;


    codeLatLng(lat, lng);
}


function geoError() {
    $('#userLocation').val('');
    $('#findRestaurants').removeClass('is-loading');
    $$toastr.error("There was some problem fetching your location please try again!");
}


function codeLatLng(lat, lng) {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if(status == google.maps.GeocoderStatus.OK) {
          console.log(results)
          if(results[1]) {
              //formatted address
              var address = results[0].formatted_address;


               $('#userLocation').val(address);

                var userLocation = { 'lat' : lat, 'lng' : lng, 'address' : address, 'city' :  results[1].long_name};


                 localStorage.setItem("userLocation", JSON.stringify(userLocation));

                  axios.post('/user/location', {lat: lat, lng: lng, address: address})
                    .then(function(response){
                     // $toastr.success('');
                     location.href = '/restaurants/explore';
                    }).catch(function (error) {
                      // handle error
                      $toastr.warning('We were not able to get your location! Please try again.');
                      console.log(error);
                    });



          } else {
              $$toastr.warning('No Results Found!')
          }
      } else {
          $$toastr.error("Geocoder failed due to: " + status);
      }
    });
}

function codeAddress() {
   geocoder = new google.maps.Geocoder();
    var address = document.getElementById('userLocation').value;
    if(address == '')
    {
      $toastr.warning('Please enter your location');
    } else {
      geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {

          var position = results[0].geometry.location

          var userLocation = { 'lat' : position.lat(), 'lng' : position.lng(), 'address' : address, 'city' :  results[0].address_components[0].long_name};
          localStorage.setItem("userLocation", JSON.stringify(userLocation));

          $('#findRestaurants').addClass('is-loading');
          axios.post('/user/location', {lat: position.lat(), lng: position.lng(), address: address})
          .then(function(response){
           // $toastr.success('');
           $('#findRestaurants').removeClass('is-loading');
           location.href = '/restaurants/explore';
          }).catch(function (error) {
            // handle error
            $('#findRestaurants').removeClass('is-loading');
            $toastr.warning('We were not able to get your location! Please try again.');
            console.log(error);
          });



      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
    }

  }
