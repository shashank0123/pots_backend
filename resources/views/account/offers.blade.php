@extends('layouts.app')

@section('title', 'Offers | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('account._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow account-user-offer">
                        <div class="card-body">
                           <p><i class="fa fa-info-circle"></i> You can see all offers here</p>
                             <div class="list-group coupons-list mb-5">
                                @foreach($coupons as $coupon)
                                    <div class="list-group-item pt-4 list-group-item-action">
                                        <div>
                                            <span class="coupon-code">{{ $coupon->code }}</span>
                                        </div>
                                        <div>
                                            <p>{{ $coupon->promo_text }}</p>
                                        </div>
                                     </div>
                                @endforeach
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

