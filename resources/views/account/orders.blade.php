@extends('layouts.app')

@section('title', 'Order | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('account._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow user-order-account">
                        <div class="card-body">
                                @foreach(auth()->user()->orders as $order)
                                    <div class="card mb-4">
                                    <div class="card-body">
                                       <div class="d-flex">
                                        <img class="mr-4 order-restaurant-img" src="{{ isset($order->restaurant->logo) ? url($order->restaurant->logo) : 'http://via.placeholder.com/84x84' }}" alt="Generic placeholder image">
                                        <div class="">
                                          <h5 class="mt-0">{{ $order->restaurant->name }} {!! $order->status_badge !!}</h5>
                                           <p class="restaurant-area">{{ $order->restaurant->area }}</p>
                                           <p class="restaurant-created">Order #{{$order->id}} | {{ $order->created_at }}</p>
                                           <p class="restaurant-deatails"><a href="/orders/{{$order->id}}">View Details</a></p>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

