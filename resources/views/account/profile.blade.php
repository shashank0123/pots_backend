@extends('layouts.app')

@section('title', 'Profile | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('account._sidebar')

                </div>

                <div class="col-md-9 pb-5">

                    <div class="card card-shadow account-profile-user">
                        <div class="card-body">
                            <form method="POST" action="/account/update">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                       <h4>Account Details</h4>
                                     </div>
                                     <div class="form-group col-sm-6">
                                       <label for="first_name">First name</label>
                                       <input id="first_name" type="name" value="{{ auth()->user()->first_name }}" placeholder="Enter your first name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                         @if ($errors->has('first_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6">
                                       <label for="last_name">Last name</label>
                                       <input id="last_name" type="name" value="{{ auth()->user()->last_name }}" placeholder="Enter your last name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                         @if ($errors->has('last_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                       <label for="email">Email address (optional)</label>
                                       <input id="email" type="email" value="{{ auth()->user()->email }}" placeholder="Enter contact e-mail address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  >

                                         @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                 </div>

                                 <div class="row">
                                    <div class="col-sm-4">
                                       <p> <button type="submit" class="btn btn-dark">Update Profile</button>  </p>
                                    </div>

                                 </div>
                            </form>

                            <div class="row mt-3">
                                     <div class="col-sm-12">
                                       <h4>Phone number</h4>
                                     </div>

                                     <div class="col-sm-12">
                                         <update-phone phone="{{auth()->user()->phone}}"></update-phone>
                                     </div>
                             </div>

                             <div class="row mt-4">


                                  <div class="col-sm-12">
                                        <form method="POST" action="/account/update/password">
                                @csrf
                                 <div class="row">
                                      <div class="col-sm-12">
                                         <h4>Account Password</h4>
                                       </div>
                                     <div class="form-group col-sm-12">
                                       <label for="old_password">Old Password</label>
                                       <input id="old_password" type="password"  placeholder="Enter your old password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password"  required autofocus>

                                         @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-6">
                                       <label for="password">New Passowrd</label>

                                       <input id="password" type="password"  placeholder="Enter new password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                         @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                     <div class="form-group col-sm-6">
                                       <label for="password">Confirm New Passowrd</label>

                                       <input id="password_confirmation" type="password"  placeholder="Confirm new password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                                         @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                 </div>



                                 <div class="row">
                                    <div class="col-sm-4">
                                       <p> <button type="submit" class="btn btn-warning">Update Password</button>  </p>
                                    </div>

                                 </div>


                    </form>
                                  </div>

                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

