@extends('layouts.app')

@section('title', 'Payment | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('account._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow">
                        <div class="card-body">
                            <h4 class="foodoor-cash-card"><b>Pots Cash :</b> <span>{{ auth()->user()->foodoor_cash }}</span></h4>
                            <hr class="my-3">
                            <p><i class="fa fa-info-circle"></i> On every successfull order you receive 5% pots cash.</p>
                            <p><i class="fa fa-info-circle"></i> 10% of your Pots money is always reduced in your billing amount. T&amp;C applied.</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

