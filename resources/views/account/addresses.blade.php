@extends('layouts.app')

@section('title', 'Addresses | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">


                            @include('account._sidebar')


                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow">
                        <div class="card-body">
                             <div class="row">
                                @if(auth()->check())
                                @foreach(auth()->user()->addresses as $address)
                                  <div class="col-md-6">
                                    <div class="card address">
                                        <div class="card-body pt-1">
                                            <span class="badge badge-warning">{{ $address->type_string }}</span>
                                            <h6 class="font-weight-bold mt-2">{{ str_limit($address->location, 70) }}</h6>
                                            <p><b>Door No.: </b> {{ $address->door_no }} | {{ $address->road_name }}
                                            <br><b>Landmark : </b> {{ $address->landmark }}</p>

                                            <p class="mb-0"><a data-toggle="modal" href="#editAddress-{{$address->id}}">Edit</a> | <a href="/addresses/{{$address->id}}/delete" onclick="return confirm('Are you sure, you want to delete the address');">Delete</a></p>

                                            @include('checkout.partials._editaddress')
                                        </div>
                                    </div>
                                  </div>
                                @endforeach
                                @endif
                                </div>
                                <button data-toggle="modal" data-target="#addressModal" class="btn btn-primary mt-4">Add New Address</button>
                                @include('checkout.partials._addaddress')
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

