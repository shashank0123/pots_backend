 <div class="list-group account-sidebar-menu restaurant-filters">
  <a href="/my-account" class="list-group-item list-group-item-action {{ request()->is('my-account') ? 'active' : '' }}"><i class="fas fa-user mr-2"></i> Profile</a>
  <a href="/my-account/orders" class="list-group-item list-group-item-action {{ request()->is('my-account/orders') ? 'active' : '' }}"><i class="fas fa-file-invoice mr-2"></i> Orders</a>
  <a href="/my-account/offers" class="list-group-item list-group-item-action {{ request()->is('my-account/offers') ? 'active' : '' }}"><i class="fas fa-qrcode mr-2"></i> Offers</a>
  <a href="/my-account/bookmarks" class="list-group-item list-group-item-action {{ request()->is('my-account/bookmarks') ? 'active' : '' }}"><i class="fas fa-star mr-2"></i> Bookmarks</a>
  <a href="/my-account/payments" class="list-group-item list-group-item-action {{ request()->is('my-account/payments') ? 'active' : '' }}"><i class="fas fa-credit-card mr-2"></i> Payments</a>
  <a href="/my-account/addresses" class="list-group-item list-group-item-action {{ request()->is('my-account/addresses') ? 'active' : '' }}"><i class="fas fa-map-marked-alt mr-2"></i> Addresses</a>
  <a class="list-group-item list-group-item-action" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fas fa-sign-out-alt mr-2"></i>  {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

</div>



        <button class="btn btn-primary show-filters" data-toggle="modal" data-target="#filterRestaurantsModal" style="display: none;z-index: 1000000;">
            <i class="fa fa-bars"></i> My Account
        </button>


<div class="modal fade" id="filterRestaurantsModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">My Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="list-group restaurant-filters">
  <a href="/my-account" class="list-group-item list-group-item-action {{ request()->is('my-account') ? 'active' : '' }}"><i class="fas fa-user mr-2"></i> Profile</a>
  <a href="/my-account/orders" class="list-group-item list-group-item-action {{ request()->is('my-account/orders') ? 'active' : '' }}"><i class="fas fa-file-invoice mr-2"></i> Orders</a>
  <a href="/my-account/offers" class="list-group-item list-group-item-action {{ request()->is('my-account/offers') ? 'active' : '' }}"><i class="fas fa-qrcode mr-2"></i> Offers</a>
  <a href="/my-account/bookmarks" class="list-group-item list-group-item-action {{ request()->is('my-account/bookmarks') ? 'active' : '' }}"><i class="fas fa-star mr-2"></i> Bookmarks</a>
  <a href="/my-account/payments" class="list-group-item list-group-item-action {{ request()->is('my-account/payments') ? 'active' : '' }}"><i class="fas fa-credit-card mr-2"></i> Payments</a>
  <a href="/my-account/addresses" class="list-group-item list-group-item-action {{ request()->is('my-account/addresses') ? 'active' : '' }}"><i class="fas fa-map-marked-alt mr-2"></i> Addresses</a>
  <a class="list-group-item list-group-item-action" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fas fa-sign-out-alt mr-2"></i>  {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

</div>
      </div>

    </div>
  </div>
</div>