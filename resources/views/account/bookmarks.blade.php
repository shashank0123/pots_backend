@extends('layouts.app')

@section('title', 'Addresses | Pots - Food at your door step')

@section('content')

    <div class="bg-light fixed-card-height">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('account._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow">
                        <div class="card-body">
                           <p><i class="fa fa-info-circle"></i> You can bookmark restaurants you like and have them listed here</p>
                            <div class="row">
                                 @foreach(auth()->user()->bookmarks as $restaurant)
                                  <div class="col-md-4">
                                     @include('restaurants.partials._card')
                                  </div>
                                  @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

