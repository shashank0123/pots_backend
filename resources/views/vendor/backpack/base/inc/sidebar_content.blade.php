<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

<li class="header">Sales</li>
<li><a href="{{ backpack_url('earnings') }}"><i class="fa fa-money"></i> <span>Earnings</span></a></li>

<li class="header">Manage Orders</li>
<li><a href="{{ backpack_url('todays-orders') }}"><i class="fa fa-file-text"></i> <span>Today's Orders</span></a></li>
<li><a href="{{ backpack_url('offline-orders') }}"><i class="fa fa-building"></i> <span>Offline Orders</span></a></li>
<li><a href="{{ backpack_url('order-history') }}"><i class="fa fa-list-ol"></i> <span>Orders History</span></a></li>
<li><a href="{{ backpack_url('cancelled-orders') }}"><i class="fa fa-trash"></i> <span>Cancelled Orders</span></a></li>


<li class="header">Site Data</li>
<li><a href="{{ backpack_url('customers') }}"><i class="fa fa-users"></i> <span>Customers</span></a></li>
<li><a href="{{ backpack_url('coupons') }}"><i class="fa fa-qrcode"></i> <span>Coupons</span></a></li>
<li><a href="{{ backpack_url('restaurants') }}"><i class="fa fa-building"></i> <span>Restaurants</span></a></li>
<li><a href="{{ backpack_url('delivery-boys') }}"><i class="fa fa-motorcycle"></i> <span>Delivery Boys</span></a></li>


<li class="header">Site Content</li>
<li><a href='{{ backpack_url('cities') }}'><i class='fa fa-map-marker'></i> <span>Cities</span></a></li>
<li><a href='{{ backpack_url('cuisines') }}'><i class='fa fa-th'></i> <span>Cuisines</span></a></li>
<li><a href='{{ backpack_url('home-banners') }}'><i class='fa fa-image'></i> <span>Home Page Banners</span></a></li>
<li><a href='{{ backpack_url('offer-banners') }}'><i class='fa fa-image'></i> <span>Offer Banners</span></a></li>

<li class="header">Site Backend</li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i> <span>Settings</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}'><i class='fa fa-hdd-o'></i> <span>Backups</span></a></li>
<li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
