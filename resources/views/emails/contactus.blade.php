@component('mail::message')
# New Contact Information

You have received a new contact information at Pots!

### Name : {{ $name }}

### Phone : <a href="tel:{{$phone}}">{{ $phone }}</a>

### Email : <a href="mailto:{{$email}}">{{ $email }}</a>

### Message : {{ $message }}

@component('mail::button', ['url' => 'https://foodoor.in'])
Visit Site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
