@extends('layouts.app')


@section('tracking')

    <!-- Event snippet for Order Completion (AdWords) conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-792677172/wQ-aCIzjpIsBELSW_fkC',
      'transaction_id': ''
  });
</script>

@endsection


@section('content')

<div style="height: 80vh;" class="d-flex align-items-center justify-content-center">
    <div class="text-center">
        <img src="/img/success.png" style="width: 150px;height: 100%;display: block;margin: 0 auto;margin-bottom: 15px;" alt="restaurant" />
        <h3 class="font-weight-bold" style="letter-spacing: 2px;">Thank you placing an order!</h3>
        <p class="text-muted"  style="font-size: 19px;letter-spacing: 1px;">Your order will shortly arrive to you. Click the button below to track your order.</p>
        <a href="/orders/{{$order->id}}" class="btn btn-primary">Track My Order</a>
    </div>
</div>

@endsection