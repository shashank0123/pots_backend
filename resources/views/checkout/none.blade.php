@extends('layouts.app')

@section('content')

    <div style="height: 80vh;" class="d-flex align-items-center justify-content-center">
        <div class="text-center">
            <img src="/img/empty.png" style="width: 150px;height: 100%;display: block;margin: 0 auto;margin-bottom: 15px;" alt="restaurant" />
            <h3 class="font-weight-bold" style="letter-spacing: 2px;">Your cart is empty!</h3>
            <p class="text-muted" style="font-size: 19px;letter-spacing: 1px;">Explore our restaurants and add some items in your cart</p>
            <a href="/restaurants/explore" class="btn btn-primary">Explore Restaurants</a>
        </div>
    </div>

@endsection