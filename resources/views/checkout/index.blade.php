@extends('layouts.app')



@section('content')

    <div class="bg-light checkout" style="min-height: 90vh;">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-8">

                    <div v-if="loggedIn" class="card address-card card-shadow">
                      <div class="card-body">
                        <h4 class="font-weight-bold mb-4">Delivery Address</h4>
                        <el-alert
                                  v-if="checkoutError"
                                  :title="checkoutErrorMessage"
                                  type="error"
                                  class="mb-2 mt-2"
                                  show-icon>
                                </el-alert>

                        @if(auth()->check() && auth()->user()->addresses()->count())
                          @include('checkout.partials._addresscarousel')
                        @endif
                       <div class="row address-col">
                        @if(auth()->check())

                        @foreach(auth()->user()->addresses as $address)
                          <div class="col-md-6">
                            <div class="card address">
                                <div class="card-body pt-1">
                                    <span class="badge badge-warning">{{ $address->type_string }}</span>
                                    <h6 class="font-weight-bold mt-2">{{ str_limit($address->location, 70) }}</h6>
                                    <p><b>Door No.: </b> {{ $address->door_no }} | {{ $address->road_name }}
                                    <br><b>Landmark : </b> {{ $address->landmark }}</p>
                                    <p ><a data-toggle="modal" href="#editAddress-{{$address->id}}">Edit</a> | <a href="/addresses/{{$address->id}}/delete" onclick="return confirm('Are you sure, you want to delete the address');">Delete</a></p>



                                     <el-radio @change="checkoutError = false"  v-model="checkout.address_id" label="{{ $address->id }}" border>Deliver Here</el-radio>
                                    </button>
                                </div>
                            </div>
                          </div>
                        @endforeach
                        @endif
                        </div>
                        <button data-toggle="modal" data-target="#addressModal" class="btn btn-primary mt-4">Add New Address</button>
                        @include('checkout.partials._addaddress')
                      </div>
                    </div>
                    <div v-else class="card account-card  card-shadow">
                        <div class="card-body">
                            <h4 class="font-weight-bold mb-4">Account</h4>
                            <div class="login-container" style="width: 60%;">
                             <login-register @loggedin="changeLoginStatus"></login-register>
                            </div>
                        </div>
                    </div>

                    <div v-if="loggedIn" class="card payment-card card-shadow">
                        <div class="card-body">
                            <h4 class="font-weight-bold mb-4">Payment</h4>
                            <div>
                              <input type="hidden" id="address" name="address" v-model="checkout.address_id">

                              <div>
                                <el-radio v-model="checkout.payment_mode" :label="0" border>COD</el-radio>
                                <el-radio v-model="checkout.payment_mode" :label="1" border>Pay Online</el-radio>
                              </div>

                              <div class="mt-4 place-order-section">
                                <el-input type="number" class="alt_mobile_input" v-model="checkout.delivery_alt_no" placeholder="Alternate Mobile Number (optional)"></el-input>
                                <el-button class="place-order-btn" @click="placeOrder()" type="success" :loading="placingOrder">Place Order</el-button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-4">
                    <div class="card cart  card-shadow">
                        <div class="card-body">
                        <div class="d-flex">
                            <img src="{{ isset($restaurant->logo) ? url($restaurant->logo) : 'http://via.placeholder.com/90x90' }}" style="width: 50px;height: 50px;" alt="restaurants feature">
                            <div class="ml-2">
                                <h5 class="restaurant-name"><a class="text-dark" href="/restaurants/{{ str_slug($restaurant->city->name) }}/{{$restaurant->slug}}">{{ $restaurant->name }}</a></h5>
                                <p class="restaurant-area">{{ $restaurant->area }}</p>
                            </div>
                        </div>

                        <div v-if="itemsCount == 0" class="cart-empty-view">
                            <p>There are no items in your cart</p>
                        </div>

                        <a v-else href="#cartModal" data-toggle="modal" class="view-cart" style="display: none;">View Detailed Cart</a>

                        <div v-if="itemsCount > 0"  class="cart-items my-3">
                            <div class="">
                            <div v-for="item in cartItems" class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                 <img v-if="item.is_veg" class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="restaurant">

                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="restaurant">

                                               @{{ item.name }}</h4>
                                        <p class="price">₹ @{{ item.price * item.quantity }}</p>
                                    </div>

                                    <input type="number" class="number-input" :value="item.quantity" :id="item.id+'-qty'" @change="updateCartItem(item.id, item.attributes.item_id)" >
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            </div>
                        </div>
                        <div class="coupons-button">
                            <a v-if="bill.discount == null" href="#couponsModal" data-toggle="modal" class="btn btn-secondary">Apply Coupon</a>
                            <buttom @click="removeCoupon()" v-else class="btn btn-danger"><i class="fas fa-trash"></i> Remove Coupon</button>
                        </div>
                         <div class="suggestions">
                            <input class="form-control" placeholder="Any suggestions for restaurant?" name="suggestions" v-model="checkout.suggestions" id="suggestions" />
                        </div>
                        <div class="cart-bill">
                            <table class="table table-borderless">
                                 <tbody>
                                    <tr>
                                      <td>Subtotal</td>
                                      <td class="amount">₹ @{{ bill.subTotal }}</td>
                                    </tr>
                                    <tr>
                                      <td>GST</td>
                                      <td class="amount">₹ @{{ bill.gst }}</td>
                                    </tr>
                                    <tr class="delivery">
                                      <td>Delivery Charges</td>
                                      <td class="amount">₹ @{{ bill.deliveryCharges }}</td>
                                    </tr>
                                     <tr v-if="bill.discount != null" class="discount">
                                      <td>Discount</td>
                                      <td class="amount">- ₹ @{{ bill.discount }}</td>
                                    </tr>
                                     <tr class="total">
                                      <td>To Pay</td>
                                      <td class="amount">₹ @{{ bill.total }}</td>
                                    </tr>
                                 </tbody>
                            </table>
                        </div>
                       </div>

                    </div>
                </div>
            </div>

             <el-button class="confirm-order" @click="placeOrder()" type="success" :loading="placingOrder"  style="display: none;">
            Place Order
        </el-button>
        </div>
    </div>



    <div class="modal fade" id="cartModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Your Cart <br> <small class="count text-muted">@{{ cartItems.length }} ITEMS</small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="cart">

                        <div v-if="itemsCount == 0" class="cart-empty-view">
                            <p>There are no items in your cart</p>
                        </div>

                        <div v-else>
                        <div  class="card cart-items my-3">
                            <div class="card-body">
                            <div v-for="item in cartItems" class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                 <img v-if="item.is_veg" class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="restaurant">

                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="restaurant">

                                               @{{ item.name }}</h4>
                                        <p class="price">₹ @{{ item.price * item.quantity }}</p>
                                    </div>

                                    <input type="number" class="number-input" :value="item.quantity" :id="item.id+'-qty-modal'" @change="updateModalCartItem(item.id, item.attributes.item_id)" >
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            </div>
                        </div>

                        </div>

                    </div>
      </div>

    </div>
  </div>
</div>

@include('checkout.partials._coupons')

@if(auth()->check())
@foreach(auth()->user()->addresses as $address)
   @include('checkout.partials._editaddress')
@endforeach
@endif

@endsection


@section('scripts')



@endsection