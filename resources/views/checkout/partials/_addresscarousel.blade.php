<div class="main-carousel address-carousel"  style="display: none;max-height: 249px;overflow: hidden;" data-flickity='{ "cellAlign": "left", "contain": true, "pageDots": false  }'>
                                   @foreach(auth()->user()->addresses as $address)
                                  <div  class="carousel-cell d-inline-block">
                                      <div class="card address">
                                          <div class="card-body pt-1">
                                              <span class="badge badge-warning">{{ $address->type_string }}</span>
                                              <h6 class="font-weight-bold mt-2">{{ str_limit($address->location, 70) }}</h6>
                                              <p><b>Door No.: </b> {{ $address->door_no }} | {{ $address->road_name }}
                                              <br><b>Landmark : </b> {{ $address->landmark }}</p>
                                              <p ><a data-toggle="modal" href="#editAddress-{{$address->id}}">Edit</a> | <a href="/addresses/{{$address->id}}/delete" onclick="return confirm('Are you sure, you want to delete the address');">Delete</a></p>



                                               <el-radio @change="checkoutError = false"  v-model="checkout.address_id" label="{{ $address->id }}" border>Deliver Here</el-radio>
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                                  @endforeach
                               </div>