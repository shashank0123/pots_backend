@extends('layouts.app')

@section('tracking')

    <!-- Event snippet for Create Account(AdWords) conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-792677172/xf5TCPGHrYsBELSW_fkC',
      'transaction_id': ''
  });
</script>

@endsection

@section('content')

<div style="height: 80vh;" class="d-flex align-items-center justify-content-center">
    <div class="text-center">
        <img src="/img/success.png" style="width: 150px;height: 100%;display: block;margin: 0 auto;margin-bottom: 15px;" />
        <h3 class="font-weight-bold" style="letter-spacing: 2px;">Welcome to pots!</h3>
        <p class="text-muted"  style="font-size: 19px;letter-spacing: 1px;">Thank you for choosing pots! We serve the best food at your doorstep.</p>
        <a href="/checkout" class="btn btn-primary">Go to Checkout</a>
    </div>
</div>

@endsection