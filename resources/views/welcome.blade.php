@extends('layouts.app')

@section('title', 'Home | Pots - Food at your door step')

@section('content')

    <div class="jumbotron hero">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="padding-left: 0px;">
                    <h3 class="hero-heading">Find the best restaurants near you!</h3>
                    <p class="hero-desc">Currently we serve multi-cuisine food items with huge offers only in Patna.</p>
                    <div class="input-group mb-3">
                      <input type="text" id="userLocation" class="form-control location-input" placeholder="Enter your delivery location" aria-label="Enter your delivery location" aria-describedby="button-addon2">
                      <div class="input-group-append">
                        <button class="btn btm-outline-secondary locate-me location-resturant" onclick="getLocation()" type="button"><i class="far fa-compass"></i> Locate Me</button>
                        <button class="btn btn-primary location-resturant-button" type="button" onclick="codeAddress()" id="findRestaurants">Find Restaurants</button>
                      </div>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="main-carousel" style="max-height: 280px;overflow: hidden;" data-flickity='{ "cellAlign": "center", "contain": true, "pageDots": false, "wrapAround": true, "autoPlay": 1500 }'>
                        @foreach($banners as $banner)
                        <div  class="carousel-cell p-1 d-inline-block">
                            <a  href="{{ $banner->url }}">
                            <img style="width: 280px;height: 270px;" src="{{ url($banner->image) }}" />
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="jumbotron process">
        <div class="container">
            <div class="row">
                <div class="col-md-4 step">
                    <img src="/img/steps/choose.png" />
                    <h4>1. Choose Restaurant</h4>
                    <p>Find your favourite restaurants from our long list.</p>
                </div>
                <div class="col-md-4 step">
                     <img src="/img/steps/select.png" />
                     <h4>2. Select items to buy</h4>
                     <p>Add your desired food items to the cart at your fingertips.</p>
                </div>
                <div class="col-md-4 step">
                     <img  src="/img/steps/deliver.png" />
                     <h4>3. Get Food Delivered</h4>
                     <p>Confirm and then live track your order until it reaches to you.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container featured my-md-5">
        <h4 class="title">Featured Restaurants</h4>

        <div class="row">
            @foreach($featured as $restaurant)

                <div class="col-md-4 mb-3">
                    <div class="card featured-item">
                        <a href="/restaurants/{{ str_slug($restaurant->city->name) }}/{{$restaurant->slug}}" class="d-flex">
                            <img src="{{ isset($restaurant->logo) ? url($restaurant->logo) : 'http://via.placeholder.com/90x90' }}" style="width: 70px;height: 70px;">
                            <div class="ml-3">
                                <h5 class="restaurant-name">{{ str_limit($restaurant->name, 20) }}</h5>
                                <p class="restaurant-area">{{ str_limit($restaurant->area, 23) }}</p>
                            </div>
                        </a>
                    </div>
                </div>


            @endforeach
        </div>
    </div>

    <div class="container add-restaurant my-md-5">
        <div class="card bg-light join-restaurants-feature">
            <div class="card-body">
                <div class="d-flex align-items-center restaurants-flex">
                    <div style="flex: 1;">
                    <p>Join the thousands of other restaurants who benefit from having their menus on <b>Pots directory</b>.
                     </p>
                    </div>
                     <button data-toggle="modal" data-target="#restaurantModal" class="btn btn-lg btn-primary">Add My Restaurant</button>
                     @include('layouts.partials._enquiryModal')
                </div>
            </div>
        </div>

    </div>

    <footer class="footer " style="background: #232323 !important;padding: 60px 0;padding-bottom: 30px;">
            <div class="container">
                <!-- top footer statrs -->
                <div class="row top-footer">

                    <div class="col-xs-12 col-sm-3 about color-gray">
                        <h5>Company</h5>
                        <ul>
                            <li><a target="_blank" href="/company/about">About us</a> </li>
                            <li><a target="_blank" href="/company/contact">Contact Us</a> </li>
                            <li><a target="_blank" href="/company/team">Team</a> </li>
                            <li><a target="_blank" href="/company/careers">Careers</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 how-it-works-links color-gray">
                        <h5>Legals</h5>
                        <ul>
                            <li><a target="_blank" href="/company/terms">Terms &amp; Conditions</a> </li>
                            <li><a target="_blank" href="/company/privacy-policy">Privacy Policy</a> </li>

                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                     <h5>Serving locations</h5>
                        <ul>
                            <li><a href="/restaurants/explore">Patna</a> </li>
                        </ul>
                    </div>

                     <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                        <a href="#bulkOrderModal" data-toggle="modal">
                            <img src="/img/bulk.png" alt="Pots bulk order" style="width: 120px;margin-bottom: 20px;">
                        </a>


                    </div>


                </div>
                <!-- top footer ends -->
                <!-- bottom footer statrs -->
                <div class="bottom-footer" style="border-top:  1px solid #fff; padding-top: 20px;">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 payment-options color-gray hidden-sm-down">
                            <a href="#"> <img src="/img/pots-logo.png" alt="Pots white logo" style="width: 180px;"> </a>
                            <!-- <a href="#"> <img src="/img/wordmark-white.png" alt="Pots white logo" style="width: 180px;"> </a> -->
                        </div>
                        <div class="col-xs-12 col-sm-4 address color-gray" style="text-align: center;">
                            <h4 style="color: #fff;margin-top: 7px;">© 2020 Pots | Powered by <a href="https://trumpets.co.in/" class="text-white font-weight-bold" style="letter-spacing: 2px;" target="_blank">Trumpets</a>.</h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 additional-info color-white" style="text-align: center;">
                           <h4 style="color: #fff;font-weight: bold;">
                            <a class="text-white" target="_blank" href="https://www.facebook.com/Ranchipots/"><i style="margin: 10px;" class="fab fa-facebook"></i></a>
                            <a class="text-white" target="_blank" href="https://www.instagram.com/foodoor.in"><i style="margin: 10px;" class="fab fa-instagram"></i></a>
                            <a class="text-white" target="_blank" href="https://www.instagram.com/foodoor.in"><i style="margin: 10px;" class="fa fa-rss"></i></a>

                           </h4>
                        </div>
                    </div>
                </div>
                <!-- bottom footer ends -->
            </div>
        </footer>
@endsection


@section('scripts')


<script type="text/javascript">
    $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        if($('#userLocation').val() != '')
        {
           codeAddress();
        }
    }
});
</script>



@endsection
