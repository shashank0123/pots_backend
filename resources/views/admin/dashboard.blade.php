@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}<small>{{ trans('backpack::base.first_page_you_see') }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection


@section('content')
     <div class="row">



          <div class="col-md-6">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Monthly Sales</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Month</th>
                  <th>Sales (Rs.)</th>
                  <th>Profit Earned (Rs.)</th>
                </tr>
                @foreach($earnings as $index => $earning)
                <tr>
                  <td>{{ getMonth($earning->month) }}</td>
                  <td><span class="label label-warning">Rs. {{ $earning->sales }}</span></td>
                  <td><span class="label label-success">Rs. {{ $earning->profits }}</span></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->


        </div>
          <!-- /.box -->
          </div>




          <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Recent Customers</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Order ID</th>
                  <th>Customer Name</th>
                  <th>Customer Phone</th>
                  <th>Last Order</th>
                </tr>
                @foreach($recentOrders as $index => $order)
                <tr>
                  <td>{{ $order->id }}</td>
                  <td>{{ $order->customer_name }}</td>
                  <td>{{ $order->customer_phone }}</td>
                  <td><span class="label label-primary">{{ $order->booking_date }}</span></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>

        </div>
        <!-- /.col (RIGHT) -->
      </div>

      <div class="row">
        <div class="col-md-6">
           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Earnings By Restaurant</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Restaurant</th>
                  <th>Sales (Rs.)</th>
                  <th>Profit Earned (Rs.)</th>
                </tr>
                @foreach($restaurantEarnings as $index => $earning)
                <tr>
                  <td>{{ getRestaurantName($earning->restaurant_id) }}</td>
                  <td><span class="label label-warning">Rs. {{ $earning->sales }}</span></td>
                  <td><span class="label label-success">Rs. {{ $earning->profits }}</span></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->


        </div>
          <!-- /.box -->
          </div>
      </div>


      <section class="content-header" style="margin-left: -14px;">
      <h1>
       Charts
      </h1>

    </section>
@endsection
