@extends('layouts.app')

@section('title', ' | Pots - Food at your door step')

@section('content')


    <div class="jumbotron restaurant-hero">
        <div class="container">
            <div class="row">
                <div class="col-md-3  image">
                    <figure><img src="{{ isset($restaurant->logo) ? url($restaurant->logo) : 'http://via.placeholder.com/350x250' }}" style="height: 160px;width: 100%;" alt="Profile Image"></figure>
                </div>
                <div class="col-md-5 details">
                     <h3 class="title">{{ $restaurant->name }}</h3>
                     <p class="area">{{ $restaurant->area }}
                     <p><span class="open-indicator badge badge-{{ $restaurant->open_indicator }}">{{ $restaurant->open_status }}</span> {{ $restaurant->cuisines_available }}</p>
                     <div class="d-flex justify-content-between info" style="width: 85%;">
                        <span class="rating"><i class="fas fa-star"></i> 3.5 </span>
                        <span>|</span>
                        <span class="delivery_time"> {{ $restaurant->distance < 3 ? '45 MINS' : '60 MINS' }}  </span>
                        <span>|</span>
                        <span class="for_two"> &#8377; {{ $restaurant->cost_for_two }} FOR TWO</span>
                    </div>
                </div>
                <div class="col-md-4 offer">
                    @if($restaurant->promo_text != '' || $restaurant->promo_text != null)
                            @if(checkRange($restaurant->valid_from, $restaurant->valid_through, date('Y-m-d')))
                              <div class="alert alert-success" role="alert" style="margin-top: 40px;">
                                 <b><i class="fas fa-percent"></i> OFFER</b> <br>
                                 {{ $restaurant->promo_text }}
                              </div>
                             @endif
                           @endif
                </div>
            </div>

        </div>



    </div>
    <div class="container mt-3">
       <a style="margin-bottom: 14px;display: block;" href="/restaurants/explore"><i class="fas fa-arrow-left"></i> All Restaurants</a>
    </div>
    <div class="container restaurant-hero-resp" style="display: none;">

     @include('restaurants.partials._card', ['restaurant' => $restaurant])
     @if($restaurant->promo_text != '' || $restaurant->promo_text != null)
                            @if(checkRange($restaurant->valid_from, $restaurant->valid_through, date('Y-m-d')))
                              <div class="alert alert-success" role="alert" p>
                                 <b><i class="fas fa-percent"></i> OFFER</b> <br>
                                 {{ $restaurant->promo_text }}
                              </div>
                             @endif
                           @endif

    </div>

            <div class="container items-search">
                <div class="d-flex justify-content-between">
                    <form method="GET" class="search-form" style="width: 35%;">
                    <div class="search-input" >
                        <div class="input-group" >
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
                          </div>
                          <input name="query" value="{{request('query')}}" type="text" class="form-control" placeholder="Search menu" aria-label="Search Menu" aria-describedby="basic-addon1">
                        </div>
                    </div>
                  </form>

                    <div class="search-filters">
                      @if(request('veg') == 1)
                         <a href="{{ request()->url() }}"  class="btn btn-outline-success toggler active"  aria-pressed="false" autocomplete="off">
                         <i class="fas fa-check-circle active-icon"></i> <i class="far fa-circle inactive-icon"></i> Veg Only
                        </a>
                      @else
                        <a href="{{ request()->url() }}?veg=1"  class="btn btn-outline-success toggler"  aria-pressed="false" autocomplete="off">
                         <i class="fas fa-check-circle active-icon"></i> <i class="far fa-circle inactive-icon"></i> Veg Only
                        </a>
                      @endif

                      @auth
                      @if($restaurant->isBookmarked)
                        <a href="/bookmarks/{{$restaurant->id}}/remove" class="btn btn-outline-danger toggler active" aria-pressed="false" autocomplete="off">
                         <i class="fas fa-star active-icon"></i> <i class="far fa-star inactive-icon"></i> Bookmark
                        </a>
                      @else
                         <a href="/bookmarks/{{$restaurant->id}}" class="btn btn-outline-danger toggler"  aria-pressed="false" autocomplete="off">
                         <i class="fas fa-star active-icon"></i> <i class="far fa-star inactive-icon"></i> Bookmark
                        </a>
                     @endif
                     @endauth
                    </div>
                </div>
            </div>


            <div class="container my-5 my-res-1">
                <div class="row">
                    <div class="col-md-3 cuisine-filters-col">
                        @include('restaurants.partials._itemFilters')
                    </div>

                    <div class="col-md-6">
                      @if(request()->has('query') && request('query') != '')
                       <div class="cuisine mb-4" id="search-items">
                                <h4 class="name">Search Results</h4>
                                </div>
                      @if(count($searchitems))
                      <div>

                            @foreach($searchitems as $key => $itemSet)
                                <div class="cuisine" id="search-cuisine-{{ $key }}">
                                <h4 class="name">{{ getCuisine($key)->name }}</h4>
                                <span class="count">{{ count($itemSet) }} ITEMS</span>
                                </div>
                                @foreach($itemSet as $item)
                                    @include('restaurants.partials._itemList')
                                @endforeach
                                <br><br>
                            @endforeach
                        </div>
                        @else
                          <p>No results found!</p>
                        @endif

                       @endif
                        @if(count($featuredItems))
                            <div class="cuisine" id="featured">
                                <h4 class="name">Featured Items</h4>
                                <span class="count">{{ count($featuredItems) }} ITEMS</span>
                            </div>
                            <div class="mt-4 pb-5 featured-items row">
                                @foreach($featuredItems as $item)
                                    <div class="col-md-6 px-4 {{ $item->is_veg ? '' : 'item-non-veg' }}">
                                        @include('restaurants.partials._itemCard')
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div>
                            @foreach($allitems as $key => $itemSet)
                                <div class="cuisine" id="cuisine-{{ $key }}">
                                <h4 class="name">{{ getCuisine($key)->name }}</h4>
                                <span class="count">{{ count($itemSet) }} ITEMS</span>
                                </div>
                                @foreach($itemSet as $item)
                                    @include('restaurants.partials._itemList')
                                @endforeach
                                <br><br>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-3 cart-col">
                        <div class="cart">
                        <h4 class="cart-title">Your Cart</h4>
                        <span class="count">@{{ cartItems.length }} ITEMS</span>
                        <div v-if="itemsCount == 0" class="cart-empty-view">
                            <p>There are no items in your cart</p>
                        </div>

                        <div v-else>
                        <div  class="card cart-items my-3">
                            <div class="card-body">
                            <div v-for="item in cartItems" class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                 <img v-if="item.is_veg" class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="veg restaurants">

                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="nonveg restaurants">

                                               @{{ item.name }}</h4>
                                        <p class="price">₹ @{{ item.price * item.quantity }}</p>
                                    </div>

                                    <input type="number" class="number-input" :value="item.quantity" :id="item.id+'-qty'" @change="updateCartItem(item.id, item.attributes.item_id)" >
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <p class="cart-subtotal-title">SUBTOTAL</p>
                                <h3 class="cart-subtotal">₹ @{{ cartSubtotal }}</h3>
                                <p class="cart-subtotal-subtitle">Extra charges may apply</p>
                            </div>
                        </div>
                        @if($restaurant->is_open)
                        <a href="/checkout" :class="cartSubtotal < 99 ? 'disabled' : ''" class="btn btn-success btn-lg checkout-btn">Checkout</a>
                        @else
                        <a href="javascript:;" class="btn btn-success btn-lg checkout-btn disabled">Checkout</a>
                        @endif
                         <p v-if="cartSubtotal < 99" class="cart-subtotal-warning mt-2">Minimum order amount should be ₹99</p>
                        </div>

                    </div>
                </div>
                 </div>

            </div>



            @if($restaurant->is_open)
              <button class="btn btn-success show-cart" data-toggle="modal" data-target="#cartModal" style="display: none;">
                  <span>View Cart</span> <span>₹ @{{ cartSubtotal }}</span>
              </button>
            @else
                <button class="btn btn-danger show-cart disabled" >
                <span>Restaurant Closed</span>
            </button>
            @endif


<div class="modal fade" id="cartModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Your Cart <br> <small class="count text-muted">@{{ cartItems.length }} ITEMS</small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="cart">

                        <div v-if="itemsCount == 0" class="cart-empty-view">
                            <p>There are no items in your cart</p>
                        </div>

                        <div v-else>
                        <div  class="card cart-items my-3">
                            <div class="card-body">
                            <div v-for="item in cartItems" class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                 <img v-if="item.is_veg" class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="veg restaurants">

                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="nonveg restaurants">

                                               @{{ item.name }}</h4>
                                        <p class="price">₹ @{{ item.price * item.quantity }}</p>
                                    </div>

                                    <input type="number" class="number-input" :value="item.quantity" :id="item.id+'-qty-modal'" @change="updateModalCartItem(item.id, item.attributes.item_id)" >
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <p class="cart-subtotal-title">SUBTOTAL</p>
                                <h3 class="cart-subtotal">₹ @{{ cartSubtotal }}</h3>
                                <p class="cart-subtotal-subtitle">Extra charges may apply</p>
                            </div>
                        </div>
                        <a href="/checkout" :class="cartSubtotal < 99 ? 'disabled' : ''" class="btn btn-success btn-lg checkout-btn">Checkout</a>
                         <p v-if="cartSubtotal < 99" class="cart-subtotal-warning mt-2">Minimum order amount should be ₹99</p>
                        </div>

                    </div>
      </div>

    </div>
  </div>
</div>



<button class="btn btn-secondary items-filter" data-toggle="modal" data-target="#filterRestaurantsModal" style="display: none;">
            <i class="fa fa-filter"></i> Filter  Menu
        </button>


<div class="modal fade" id="filterRestaurantsModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filter Restaurants</h5>
        <button type="button" class="btn btn-sm btn-success" data-dismiss="modal" aria-label="Close">
          Apply
        </button>
      </div>
      <div class="modal-body">
        @include('restaurants.partials._itemFilters')
      </div>

    </div>
  </div>
</div>




@endsection

@section('scripts')

    <script>
         var sections = $('.cuisine')
          ,  menu = $('.cuisine-filters')
          , nav = $('.restaurant-hero')
          , nav_height = nav.outerHeight();



        menu.find('.head').on('click', function () {
          var $el = $(this)
            , id = $el.attr('href');

          $('html, body').animate({
            scrollTop: $(id).offset().top - nav_height
          }, 500);

          return false;
        });
    </script>

@endsection