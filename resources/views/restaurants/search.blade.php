@extends('layouts.app')

@section('title', 'Home | Pots - Food at your door step')

@section('content')


    <div class="container my-5">
        <h2 class="section-title">Find your favourite restaurants</h2>
        <form method="GET">
            <div class="input-group mb-3 search-bar">
                <div class="input-group-prepend bg-white">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i> </span>
                  </div>
              <input type="text" style="height: auto !important;" class="form-control" name="query" value="{{ request('query') }}" autocomplete="off" placeholder="Search your favourite restaurant" aria-label="Restaurant's name" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="submit" id="button-addon2">Search</button>
              </div>
            </div>

        </form>



        @if(count($restaurants))
        <div class="infinite-scroll">


                     <h2 class="section-title-small">Results for '{{request('query')}}'</h2>
                    <div class="row">
                @foreach($restaurants as $restaurant)
                   <div class="col-md-3">
                        @include('restaurants.partials._card')
                   </div>
                 @endforeach
                 </div>



           {{ $restaurants->links() }}
        </div>
        @else
            @if(request()->has('query'))
                <h2 class="section-title-small">No Results Found</h2>
            @endif
        @endif
    </div>


@endsection


@section('scripts')

    <script type="text/javascript">
            $('ul.pagination').hide();
            $(function() {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="/img/loading.gif" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {
                        $('ul.pagination').remove();
                    }
                });
            });
    </script>

@endsection