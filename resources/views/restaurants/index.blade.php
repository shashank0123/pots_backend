@extends('layouts.app')

@section('title', 'Home | Pots - Food at your door step')

@section('content')

    <div class="section-search">

        <div class="input-group head-search" >
          <input style="height: auto;" type="text" id="userLocation" value="{{ session('address') }}" class="form-control location-input" placeholder="Enter your delivery location" aria-label="Enter your delivery location" aria-describedby="button-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button" onclick="codeAddress()" id="findRestaurants"><i class="fas fa-search-location"></i></button>
          </div>

        </div>

         <button class="btn btm-outline-secondary locate-me" onclick="getLocation()" type="button"><i class="far fa-compass"></i> Locate Me</button>


    </div>

    <div class="jumbotron restaurant-offers">
        <div class="container">
            <div class="main-carousel"  style="max-height: 280px;overflow: hidden;"  data-flickity='{ "cellAlign": "left", "contain": true, "pageDots": false, "wrapAround": true, "autoPlay": 1500 }'>
                @foreach($offers as $banner)
                <div class="carousel-cell p-2 d-inline-block">
                    <a  href="{{ $banner->url }}">
                    <img style="width: 280px;height: 270px;" src="{{ url($banner->image) }}" alt="banner" />
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container my-5 my-sm-1">


            <div class="row">
                <div class="col-md-3 d-none d-sm-none d-md-block">
                   @include('restaurants.partials._filters')
                </div>
                <div class="col-md-9 mb-5">
                    {{-- <h2 class="section-title">Explore Restaurants</h2> --}}
                     <div class="infinite-scroll">
                            <div class="row">

                                @foreach($restaurants as $restaurant)
                                   <div class="col-md-4">
                                        @include('restaurants.partials._card')
                                   </div>
                                 @endforeach
                          </div>

                        {{ $restaurants->links() }}
                    </div>
                 </div>
            </div>


        </div>


        <button class="btn btn-primary show-filters" data-toggle="modal" data-target="#filterRestaurantsModal" style="display: none;">
            <i class="fa fa-filter"></i> Filters
        </button>


<div class="modal fade" id="filterRestaurantsModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filter Restaurants</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="list-group restaurant-filters">
          <a href="/restaurants/explore" class="list-group-item list-group-item-action {{ is_filter_active('all') }}"><i class="far fa-list-alt mr-2"></i> All Restaurants</a>
          <a href="/restaurants/explore?filter=popular" class="list-group-item list-group-item-action {{ is_filter_active('popular') }}"><i class="fas fa-star mr-2"></i> Most Popular</a>
          <a href="/restaurants/explore?filter=budget" class="list-group-item list-group-item-action {{ is_filter_active('budget') }}"><i class="fas fa-money-bill-wave mr-2"></i> Pocket Friendly</a>
          <a href="/restaurants/explore?filter=veg" class="list-group-item list-group-item-action {{ is_filter_active('veg') }}"><i class="fas fa-feather-alt mr-2"></i> Pure Veg</a>
          <a href="/restaurants/explore?filter=nonveg" class="list-group-item list-group-item-action {{ is_filter_active('nonveg') }}"><i class="fas fa-fish mr-2"></i> Non Veg Special</a>
        </div>
      </div>

    </div>
  </div>
</div>


@endsection


@section('scripts')

    <script type="text/javascript">

            $('ul.pagination').hide();
            $(function() {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="/img/loading.gif" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {
                        $('ul.pagination').remove();
                    }
                });
            });
    </script>

@endsection