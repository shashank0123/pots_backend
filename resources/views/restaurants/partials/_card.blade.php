<a href="/restaurants/{{ str_slug($restaurant->city->name) }}/{{$restaurant->slug}}" class="card restaurant-card mb-3">
  <img class="card-img-top" src="{{ isset($restaurant->logo) ? url($restaurant->logo) : 'http://via.placeholder.com/100x100' }}" style="height: 180px;" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">
      @if($restaurant->is_pure_veg)
      <img src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;
      margin-right: 5px;" alt="restaurant">@endif
      {{ $restaurant->name }}</h5>
    <p class="card-text">{{ $restaurant->cuisines_available }} <br><span class="open-indicator badge badge-{{ $restaurant->open_indicator }}">{{ $restaurant->open_status }}</span> </p>

    <div class="d-flex justify-content-between info">
        <span class="badge badge-success rating"><i class="fas fa-star"></i> 3.5 </span>
        <span>|</span>
        <span class="delivery_time"> {{ $restaurant->distance < 3 ? '45 MINS' : '60 MINS' }} </span>
        <span>|</span>
        <span class="for_two"> &#8377; {{ $restaurant->cost_for_two }} FOR TWO</span>
    </div>



     @if($restaurant->promo_text != '' || $restaurant->promo_text != null)
    @if(checkRange($restaurant->valid_from, $restaurant->valid_through, date('Y-m-d')))
    <hr class="offer-hr">
      <div class="offer">
         {{ $restaurant->promo_text }}
      </div>
     @endif
   @endif
  </div>
</a>