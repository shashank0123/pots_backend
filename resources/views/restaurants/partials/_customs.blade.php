<div class="modal" id="toppings-{{$item->id}}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><b>Customise '{{ $item->name }}'</b> <br> <small class="text-muted">&#8377; {{ $item->special_price }}</small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <customise-item @updated="updateCart" :item="{{ $item }}"></customise-item>
      </div>
    </div>
  </div>
</div>