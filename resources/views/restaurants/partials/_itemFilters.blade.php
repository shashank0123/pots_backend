 <div class="list-group cuisine-filters">
   {{--  @foreach($restaurant->cusineMenu as $cuisine)
        <a href="/restaurants/explore" class="list-group-item list-group-item-action {{ is_filter_active('all') }}">All Restaurants</a>
    @endforeach --}}

     <ul class="nav flex-column flex-nowrap">
                @if(request()->has('query') && request('query') != '')
                    <li class="nav-item"><a class="nav-link head" href="#search-items">Search Results</a></li>
                @endif
                @if(count($featuredItems))
                    <li class="nav-item"><a class="nav-link head" href="#featured"><i class="fa fa-check-circle d-none"></i> Featured Items</a></li>
                @endif
                @foreach($restaurant->cuisineMenu as $cuisine)
                @if($cuisine->parent_id == 0 || $cuisine->parent_id == null)
                     @if($cuisine->subs()->count() == 0)
                        <li class="nav-item"><a class="nav-link" href="#cuisine-{{$cuisine->id}}"><i class="fa fa-check-circle d-none"></i> {{ $cuisine->name }}</a></li>
                     @else
                            <li class="nav-item">
                                <a class="nav-link sub collapsed" href="#submenu-{{$cuisine->id}}" data-toggle="collapse" data-target="#submenu-{{$cuisine->id}}">{{ $cuisine->name }}</a>
                                <div class="collapse" id="submenu-{{$cuisine->id}}" aria-expanded="false">
                                                    <ul class="flex-column nav pl-4">
                                                        @foreach($cuisine->subs as $subCuisine)
                                                            @if($subCuisine->items()->where('restaurant_id', $restaurant->id)->count())
                                                                <li class="nav-item">
                                                                    <a class="nav-link p-1" href="#cuisine-{{$subCuisine->id}}">
                                                                        <i class="fa fa-check-circle d-none"></i> {{ $subCuisine->name }}
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        @endforeach

                                                    </ul>
                                                </div>

                                  </li>
                            @endif
                            @endif
                            @endforeach


            </ul>
</div>