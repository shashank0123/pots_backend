 <div class="list-group restaurant-filters">
  <a href="/restaurants/explore" class="list-group-item list-group-item-action {{ is_filter_active('all') }}"><i class="far fa-list-alt mr-2"></i> All Restaurants</a>
  <a href="/restaurants/explore?filter=popular" class="list-group-item list-group-item-action {{ is_filter_active('popular') }}"><i class="fas fa-star mr-2"></i> Most Popular</a>
  <a href="/restaurants/explore?filter=budget" class="list-group-item list-group-item-action {{ is_filter_active('budget') }}"><i class="fas fa-money-bill-wave mr-2"></i> Pocket Friendly</a>
  <a href="/restaurants/explore?filter=veg" class="list-group-item list-group-item-action {{ is_filter_active('veg') }}"><i class="fas fa-feather-alt mr-2"></i> Pure Veg</a>
  <a href="/restaurants/explore?filter=nonveg" class="list-group-item list-group-item-action {{ is_filter_active('nonveg') }}"><i class="fas fa-fish mr-2"></i> Non Veg Special</a>
</div>


