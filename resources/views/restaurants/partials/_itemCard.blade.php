<div class="card item-card">
  <img class="card-img-top" src="{{ isset($item->photo) ? url($item->photo) : 'http://via.placeholder.com/350x250' }}" style="max-height: 200px;height: 200px;" alt="restaurants item">
  <div class="card-body">
    <h5 class="card-title">@if($item->is_veg)
             <img class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="veg restaurants">
            @else
            <img class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="nonveg restaurants">
            @endif
           {{ substr($item->name, 0, 25) }}</h5>
    <p class="card-text">{{ $item->cuisine->name }}</p>
    <div class="d-flex justify-content-between">
          <span class="price">{!! getPriceView($item) !!}</span>

       @if($restaurant->is_open)
       @if($item->isCustomisable())
            <a href="#toppings-{{$item->id}}"  id="add-cart-{{$item->id}}" data-toggle="modal" class="btn btn-outline-success btn-sm add-item">Add</a>
         @else
         <input type="number" data-key="{{ isInCart($item) ? getCartItem($item)->id : 0 }}" style="display: {{ isInCart($item) ? 'inline-block' : 'none' }}" class="number-input" value="{{ isInCart($item) ? getCartItem($item)->quantity : 0 }}" id="{{ $item->id }}-menu-qty" @change="updateCartItemFromMenu('{{ $item->id }}')" >
        <a href="javascript:;" style="display: {{ !isInCart($item) ? 'inline-block' : 'none' }}" @click.prevent="addToCart({{$item}})" id="add-cart-{{$item->id}}" class="btn btn-outline-success btn-sm add-item">Add</a>
        @endif
        @endif

    </div>

  </div>

</div>


 @if($item->isCustomisable())
  @include('restaurants.partials._customs')
  @endif