@extends('layouts.app')



@section('content')

<div style="height: 80vh;" class="d-flex align-items-center justify-content-center">
    <div class="text-center">
        <img src="/img/notavailable.png" style="width: 150px;height: 100%;display: block;margin: 0 auto;margin-bottom: 15px;" />
        <h3 class="font-weight-bold" style="letter-spacing: 2px;">We aren't here yet!</h3>
        <p class="text-muted"  style="font-size: 19px;letter-spacing: 1px;">We are currently serving in Ranchi only. Please choose a servicable location.</p>
        <a href="/reset" class="btn btn-primary">Go Home</a>
    </div>
</div>

@endsection