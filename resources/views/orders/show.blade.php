@extends('layouts.app')
 
@section('title', 'Order | Pots - Food at your door step')

@section('content')

    <div class="bg-light checkout" style="min-height: 90vh;">
        <div class="container py-5 py-res-0">

        <div class="row">

         <div class="col-md-8">
        <div class="card card-shadow">

            <div class="card-body delivery-details">
               <div>
        <h3 class="font-weight-bold mb-3 float-left order-id"><a href="/my-account/orders"><i class="fas fa-arrow-left"></i></a> Order #{{$order->id}} {!! $order->payment_badge !!}</h3>
        @if($order->status >= 4)
        <a class="btn btn-success float-right download-invoice" target="_blank" href="/orders/{{ $order->id }}/invoice">Download Invoice</a>
        @endif
        </div>
         <div class="clearfix"></div>

                       <div class="clearfix"></div>
                <div class="restaurant-details" style="font-size: 18px;">


                     <div class="d-flex mt-2">
                            <div class="d-flex">
                               <i class="fas fa-map-marker-alt location-marker" style="display: none;"></i>
                               <div>
                                <h5 class="restaurant-name mb-0 mt-1">{{ $order->restaurant_name }}</h5>
                                <p class="restaurant-area text-muted">{{  $order->restaurant_area }}</p>
                               </div>
                            </div>
                        </div>

                </div>
                <div class="customer-details mt-3" style="font-size: 18px;">

                     <div class="mt-4 d-flex" style="position: relative;">
                               <i class="fas fa-map-marker-alt location-marker dashed" style="display: none;"></i>
                               <div>
                    <h5 class="customer-name">{{ $order->customer_name }} | {{ $order->customer_phone }}
                      {{ $order->delivery_alt_no != null && $order->delivery_alt_no != '' ? ', ' . $order->delivery_alt_no : ''  }}</h5>

                      <div class="address-box text-muted">
                                    <h6 class="mt-1">{{ str_limit($order->delivery_location, 70) }}</h6>
                                </div>
                              </div>
                    </div>
                </div>
                <div class="order-status-desc" >
                  <i class="fas fa-{{$order->status_icon}}"></i> {{ $order->status_desc }}
                </div>


            </div>


        </div>
      </div>
      <div class="col-md-4">
         <div class="card cart cart-order  card-shadow">
                        <div class="card-body">



                        <a  href="#cartModal" data-toggle="modal" class="view-cart" style="display: none;">Click to view details</a>

                        <div class="cart-items my-3">
                            <div class="">
                              @foreach($order->items as $item)
                            <div  class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                @if($item->is_veg)
                                                 <img  class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="veg restaurant">
                                                 @else
                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="nonveg restaurant">
                                                @endif
                                               {{ $item->name }}</h4>
                                        <p class="price" style="font-size: 10px;">{{ $item->customs }}</p>
                                    </div>

                                    <p>₹ {{ $item->price * $item->quantity }}</p>
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            @endforeach
                            </div>
                        </div>
                        @if($order->discount > 0)
                        <div class="coupons-button">
                            <button v-else class="btn btn-success"><i class="fas fa-check-circle"></i> Coupon Applied</button>
                        </div>
                        @endif
                         <div class="suggestions">
                            <p class="text-muted">{{ $order->suggestions }}</p>
                        </div>
                        <div class="cart-bill">
                            <table class="table table-borderless">
                                 <tbody>
                                    <tr>
                                      <td>Subtotal</td>
                                      <td class="amount">₹ {{ $order->subtotal }}</td>
                                    </tr>
                                    <tr>
                                      <td>GST</td>
                                      <td class="amount">₹ {{ $order->tax }}</td>
                                    </tr>
                                    <tr class="delivery">
                                      <td>Delivery Charges</td>
                                      <td class="amount">₹ {{ $order->delivery_charges }}</td>
                                    </tr>
                                    @if($order->discount > 0)
                                     <tr class="discount">
                                      <td>Discount</td>
                                      <td class="amount">- ₹ {{ $order->discount }}</td>
                                    </tr>
                                    @endif
                                     <tr class="total">
                                      <td>Total</td>
                                      <td class="amount">₹ {{ $order->total }}</td>
                                    </tr>
                                 </tbody>
                            </table>
                        </div>
                       </div>

                    </div>
       </div>
      </div>
        </div>
    </div>


     <div class="modal fade" id="cartModal" style="z-index: 10000000000;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Purchased Items <br> <small class="count text-muted">{{ count($order->items) }} ITEMS</small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding: 15px !important;padding-right: 22px !important">
        <div class="cart">



                        <div>
                        <div  class="cart-items my-3">
                            <div class="">
                            @foreach($order->items as $item)
                            <div  class="item d-flex justify-content-between align-items-center">

                                    <div style="flex: 1">
                                        <h4 class="name">
                                                @if($item->is_veg)
                                                 <img  class="veg-indicator" src="/img/veg.png" style="width: 15px;height: 15px;margin-top: -4.5px;" alt="veg restaurant">
                                                 @else
                                                <img v-else class="veg-indicator" src="/img/nonveg.png" style="width: 15px;height: 15px;margin-top:-4.5px;" alt="nonveg restaurant">
                                                @endif
                                               {{ $item->name }}</h4>
                                        <p class="price" style="font-size: 10px;">{{ $item->customs }}</p>
                                    </div>

                                    <p>₹ {{ $item->total }}</p>
                                    <!-- <spinner inline controls size="small" :value="item.quantity" @change="(newValue, oldValue, item) => {}"></spinner> -->


                            </div>
                            @endforeach
                            </div>
                        </div>

                        </div>

                    </div>
      </div>

    </div>
  </div>
</div>
@endsection