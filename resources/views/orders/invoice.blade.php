<!DOCTYPE html>
<html>
<head>
    <title>Pots-order-{{$order->id}}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style type="text/css">
        html body {
            font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,Sans-serif;
            background: white;
            width: 100%;
            max-width: 800px;
            min-height: 958px;
            margin: 30px auto 30px auto;
        }

        .logo img {
            width: 155px;
            height: auto;
            float: right;
        }
    </style>
</head>
<body>

     <div>

        <div class="logo">
            <img src="https://foodoor.in/img/pots-logo.png" />
            <!-- <img src="https://foodoor.in/img/wordmark.png" /> -->
        </div>

        <div class="mt-2">

            <h3 style="font-weight: bold;">Order no. : #{{$order->id}}

                @if($order->payment_status)
                <span style="font-size: 14px;color: green;"> PAID</span>
                @else
                <span style="font-size: 14px;color: red;" >PAYMENT PENDING</span>
                @endif
            </h3>
            <p>Thanks for choosing Pots, {{$order->customer_name}}! </p>

            <hr>

            <div style="font-size: 15px;">
                <div >
                    <p>Delivery Details : </p>

                    <p><b>{{ $order->customer_name }}</b> | {{ $order->customer_phone }}{{ $order->delivery_alt_no != null && $order->delivery_alt_no != '' ? ', ' . $order->delivery_alt_no : '' }}<br>
                          {{  $order->delivery_location }}<br><b>Door No.: </b> {{ $order->delivery_door_no }} | {{ $order->delivery_road_name }}
                    <br><b>Landmark : </b> {{ $order->delivery_landmark }}</p>

                </div>

                <div >
                    <p>Ordered From : </p>

                    <p><b>{{ $order->restaurant_name }}</b><br>
                          {{ $order->restaurant_area }}
                          </p>
                </div>

                <div >
                    <p>Customer Suggestions : </p>
                    <p>{{$order->suggestions}}</p>
                </div>
            </div>


            <div class="mt-2">
                <table class="table ">

                   <thead class="thead-dark">
                    <tr>
                        <th>Item Name</th>
                        <th>Quantity</th>
                        <th style="text-align: right;">Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->items as $item)
                                    <tr>
                                        <td style="font-size: 14px;"> {{ $item->name }}
                                                <br> {{$item->customs}}

                                                </td>
                                        <td style="font-size: 14px;">{{ $item->qty }} x {{ $item->price }}</td>
                                        <td style="font-size: 14px;text-align: right;">Rs. {{ $item->total }}</td>
                                    </tr>
                    @endforeach

                                <tr style="font-size: 14px;text-align: right;">
                                                         <td></td>
                                                        <td style="font-size: 14px;">Subtotal</td>

                                                        <td style="font-size: 14px;">Rs. {{$order->subtotal }}</td>
                                                    </tr>
                                                    @if($order->foodoor_cash > 0)
                                                    <tr style="text-align: right;">
                                                        <td style="font-size: 14px;"></td>
                                                        <td style="font-size: 14px;">Pots Cash</td>

                                                        <td style="font-size: 14px;">-Rs. {{ $order->foodoor_cash }}</td>
                                                    </tr>
                                                    @endif
                                                    <tr style="text-align: right;">
                                                        <td style="font-size: 14px;"></td>
                                                        <td style="font-size: 14px;">GST</td>

                                                        <td style="font-size: 14px;">Rs. {{ $order->tax }}</td>
                                                    </tr>
                                                    <tr style="text-align: right;">
                                                     <td></td>
                                                        <td style="font-size: 14px;">Delivery Charges</td>

                                                        <td style="font-size: 14px;">Rs. {{ $order->delivery_charges}}</td>
                                                    </tr>
                                                    @if($order->discount >0 )
                                                     <tr style="text-align: right;">
                                                     <td></td>
                                                        <td style="font-size: 14px;">Discount</td>

                                                        <td style="font-size: 14px;">- Rs. {{ $order->discount}}</td>
                                                    </tr>
                                                    @endif



                                                    <tr style="text-align: right;">
                                                      <td></td>
                                                        <td style="font-size: 14px;" class="text-color"><strong>Total</strong></td>

                                                             <td style="font-size: 14px;" class="text-color"><strong>Rs. {{ $order->total }}</strong></td>

                                                    </tr>
                                </tbody>
                </table>
            </div>
        </div>


     </div>

</body>
</html>