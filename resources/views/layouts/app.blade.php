<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Find the best restaurants near you!Currently we serve multi-cuisine food items with huge offers only in Ranchi.Join the thousands of other restaurants who benefit from having their menus on Pots directory.">
    <meta name="keyword" content="food,pots,food delivery,home delivery,food technology,foodhub,food hub,food and health,ranchi,food in ranchi,food delivery in ranchi, home delivery in ranchi,famous food in ranchi,best food in ranchi,online food in ranchi,street food in ranchi,food mess in ranchi,marwari food in ranchi,food app in ranchi,food bazaar ranchi.">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.partials._meta')

    <title>@yield('title', 'Pots')</title>

    <script>
        window.App = {!! json_encode([
            'user' => Auth::user(),
            'signedIn' => Auth::check()
        ]) !!};
    </script>


    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/flickity.min.css') }}" media="screen">

    <script src="{{ asset('js/flickity.min.js') }}"></script>
    @yield('styles')

    @include('layouts.partials._tracking')

    @yield('tracking')

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127804696-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127804696-1');
</script>




<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Corporation",
  "name": "Ellora Construction",
  "url": "http://elloraconstructions.com",
  "logo": "images/logo.gif",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "",
    "contactType": "sales"
  },
  "sameAs": [
    "",
    "",
    ""
  ]
}
</script>

<!--website schema-->

<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "WebSite",
  "name": "Pots",
  "url": "https://foodoor.in/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://foodoor.in/company/contact{search_term_string}https://foodoor.in/search",
    "query-input": "required name=search_term_string"
  }
}
</script>

<!--breadcrumbs-->

<script type="application/ld+json">
{
  "@context": "http://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [
    { 
      "@type": "ListItem", 
      "position": "1", 
      "item": { 
        "@id": "https://foodoor.in/", 
        "name": "Home" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "2", 
      "item": { 
        "@id": "https://foodoor.in/search", 
        "name": "Search" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "3", 
      "item": { 
        "@id": "https://foodoor.in/checkout", 
        "name": "Cart" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "4", 
      "item": { 
        "@id": "https://foodoor.in/company/about", 
        "name": "About" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "5", 
      "item": { 
        "@id": "https://foodoor.in/company/team", 
        "name": "Team" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "6", 
      "item": { 
        "@id": "https://foodoor.in/company/contact", 
        "name": "Contact" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "7", 
      "item": { 
        "@id": "https://foodoor.in/company/careers", 
        "name": "Career" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "8", 
      "item": { 
        "@id": "https://foodoor.in/company/terms", 
        "name": "Terms" 
      } 
    },
    { 
      "@type": "ListItem", 
      "position": "9", 
      "item": { 
        "@id": "https://foodoor.in/company/privacy-policy", 
        "name": "Privacy Policy" 
      } 
    }
  ]
}
</script>


<!--restaurant-->

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Restaurant",
  "name": "Pots",
  "image": "https://foodoor.in/storage/uploads/banners/ce71cb92614cab3f97dacda6ca85c177.JPG",
  "@id": "",
  "url": "https://foodoor.in/",
  "telephone": "08788191787",
  "priceRange": "100",
  "menu": "https://foodoor.in/restaurants/ranchi/bhukkadd",
  "servesCuisine": "cooking",
  "acceptsReservations": "true",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Hatia Ranchi, Jharkhand",
    "addressLocality": "Hatia Ranchi",
    "postalCode": "",
    "addressCountry": "IN"
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "00:00",
    "closes": "23:59"
  }
}
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KJ877SX');</script>

<!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KJ877SX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

    <div id="app">
        @include('flash::message')
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">


                    @if(request()->is('restaurants*'))
                     <div class="navbar-brand">
                        <div class="d-flex">
                         <a href="/">
                         <img class="logo" src="/img/logo.png" />
                         <img class="logo-responsive" style="display: none;" src="/img/pots-logo.png" />
                         <!-- <img class="logo-responsive" style="display: none;" src="/img/wordmark.png" /> -->
                     </a>
                         <div class="input-group nav-search" >
                          <input style="height: auto;" type="text" id="userLocation" value="{{ session('address') }}" class="form-control location-input" placeholder="Enter your delivery location" aria-label="Enter your delivery location" aria-describedby="button-addon2">
                          <div class="input-group-append">
                            <button class="btn btm-outline-secondary locate-me" onclick="getLocation()" type="button"><i class="far fa-compass"></i> Locate Me</button>
                            <button class="btn btn-primary" type="button" onclick="codeAddress()" id="findRestaurants"><i class="fas fa-search-location"></i></button>
                          </div>

                        </div>
                    </div>
                         </div>
                    @else
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img class="logo-wordmark" src="/img/pots-logo.png" />
                        <!-- <img class="logo-wordmark" src="/img/wordmark.png" /> -->
                    </a>
                    @endif

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse top-nav" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item">
                            <a class="nav-link" href="/search"><i class="fas fa-search"></i> {{ __('Search') }}</a>
                        </li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="modal" href="#loginModal"><i class="fas fa-user-alt"></i> {{ __('Account') }}</a>
                            </li>
                        @else
                            <li class="nav-item user-tab"  style="display: none">
                                <a class="nav-link" href="/my-account"><i class="fas fa-user-alt"></i> {{ __('Account') }}</a>
                            </li>
                            <li class="nav-item dropdown user-dropdown" >
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   Account <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/my-account">Profile</a>
                                    <a class="dropdown-item" href="/my-account/orders">Orders</a>
                                    <a class="dropdown-item" href="/my-account/offers">Offers</a>
                                    <a class="dropdown-item" href="/my-account/bookmarks">Bookmarks</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        @endguest
                        <li class="nav-item">
                                <a class="nav-link" href="/checkout"><i class="fas fa-shopping-bag"></i> {{ __('Cart') }}</a>
                            </li>

                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>


        @include('layouts.partials._loginModal')

    </div>

    <!-- Scripts -->
     <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABIlUStLsr84EGUomykEKJeNPIuWbT854&v=3.exp&sensor=false&libraries=places"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jscroll.js') }}"></script>
    <script type="text/javascript">
        $('#flash-overlay-modal').modal();
        $('div.flash-alert.alert').not('.alert-important').delay(3000).fadeOut(1000);

        var input = $('#userLocation:hidden');
        input.remove();
       console.log(input);
        // initialise location input
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('userLocation'));
            google.maps.event.addListener(places, 'place_changed', function () {
                    console.log(places);
            });
        });

    </script>

    <script src="/js/locationpicker.jquery.js"></script>




    <script src="{{ asset('js/helpers.js') }}"></script>
    @yield('scripts')

</body>
</html>
