<div class="modal" id="restaurantModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="/restaurants/request">
        <div class="modal-header">
          <h5 class="modal-title"><b>Fill up the details and we will get back to you soon</b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
             <div class="form-group">
                              <label>Name <span style="color: red;">*</span></label>
                              <input type="text" name="name"  class="form-control" placeholder="Enter contact name" required="">
                          </div>
                          <div class="form-group">
                              <label>Phone No. <span style="color: red;">*</span></label>
                              <input type="number" step="1" min="0" name="phone" placeholder="Enter contact number"  class="form-control" required="">
                          </div>
                           <div class="form-group">
                              <label>Email Address</label>
                              <input type="email" placeholder="Enter contact email"  name="email"  class="form-control" >
                          </div>
                           <div class="form-group">
                              <label>Address</label>
                              <input type="text"  name="address" placeholder="Where do you reside"  class="form-control" >
                          </div>
                          <div class="form-group">
                              <label>Message</label>
                              <textarea class="form-control" cols="3" name="message" placeholder="Any specific details"></textarea>
                          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Send Details</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal" id="bulkOrderModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="/restaurants/request/bulk">
        <div class="modal-header">
          <h5 class="modal-title"><b>Fill up the details and we will get back to you soon</b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
            <div class="form-group">
                <label>Name <span style="color: red;">*</span></label>
                <input type="text" name="name"  class="form-control" placeholder="Enter contact name" required="">
            </div>
            <div class="form-group">
                <label>Phone No. <span style="color: red;">*</span></label>
                <input type="number" step="1" min="0" name="phone" placeholder="Enter contact number"  class="form-control" required="">
            </div>
             <div class="form-group">
                <label>Event</label>
                <input type="text"  name="event" placeholder="Tell us about your event"  class="form-control" >
            </div>
             <div class="form-group">
                <label>Date <span style="color: red;">*</span></label>
                <input type="date"   name="eventDate"   class="form-control datepicker" required="">
            </div>
            <div class="form-group">
                <label>Message</label>
                <textarea class="form-control" cols="3" name="message" placeholder="Your order purpose/details" ></textarea>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Send Details</button>
        </div>
      </form>
    </div>
  </div>
</div>