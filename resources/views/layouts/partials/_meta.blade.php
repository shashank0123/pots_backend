<meta name="description" content="@yield('meta-desc')">

<meta name="keywords" content="@yield('meta-keywords')"><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

<meta property="og:type" content="website">

<meta property="og:description" content="Order food online from restaurants and get it delivered.
  Serving in Bangalore, Hyderabad, Delhi, Gurgaon, Jaipur, Chandigarh, Ahemdabad, Noida, Mumbai, Pune, Kolkata and Chennai.
  Order Pizzas, Burgers, Biryanis, Desserts or order from Subway, Pizza Hut, Dominos, KFC, McDonalds.">