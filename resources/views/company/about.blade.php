@extends('layouts.app')

@section('title', 'About | Pots - Food at your door step')

@section('content')

    <div class="bg-light company-features-section">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('company._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow company-content-features">
                        <div class="card-body">
                             <h3>About Us</h3>

                             <p>We are team Pots.!!</p>
                             <p> Pots is a platform where user can get delicious food delivered at doorsteps with amazing discounts
                              and cashbacks. Also restaurants will be able to expand their horizons and serving locations. It provides a
                              single window for ordering food from a wide range of restaurants. We have our own exclusive fleet of
                              delivery executives to pick orders from restaurants and deliver it to customers. Our delivery executives
                              are native to the place which ensure faster and reliable delivery.</p>
                              <p>We are the first in Ranchi, which is providing website along with the android mobile app to order food
                              online. Pots is starting its operations from the capital of Jharkhand and very soon will mark its
                              presence across the state.</p>
                              <p>
                              We are inspired by the thought of providing a complete food ordering and delivery solution from the
                              best neighborhood restaurants to the urban foodie. We embrace innovation, it’s at our core.
                              We always put our customer&#39;s need first and always go extra mile for this.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

