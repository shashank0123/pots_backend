@extends('layouts.app')

@section('title', 'Careers | Pots - Food at your door step')

@section('content')

    <div class="bg-light company-features-section">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('company._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow company-content-features">
                        <div class="card-body">
                              <h3>Careers</h3>

                             <p>Pots provide online food delivery solutions to end customers. We have started our operations from the capital of Jharkhand, Ranchi and very soon will mark our presence across the state. We are looking for smart, young and dynamic people who can take every problem as a challenge.</p>

                             <p>Please click the below button to know more about the current openings.</p>

                             <a target="_blank" href="https://goo.gl/forms/Iiyub8ZCnM151lM52" class="btn btn-primary">Be a Part of Pots</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

