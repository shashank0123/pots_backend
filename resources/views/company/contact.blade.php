@extends('layouts.app')

@section('title', 'Contact | Pots - Food at your door step')

@section('content')

<div class="bg-light company-features-section">
    <div class="container py-5 py-res-0">
        <div class="row">
            <div class="col-md-3">
                @include('company._sidebar')
            </div>
            <div class="col-md-9 pb-5">
                <div class="card card-shadow comapny-contact-features">
                    <div class="card-body">
                        <h3>Contact Us</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <p><b>Contact Info</b></p>

                                <p>
                                    <i class="fa fa-phone"></i> 
                                    <a href="tel:9117226666">9117226666,</a> 
                                    <a href="tel:9905812356">9905812356,</a> 
                                </p>

                                <!-- <p>
                                    <i class="fab fa-whatsapp"></i> 
                                    <a href="tel:08788191787">08788191787</a> 
                                </p> -->

                                <p>
                                    <i class="fa fa-envelope"></i> 
                                    <a href="mailto:contact@foodoor.in">contact@foodoor.in</a> 
                                </p>
                                <br>
                                <div class="mapouter">
                                    <div class="gmap_canvas">
                                        <iframe width="400" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=Patna, Bihar&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                    <style>
                                        .mapouter{overflow:hidden;height:327px;width:456px;}
                                        .gmap_canvas {background:none!important;height:327px;width:456px;}
                                    </style>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card contact-form-custom">
                                    <div class="card-body">
                                        <form method="POST" action="/contact">
                                            @csrf
                                            <div class="form-group">
                                                <label>Name<sup class="error">*</sup></label>
                                                <input type="text" name="name" placeholder="Full name" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Email<sup class="error">*</sup></label>
                                                <input type="email" name="email" placeholder="Contact Email" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Phone<sup class="error">*</sup></label>
                                                <input type="text" name="phone" placeholder="Contact number" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Message</label>
                                                <textarea name="message" class="form-control" placeholder="Write something to us.."></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

