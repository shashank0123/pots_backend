@extends('layouts.app')

@section('title', 'Team | Pots - Food at your door step')

@section('content')

    <div class="bg-light company-features-section">
        <div class="container py-5 py-res-0">
            <div class="row">
                <div class="col-md-3">

                            @include('company._sidebar')

                </div>

                <div class="col-md-9 pb-5">
                    <div class="card card-shadow company-team-features">
                        <div class="card-body">
                             <h3>Our Team</h3>

                             <div class="row">
                                <div class="col-md-3">
                                    <img src="http://via.placeholder.com/250x250" class="img-thumbnail" alt="restaurant thumbnail">
                                    <h5><b>Person 1</b></h5>
                                    <p >Role of person</p>
                                </div>

                                <div class="col-md-3">
                                    <img src="http://via.placeholder.com/250x250" class="img-thumbnail" alt="restaurant thumbnail">
                                    <h5><b>Person 2</b></h5>
                                    <p >Role of person</p>
                                </div>

                                <div class="col-md-3">
                                    <img src="http://via.placeholder.com/250x250" class="img-thumbnail" alt="restaurant thumbnail">
                                    <h5><b>Person 3</b></h5>
                                    <p >Role of person</p>
                                </div>

                             </div>

                             <div class="row comapny-team-custom">

                                <div class="col-md-3">
                                    <img src="http://via.placeholder.com/250x250" class="img-thumbnail" alt="restaurant thumbnail">
                                    <h5><b>Person 4</b></h5>
                                    <p >Role of person</p>
                                </div>

                                <div class="col-md-3">
                                    <img src="http://via.placeholder.com/250x250" class="img-thumbnail" alt="restaurant thumbnail">
                                    <h5><b>Person 5</b></h5>
                                    <p >Role of person</p>
                                </div>

                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

