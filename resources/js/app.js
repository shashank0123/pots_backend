
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueNumberInput from '@chenfengyuan/vue-number-input';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


Vue.use(ElementUI);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('customise-item', require('./components/CustomiseItem.vue'));
Vue.component('edit-customs', require('./components/EditCustomise.vue'));
Vue.component('login-register', require('./components/LoginRegister.vue'));
Vue.component('add-address', require('./components/AddAddress.vue'));
Vue.component('edit-address', require('./components/EditAddress.vue'));
Vue.component('update-phone', require('./components/UpdatePhone.vue'));
Vue.component('coupons', require('./components/Coupons.vue'));
Vue.component('spinner', VueNumberInput);

const app = new Vue({
    el: '#app',

    data() {
        return {
            cartItems: [],
            itemsCount: 1,
            cartSubtotal: 0,
            loggedIn: false,
            showFilters: false,
            checkout: {
                address_id: 0,
                suggestions: '',
                payment_mode: 0,
                delivery_alt_no: '',
            },
            couponApplied: false,
            placingOrder: false,
            checkoutError: false,
            checkoutErrorMessage: '',
            bill: {},
        }
    },

    created() {
        this.loggedIn = window.App.signedIn;
        var self = this;
        axios.get('/cart')
            .then(function(response){
                console.log(response.data);
                 self.cartItems = response.data.items;
                 self.cartSubtotal = response.data.subtotal;
                 self.bill = response.data.bill;
            }).catch(function(error){
                console.log(error);
                 $toastr.error('Error loading cart data!');
            });
    },

    methods: {
        toggleVeg()
        {
            $('.item-non-veg').toggle();
        },

        addToCart(item) {
            var btn = event.target;
            $(btn).removeClass('btn-outline-success');
            $(btn).addClass('btn-success');
            $(btn).html('Adding..');
            var self = this;
            axios.post('/cart/add', {qty: 1, item: item.id})
            .then(function(response){
                console.log(response.data.added);
                 self.cartItems = response.data.items;
                 self.cartSubtotal = response.data.subtotal;
                 $(btn).addClass('btn-outline-success');
                 $(btn).removeClass('btn-success');
                 $(btn).html('Add');
                 $('#add-cart-'+item.id).css('display', 'none');
                 $('#' + item.id + '-menu-qty').val(response.data.added.quantity);
                 $('#' + item.id + '-menu-qty').css('display', 'inline-block');
                 $('#' + item.id + '-menu-qty').data('key', response.data.added.id);
                 $toastr.success('Item added to cart!');
            }).catch(function(error){
                console.log(error);
                 $(btn).addClass('btn-outline-success');
                 $(btn).removeClass('btn-success');
                 $(btn).html('Add');
                 $toastr.error(error.response.data.message);
            });

        },

        removeFromCart(id, itemId) {
            var self = this;
            axios.post('/cart/remove/' + id).then(function(response){
                self.cartItems = response.data.items;
                self.cartSubtotal = response.data.subtotal;
                 self.bill = response.data.bill;
                if(self.bill['subTotal'] == 0)
                {
                    axios.post('/cart/clear');
                    if(window.location.pathname == '/checkout'){
                        location.reload();
                    }
                }
                $('#add-cart-'+ itemId).css('display', 'inline-block');
                $('#' +  itemId + '-menu-qty').css('display', 'none');
            })
        },


        updateCartItem(id, itemId)
        {
            var input = $('#' + id + '-qty');

            var qty = input.val();
            if(qty <= 0)
            {
                this.removeFromCart(id, itemId);

            } else {
                 var self = this;
                axios.post('/cart/update/' + id, {qty : qty}).then(function(response){

                    self.cartItems = response.data.items;
                    self.cartSubtotal = response.data.subtotal;
                    self.bill = response.data.bill;
                });
            }
        },

        updateCartItemFromMenu(id)
        {
            var input = $('#' + id + '-menu-qty');
            var key = input.data('key');
            var qty = input.val();
            if(qty <= 0)
            {
                this.removeFromCart(key, id);

            } else {
                 var self = this;
                axios.post('/cart/update/' + key, {qty : qty}).then(function(response){
                    self.cartItems = response.data.items;
                    self.cartSubtotal = response.data.subtotal;
                });
            }
        },

        updateModalCartItem(id, itemId)
        {
            var input = $('#' + id + '-qty-modal');

            var qty = input.val();
            if(qty <= 0)
            {
                this.removeFromCart(id, itemId);

            } else {
                 var self = this;
                axios.post('/cart/update/' + id, {qty : qty}).then(function(response){

                    self.cartItems = response.data.items;
                    self.cartSubtotal = response.data.subtotal;
                    self.bill = response.data.bill;
                });
            }
        },

        changeLoginStatus(status) {
             location.reload();
        },

        placeOrder() {
            var self = this;
            self.placingOrder = true;

            if(self.checkout.address_id == 0)
            {
                self.checkoutError = true;
                self.checkoutErrorMessage = 'Please choose a delivery address';
                self.placingOrder = false;
            } else {
                axios.post('/orders', self.checkout)
                     .then(function(response){
                       // self.placingOrder = false;
                        window.location = response.data.redirect;
                     }).catch(function(error){
                         self.checkoutError = true;
                         self.checkoutErrorMessage = 'There was some problem placing your order please try again!';
                         self.placingOrder = false;
                     });
            }


        },

        updateCart()
        {
             var self = this;
            axios.get('/cart')
                .then(function(response){
                    console.log(response.data);
                     self.cartItems = response.data.items;
                     self.cartSubtotal = response.data.subtotal;
                     self.bill = response.data.bill;
                }).catch(function(error){
                    console.log(error);
                     $toastr.error('Error loading cart data!');
                });
        },

        removeCoupon()
        {
            var self = this;
            axios.get('/coupons/remove').then(function(response){
                self.updateCart();
                $toastr.success('Coupon was removed successfully!');
            });
        }




    }
});
