<?php

use App\Models\Cuisine;
use App\Models\MenuItem;
use App\Models\Restaurant;

function getMonth($index)
{
    $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'
               , 'September', 'October', 'November', 'December'];

    return $months[$index-1];
}

function getRestaurantName($id)
{
    return Restaurant::findOrFail($id)->name;
}

function getCuisine($id)
{
    return Cuisine::findOrFail($id);
}

function getPriceView($item)
{
    if($item->price != $item->special_price)
    {
       return '<s> &#8377;' . $item->price . '</s> &#8377;' . $item->special_price;
    } else {
       return '&#8377;' . $item->price;
    }
}

function isInCart($curitem)
{
    $items = \Cart::getContent();
    foreach ($items as $key => $item) {
        if($item->attributes->item_id == $curitem->id)
        {
          return true;
        }
    }

    return 0;
}

function getCartItem($curitem)
{
    $items = \Cart::getContent();
    foreach ($items as $key => $item) {
        if($item->attributes->item_id == $curitem->id)
        {
          return $item;
        }
    }

    return null;
}

function checkRange($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

function is_filter_active($filter)
{
  if($filter == 'all')
  {
    return !request()->has('filter') ? 'active' : '';
  }
  return request()->has('filter') && request('filter') == $filter ? 'active' : '';
}


function getCustomsString($item)
{
    $msg = '';
    $choices = $item->attributes->choices;
    $toppings = $item->attributes->toppings;
    $menuitem = MenuItem::findOrFail($item->attributes->item_id);

    if($item->attributes->size != null)
    {
        $size = $menuitem->sizes[$item->attributes->size];
        $msg = $msg . 'Size/Category : ' . $size['name'] . ' | ' ;
    }

    if($choices != null && is_array($choices)) {
      foreach ($choices as $key => $value) {
          $choice = $menuitem->additions()->where('slug', $key)->first();
          $options = $choice->options;
          $msg = $msg . ' ' . $choice->name . ' : ' . $options[$value]['name'] . ' | ';
      }
    }

    if($toppings != null && is_array($toppings)) {
      foreach ($toppings as $key => $choosen) {
          $choice = $menuitem->additions()->where('slug', $key)->first();
          $options = $choice->options;
          $selections = [];
          foreach ($choosen as $key => $value) {
            $selections[] = $options[$value]['name'];
          }
          $msg = $msg . ' ' . $choice->name . ' : ' . implode(', ', $selections) . ' | ';
      }
    }

    return $msg;


}


function compare($a, $b)
    {
         if ($a['name'] < $b['name'])
            return -1;
          if ($a['name'] > $b['name'])
            return 1;
          return 0;
    }


function sendSMS($number, $message)
{
  // Account details
  $apiKey = urlencode('RJItAyWpWRI-snoxrIANb5EnQ5Zq7UJ0bDcjk7lOYM');

  // Message details

  $sender = urlencode('FOODOR');
  $message = rawurlencode($message);

  $number = '91' . $number;

  // Prepare data for POST request
  $data = array('apikey' => $apiKey, 'numbers' => $number, "sender" => $sender, "message" => $message);

  // Send the POST request with cURL
$ch = curl_init('https://api.textlocal.in/send/');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($ch);
  // Check the return value of curl_exec(), too
    /*if ($response === false) {
        throw new Exception(curl_error($ch), curl_errno($ch));
    }*/
  curl_close($ch);

  // Process your response here
  return $response;
}


function split_name($name) {
    $name = trim($name);
    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
    return array($first_name, $last_name);
}

