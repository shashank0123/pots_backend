<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use CrudTrait;

        const DISTANCE_UNIT_KILOMETERS = 111.045;
const DISTANCE_UNIT_MILES      = 69.0;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'restaurants';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [ 'name', 'slug', 'area',  'location', 'logo', 'contact_name', 'contact_phone', 'contact_email', 'username', 'password', 'website', 'city_id', 'timings', 'bank_name', 'bank_ifsc', 'bank_acc_no', 'bank_acc_name', 'bank_acc_type',  'promo_text', 'discount_type', 'discount', 'valid_from', 'valid_through', 'min_order', 'latitude', 'longitude', 'cost_for_two', 'is_pure_veg',   'commission', 'days_open', 'gst_applicable', 'delivery_time', 'is_featured', 'days'];
    // protected $hidden = [];
    // protected $dates = [];

    protected $casts = [
        'timings' => 'json',
        'days_open' => 'array'
    ];

   public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getCityNameAttribute()
    {
        return $this->city->name;
    }

    public function getIsOpenAttribute()
    {

        if($this->all_day)
        {
            return true;
        }



        $isOpen = false;
        $now = date("h:i A");
        foreach($this->timings as $timing)
        {

            if(array_key_exists('open_time', $timing) && array_key_exists('close_time', $timing))
            {
                $stime = $timing['open_time'];
                $etime = $timing['close_time'];
                $isOpen = strtotime($now) < strtotime($etime) && strtotime($now) > strtotime($stime);
            }




            if($isOpen)
            {
                break;
            }
        }

        return $isOpen;
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getOrdersServedAttribute()
    {
        return $this->orders()->count();
    }
    public function items()
    {
        return $this->hasMany(MenuItem::class);
    }

    public function getIsBookmarkedAttribute()
    {
        return auth()->user()->bookmarks->contains($this);
    }

    public function featuredItems()
    {
        return $this->hasMany(MenuItem::class)->where('is_featured', 1);
    }

    public function getCuisineMenuAttribute()
    {
        $cuisineIds = array_keys($this->items->groupBy('cuisine_id')->toArray());
        $cuisines = [];

        foreach ($cuisineIds as $key => $cuisine) {
            $cuisineModel = Cuisine::findOrFail($cuisine);

             $cuisines[] = $cuisineModel;
        }

        $cuisineMenu = [];

         foreach ($cuisines as $key => $cuisine) {
            if($cuisine->parent_id == 0 || $cuisine->parent_id == null)
            {
              $cuisineMenu[] = $cuisine;
            } else {
              $cuisineMenu[] = $cuisine->parent;
            }
        }


         $cuisineMenu = array_unique($cuisineMenu);

         $cuisines = array_unique($cuisines);

         usort($cuisines, array($this, "cmp"));

         usort($cuisineMenu, array($this, "cmp"));

         return $cuisineMenu;
    }

    public function cmp($a, $b)
    {
        return strcmp($a->name, $b->name);
    }

    public function cuisines()
    {
        return $this->belongsToMany(Cuisine::class);
    }

    public function getOpenIndicatorAttribute()
    {
        return $this->is_open ? 'success' : 'danger';
    }
    public function getOpenStatusAttribute()
    {
        return $this->is_open ? 'Open Now' : 'Closed';
    }



    public function getCuisinesAvailableAttribute()
    {
        return implode(', ', $this->cuisines()->limit(3)->pluck('name')->toArray());
    }

    public function manageItems($crud = false)
    {

        return '<a class="btn btn-xs btn-success" href="/admin/menu-items?restaurant=' . $this->id . '" data-toggle="tooltip" title="Manage Menu"><i class="fa fa-list-ul"></i> Menu Items</a>';

    }

    public function ordersHistory()
    {
        return '<a class="btn btn-xs btn-primary" href="/admin/orders?restaurant=' . $this->id . '" data-toggle="tooltip" title="Manage Orders"><i class="fa fa-file-text"></i> Orders History</a>';
    }

    /* public function items()
    {
        return $this->hasMany(Item::class);
    } */

     public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $disk = "public";
        $destination_path = "uploads/restaurants";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }


     /*
    |--------------------------------------------------------------------------
    | Methods for storing uploaded files (used in CRUD).
    |--------------------------------------------------------------------------
    */

    /**
     * Handle file upload and DB storage for a file:
     * - on CREATE
     *     - stores the file at the destination path
     *     - generates a name
     *     - stores the full path in the DB;
     * - on UPDATE
     *     - if the value is null, deletes the file and sets null in the DB
     *     - if the value is different, stores the different file and updates DB value.
     *
     * @param  [type] $value            Value for that column sent from the input.
     * @param  [type] $attribute_name   Model attribute name (and column in the db).
     * @param  [type] $disk             Filesystem disk used to store files.
     * @param  [type] $destination_path Path in disk where to store the files.
     */
    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path)
    {
        $request = \Request::instance();

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($attribute_name) &&
            $this->{$attribute_name} &&
            $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

            // 2. Move the new file to the correct path
            $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = 'storage/' . $file_path;
        }
    }



/**
 * @param $query
 * @param $lat
 * @param $lng
 * @param $radius numeric
 * @param $units string|['K', 'M']
 */
public function scopeNearLatLng($query, $lat, $lng, $radius = 2, $units = 'M')
{
    $distanceUnit = $this->distanceUnit($units);

    if (!(is_numeric($lat) && $lat >= -90 && $lat <= 90)) {
        throw new Exception("Latitude must be between -90 and 90 degrees.");
    }

    if (!(is_numeric($lng) && $lng >= -180 && $lng <= 180)) {
        throw new Exception("Longitude must be between -180 and 180 degrees.");
    }

    $haversine = sprintf('*, (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(latitude)) * COS(RADIANS(%f - longitude)) + SIN(RADIANS(%f)) * SIN(RADIANS(latitude))))) AS distance',
        $distanceUnit,
        $lat,
        $lng,
        $lat
    );

    $subselect = clone $query;
    $subselect
        ->selectRaw(DB::raw($haversine));

    // Optimize the query, see details here:
    // http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

    $latDistance      = $radius / $distanceUnit;
    $latNorthBoundary = $lat - $latDistance;
    $latSouthBoundary = $lat + $latDistance;
    $subselect->whereRaw(sprintf("latitude BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

    $lngDistance     = $radius / ($distanceUnit * cos(deg2rad($lat)));
    $lngEastBoundary = $lng - $lngDistance;
    $lngWestBoundary = $lng + $lngDistance;
    $subselect->whereRaw(sprintf("longitude BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

    $query
        ->from(DB::raw('(' . $subselect->toSql() . ') as d'))
        ->where('distance', '<=', $radius);
}

/**
 * @param $units
 */
private function distanceUnit($units = 'K')
{
    if ($units == 'K') {
        return static::DISTANCE_UNIT_KILOMETERS;
    } elseif ($units == 'M') {
        return static::DISTANCE_UNIT_MILES;
    } else {
        throw new Exception("Unknown distance unit measure '$units'.");
    }
}

}
