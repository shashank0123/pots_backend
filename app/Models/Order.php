<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $guarded = [];
    // protected $hidden = [];
    // protected $dates = [];

   public function restaurant()
   {
        return $this->belongsTo(Restaurant::class);
   }

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getBookingDateAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getStatusTextAttribute()
    {
        $statuses = ['Order Placed', 'Confirmed by Restaurant', 'Order Ready' , 'Order Picked', 'Order Delivered'];
        return $statuses[$this->status];
    }

    public function getCancelledByNameAttribute()
    {
        return $this->cancelled_by == 0 ? 'Customer' : 'Restaurant';
    }

    public function getItemsCountAttribute()
    {
        return count($this->items);
    }

    public function getPaymentBadgeAttribute()
    {
        if($this->payment_mode == 0)
        {
            if($this->status < 4) {
                $status = 'Payment Pending';
                $color = 'danger';
            } else {
                $status = 'Payment Received';
                $color = 'success';
            }
        } else {
            if($this->status > -1) {
               $status = 'Payment Received';
                $color = 'success';
            }
        }

        return '<small><span class="badge badge-' . $color . '">' . $status . '</span></small>';
    }

    public function getStatusBadgeAttribute()
    {
        $statuses = ['Awaiting Confirmation', 'Confirmed', 'Ready', 'Picked', 'Delivered'];
        $colors = ['secondary', 'primary', 'info', 'warning', 'success'];
        return '<small><span class="badge badge-' . $colors[$this->status] . '">' . $statuses[$this->status] . '</span></small>';
    }
    public function getAmountEarnedAttribute()
    {
        return $this->subtotal - $this->commission_earned;
    }

    public function confirmOrder($crud = false)
    {
        if($this->status < 1)
        {
           return '<a class="btn btn-xs btn-success" href="/orders/'. $this->id . '/confirm" data-toggle="tooltip" title="Confirm Order"><i class="fa fa-check"></i> Confirm Order</a>';
        } elseif($this->status == 1) {

                 return '<a class="btn btn-xs btn-info" href="/orders/'. $this->id . '/ready" data-toggle="tooltip" title="Order Ready" ><i class="fa fa-check"></i> Order Ready</a>';
        } elseif($this->status == 2) {

                 return '<a class="btn btn-xs btn-info" href="/orders/'. $this->id . '/picked" data-toggle="tooltip" title="Order Picked" ><i class="fa fa-check"></i> Order Picked</a>';
        } elseif($this->status == 3) {

                 return '<a class="btn btn-xs btn-success" href="/orders/'. $this->id . '/delivered" data-toggle="tooltip" title="Order Delivered" ><i class="fa fa-check"></i> Order Delivered</a>';
        } else {
             return '<a class="btn btn-xs btn-success" href="#" data-toggle="tooltip" title="Order Delivered" disabled><i class="fa fa-check"></i> Order Closed</a>';
        }

    }

     public function cancelOrder($crud = false)
    {
           return
           '<a class="btn btn-xs btn-danger" href="/orders/'. $this->id . '/cancel" data-toggle="tooltip"  title="Cancel Order"><i class="fa fa-file-text"></i> Cancel</a>';
    }


    public function invoice($crud = false)
    {
           return
           '<a class="btn btn-xs btn-primary" href="/orders/'. $this->id . '/invoice" data-toggle="tooltip" target="_blank" title="Download Invoice"><i class="fa fa-file-text"></i> Invoice</a>';
    }




    public function addCustomerDetails($addressId)
    {
        $address = Address::findOrFail($addressId);
        $this->user_id = auth()->id();
        $this->customer_name = auth()->user()->name;
        $this->customer_phone = auth()->user()->phone;
        $this->customer_email = auth()->user()->email;
        $this->delivery_location = $address->location;
        $this->delivery_latitude = $address->latitude;
        $this->delivery_longitude = $address->longitude;
        $this->delivery_door_no = $address->door_no;
        $this->delivery_road_name = $address->road_name;
        $this->delivery_landmark = $address->landmark;
    }

    public function addRestaurantDetails($restaurant)
    {

        $this->restaurant_id = $restaurant->id;
        $this->restaurant_name = $restaurant->name;
        $this->restaurant_location = $restaurant->location;
        $this->restaurant_latitude = $restaurant->latitude;
        $this->restaurant_longitude = $restaurant->longitude;
        $this->restaurant_area = $restaurant->area;
    }

    public function addBillingDetails($bill)
    {
        $this->subtotal = $bill['subTotal'];
        $this->tax = $bill['gst'];
        $this->delivery_charges = $bill['deliveryCharges'];
        $this->total = ceil(round($bill['total'], 2));
        $this->discount = $bill['discount'] != null ? $bill['discount']  : 0;
        $this->commission_earned = $this->subtotal * ($this->restaurant->commission/100);
    }

    public function storeItems($items)
    {
        foreach ($items as $key => $item) {
            $this->items()->create(['menu_item_id' => $item->attributes->item_id, 'name' => $item->name,
                                'price' => $item->price, 'qty' => $item->quantity, 'total' => $item->price * $item->quantity,
                                'customs' => getCustomsString($item)]);
        }
    }


    public function getStatusIconAttribute()
    {
         $statuses = ['Awaiting Confirmation', 'Confirmed', 'Ready', 'Picked', 'Delivered'];
         $icons = ['clock', 'check-circle', 'shopping-bag', 'motorcycle', 'check'];
         return $icons[$this->status];
    }

    public function getStatusDescAttribute()
    {
        $statuses = ['Awaiting Confirmation', 'Confirmed', 'Ready', 'Picked', 'Delivered'];
        $colors = ['secondary', 'primary', 'info', 'warning', 'success'];
        return $statuses[$this->status];
    }
}
