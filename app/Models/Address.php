<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['user_id', 'location', 'latitude', 'longitude', 'door_no', 'road_name', 'landmark', 'type'];

    public function getTypeStringAttribute()
    {
        $types = ['Home', 'Office', 'Other'];

        return $types[$this->type];
    }
}
