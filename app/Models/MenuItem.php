<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;


class MenuItem extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'menu_items';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name', 'slug', 'description', 'cuisine_id', 'restaurant_id', 'price', 'discount_price', 'photo', 'is_veg', 'is_available', 'sizes', 'is_featured'];
    // protected $hidden = [];
    // protected $dates = [];

     protected $casts = [
        'sizes' => 'json',
    ];

    protected $appends = ['special_price'];


    protected $with = ['cuisine', 'additions'];

    public function additions()
    {
        return $this->hasMany(Topping::class, 'item_id');
    }

    public function cuisine()
    {
        return $this->belongsTo(Cuisine::class);
    }

    public function getCuisineNameAttribute()
    {
        return $this->cuisine->name;
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function getSpecialPriceAttribute()
    {

          if(isset($this->discount_price))
          {
            $price = $this->price - $this->discount_price;
          } else {
            $price = $this->price;
          }



     if($this->restaurant->promo_text != '' || $this->restaurant->promo_text != null)
     {
        if(checkRange($this->restaurant->valid_from, $this->restaurant->valid_through, date('Y-m-d')))
        {
           $discount = $this->restaurant->discount_type == 0 ? $this->restaurant->discount :  $price * ($this->restaurant->discount / 100);
           $discount = $discount < $price ? $discount : $price;

           $price = $price - $discount;
        }
     }
        return $price;
    }

    public function isCustomisable()
    {
      return count($this->additions) || ($this->sizes != null && count($this->sizes));
    }

    public function manageToppings($crud = false)
    {

            return '<a class="btn btn-xs btn-success" href="/admin/toppings?item=' . $this->id . '" data-toggle="tooltip" title="View Options/Toppings"><i class="fa fa-list-ul"></i> Additions/Options</a>';

    }


    public function getRestaurantNameAttribute()
    {
        return $this->restaurant->name;
    }

    public function getitemFullNameAttribute()
    {
        return $this->name . ' - ' . $this->restaurant->name;
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($this->attributes['name']);
    }

     public function edit()
    {
        return '<a class="btn btn-xs btn-default"   href="/admin/menu-items/' . $this->id . '/edit?restaurant=' . $this->restaurant->id . '" data-toggle="tooltip" title="Edit Item"><i class="fa fa-edit"></i> Edit</a>';
    }

     public function add()
    {
        return '<a class="btn btn-md btn-primary"   href="/admin/menu-items/create?restaurant=' . request('restaurant') . '" data-toggle="tooltip" title="Add Item"><i class="fa fa-plus"></i> Add New Item</a>';
    }

    public function delete()
    {


        return '<a href="javascript:void(0)"  class="btn btn-xs btn-default" onclick="deleteEntry(this)" data-route="/admin/menu-items/' . $this->id  .'"class="btn btn-xs btn-default" data-button-type="delete"><i class="fa fa-trash"></i> Delete</a><script>
    if (typeof deleteEntry != \'function\') {
      $("[data-button-type=delete]").unbind(\'click\');

      function deleteEntry(button) {
          // ask for confirmation before deleting an item
          // e.preventDefault();
          var button = $(button);
          var route = button.attr(\'data-route\');
          var row = $("#crudTable a[data-route=\'"+route+"\']").parentsUntil(\'tr\').parent();

          if (confirm("Are you sure you want to delete this item?") == true) {
              $.ajax({
                  url: route,
                  type: \'DELETE\',
                  success: function(result) {
                      // Show an alert with the result
                      new PNotify({
                          title: "Item Deleted",
                          text: "The item has been deleted successfully.",
                          type: "success"
                      });

                      // Hide the modal, if any
                      $(\'.modal\').modal(\'hide\');

                      // Remove the row from the datatable
                      row.remove();
                  },
                  error: function(result) {
                      // Show an alert with the result
                      new PNotify({
                          title: "NOT deleted",
                          text: "There&#039;s been an error. Your item might not have been deleted.",
                          type: "warning"
                      });
                  }
              });
          } else {
              // Show an alert telling the user we don\'t know what went wrong
              new PNotify({
                  title: "Not deleted",
                  text: "Nothing happened. Your item is safe.",
                  type: "info"
              });
          }
      }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue(\'deleteEntry\');
</script>';
    }
}
