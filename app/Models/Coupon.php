<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Coupon extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'coupons';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
     protected $fillable = [ 'code', 'promo_text', 'discount_type', 'discount', 'restaurant_id', 'valid_from', 'valid_through', 'min_order',  'applied_to_all'];
    // protected $hidden = [];
    // protected $dates = [];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

     public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class);
    }

    public function getRestaurantNameAttribute()
    {
        return $this->applied_to_all ?  'All Restaurants' : count($this->restaurants) . ' Restaurants';
    }


    public function getDiscountTypeTextAttribute()
    {
        switch ($this->discount_type) {
            case 0:
                return 'Flat';
                break;
            case 1:
                return 'Percentage';
                break;
            case 2:
                return 'Free Delivery';
                break;
            default:
                return 'Coupon';
                break;
        }

    }
}
