<?php

namespace App;

use App\Models\Order;
use App\Models\Address;
use App\Models\Restaurant;
use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use CrudTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('created_at', 'DESC');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class)->orderBy('type', 'ASC');
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Restaurant::class, 'bookmarks');
    }

    public function getOrdersCountAttribute()
    {
        return $this->orders()->count();
    }

    public function getRegisteredOnAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }
}
