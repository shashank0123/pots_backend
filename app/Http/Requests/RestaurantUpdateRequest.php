<?php

namespace App\Http\Requests;

use Faker\Provider\latitude;
use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class RestaurantUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name' => 'required|min:3|max:255',
             'area' => 'required|min:3|max:255',
             'location' => 'required',
             'location' => 'required',
             'latitude' => 'required',
             'longitude' => 'required',
             'contact_name' => 'required',
             'contact_phone' => 'required',
             'contact_email' => 'required',
             'latitude' => 'required',
             'city_id'  => 'required',
//           'delivery_time' => 'required',
             'is_pure_veg' => 'required',
             'cost_for_two' => 'required',
             'gst_applicable' => 'required',
             '24_hrs' => 'required',
             'bank_name' => 'required',
             'bank_ifsc' => 'required',
             'bank_acc_no' => 'required',
             'bank_acc_name' => 'required',
             'bank_acc_type' => 'required',
             'cuisines' => 'required',

        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
