<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class OtpController extends Controller
{
    public function send(Request $request)
    {
        $phone = $request->get('phone');

        if(User::where('phone', $phone)->exists())
        {
            return response(['message' => 'Phone number is already registered!'], 500);
        }


        $otp = mt_rand(10000, 99999);


        $message = 'Your Pots verification OTP is ' . $otp;

        $response = sendSMS($phone, $message);


        return response(['message' => 'OTP sent successfully!', 'otp' => $otp, 'phone' => $phone, 'api' => $response], 200);
    }
}
