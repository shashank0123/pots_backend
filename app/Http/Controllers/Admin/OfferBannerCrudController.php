<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OfferBannerRequest as StoreRequest;
use App\Http\Requests\OfferBannerRequest as UpdateRequest;

/**
 * Class OfferBannerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OfferBannerCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\OfferBanner');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/offer-banners');
        $this->crud->setEntityNameStrings('Offer Banner', 'Offer Banners');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

         $this->crud->addColumns([
                [
                   'name' => 'image', // The db column name
                   'label' => "Banner", // Table column heading
                   'type' => 'image',
                    // 'prefix' => 'folder/subfolder/',
                    // optional width/height if 25px is not ok with you
                    'height' => '90px',
                    'width' => '90px',
                ],

                [
                    'name' => 'url', 'label' => 'Banner Link'
                ]
        ]);

        $this->crud->addFields([
                [   // Upload
                    'name' => 'image',
                    'label' => 'Banner Image  <span style="color: red;">*</span>',
                    'type' => 'upload',
                    'upload' => true,
                    'driver' => 'uploads' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                ],

                [
                  'name' => 'url', 'label' => 'Banner Link', 'type' => 'url', 'value' => 'http://www.foodoor.in/'
                ]

            ]);

        // add asterisk for fields that are required in OfferBannerRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
