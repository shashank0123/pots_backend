<?php

namespace App\Http\Controllers\Admin;

use App\Models\Restaurant;
use App\Http\Requests\OrderRequest as StoreRequest;
use App\Http\Requests\OrderRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EarningCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/earnings');
        $this->crud->setEntityNameStrings('Earning', 'Earnings');


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

         $this->crud->addColumns([
            ['name' => 'id', 'label' => 'Order ID'],
            ['name' => 'customer_name', 'label' => 'Customer Name'],
           // ['name' => 'customer_phone', 'label' => 'Customer Phone'],
            ['name' => 'restaurant_name', 'label' => 'Restaurant'],
            ['name' => 'booking_date', 'label' => 'Date'],
            ['name' => 'subtotal', 'label' => 'Subtotal (Rs.)'],
           // ['name' => 'amount_earned', 'label' => 'Amount Paid (Rs.)'],
            ['name' => 'commission_earned', 'label' => 'Pots Commission (Rs.)'],

        ]);

       // $this->crud->addClause('whereDate', 'created_at', '=', date('Y-m-d'));

        $this->crud->addClause('where', 'status', '=', 4);

        $this->crud->addClause('where', 'is_offline', '=', 0);

        $this->crud->orderBy('created_at', 'DESC');

        $this->crud->denyAccess(['create', 'reorder', 'delete', 'update']);



      // $this->crud->addButtonFromModelFunction('line', 'confirm', 'confirmOrder', 'end');


        $this->crud->addButtonFromModelFunction('line', 'invoice', 'invoice', 'end');

        // add asterisk for fields that are required in OrderRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();
    }




    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
