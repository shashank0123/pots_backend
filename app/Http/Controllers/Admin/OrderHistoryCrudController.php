<?php

namespace App\Http\Controllers\Admin;

use App\Models\Restaurant;
use App\Http\Requests\OrderRequest as StoreRequest;
use App\Http\Requests\OrderRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderHistoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order-history');
        $this->crud->setEntityNameStrings('Completed Order', 'Completed Orders');


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

         $this->crud->addColumns([
            ['name' => 'id', 'label' => 'Order ID'],
            ['name' => 'customer_name', 'label' => 'Customer Name'],
            ['name' => 'customer_phone', 'label' => 'Customer Phone'],
            ['name' => 'restaurant_name', 'label' => 'Restaurant'],
            ['name' => 'created_at', 'label' => 'Date & Time'],
            ['name' => 'items_count', 'label' => 'Items Count'],
            ['name' => 'total', 'label' => 'Bill Amount (Rs.)'],
            ['name' => 'status_text', 'label' => 'Status'],
        ]);

       // $this->crud->addClause('whereDate', 'created_at', '=', date('Y-m-d'));

        $this->crud->addClause('where', 'status', '=', 4);

        $this->crud->addClause('where', 'is_offline', '=', 0);

        $this->crud->orderBy('created_at', 'DESC');

        $this->crud->denyAccess(['create', 'reorder', 'delete', 'update']);



      // $this->crud->addButtonFromModelFunction('line', 'confirm', 'confirmOrder', 'end');


        $this->crud->addButtonFromModelFunction('line', 'invoice', 'invoice', 'end');

        // add asterisk for fields that are required in OrderRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();

        $this->crud->addFilter([ // simple filter
          'type' => 'text',
          'name' => 'restaurant_name',
          'label'=> 'Restaurant'
        ],
        false,
        function($value) { // if the filter is active
             $this->crud->addClause('where', 'restaurant_name', 'LIKE', "%$value%");
        } );

         $this->crud->addFilter([ // date filter
          'type' => 'date',
          'name' => 'date',
          'label'=> 'Date'
        ],
        false,
        function($value) { // if the filter is active, apply these constraints
           $this->crud->addClause('whereDate', 'created_at', $value);
        });

         $this->crud->addFilter([ // daterange filter
           'type' => 'date_range',
           'name' => 'from_to',
           'label'=> 'Date range'
         ],
         false,
         function($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('whereDate', 'created_at', '>=', $dates->from);
            $this->crud->addClause('whereDate', 'created_at', '<=', $dates->to);
         });
    }




    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
