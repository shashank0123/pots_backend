<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DeliveryBoyRequest as StoreRequest;
use App\Http\Requests\DeliveryBoyUpdateRequest as UpdateRequest;

/**
 * Class DeliveryBoyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DeliveryBoyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\DeliveryBoy');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/delivery-boys');
        $this->crud->setEntityNameStrings('Delivery Boy', 'Delivery Boys');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            ['name' => 'name', 'label' => 'Full Name'],
            ['name' => 'ordersCount', 'label' => 'Orders Served'],
        ]);

        $this->crud->addFields([

            ['name' => 'name', 'label' => 'Full Name'],

            ['name' => 'email', 'label' => 'Email Address', 'type' => 'email'],

            ['name' => 'phone', 'label' => 'Phone No.'],

            ['name' => 'username', 'label' => 'Username'],

            ['name' => 'password', 'label' => 'Password'],


        ]);


        // add asterisk for fields that are required in DeliveryBoyRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
