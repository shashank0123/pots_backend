<?php

namespace App\Http\Controllers\Admin;

use App\Models\Restaurant;
use App\Http\Requests\MenuItemRequest as StoreRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\MenuItemRequest as UpdateRequest;

/**
 * Class MenuItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MenuItemCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MenuItem');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/menu-items');
        $this->crud->setEntityNameStrings('Menu Item', 'Menu Items');

        $this->crud->backlink = config('backpack.base.route_prefix') . '/menu-items?restaurant=' . request('restaurant');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'price', 'label' => 'Price'],
        ]);

        $this->crud->addColumn(['name' => 'cuisineName', 'label' => 'Cuisine']);

        if(request()->has('restaurant'))
        {
            $this->crud->addField([  // Select2
               'label' => "Restaurant",
               'type' => 'hidden',
               'name' => 'restaurant_id',
               'value' => request('restaurant')

            ]);

            $this->restaurant = Restaurant::findOrFail(request('restaurant'));

            $this->crud->addClause('where', 'restaurant_id', '=', $this->restaurant->id);

             $this->crud->setEntityNameStrings($this->restaurant->name . ' | Menu Item', $this->restaurant->name . ' | Menu Items');

             $this->crud->headlink = config('backpack.base.route_prefix') . '/restaurants';
             $this->crud->headname = $this->restaurant->name;



            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions?problem=' . $this->problem->id);

        } else {
            $this->crud->addField( [  // Select2
               'label' => "Restaurant",
               'type' => 'select2',
               'name' => 'restaurant_id', // the db column for the foreign key
               'entity' => 'restaurant', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model' => "App\Models\Restaurant", // foreign key model
               'allows_null' => false
            ]
            );
             $this->crud->setEntityNameStrings('Menu Item', 'Menu Items');

            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions');
        }


        $this->crud->addFields([
              [   // Upload
                'name' => 'photo',
                'label' => 'Item Logo',
                'type' => 'upload',
                'upload' => true,
                'driver' => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;,
                'tab' => 'General'
            ],

            ['name' => 'name', 'label' => 'Name', 'tab' => 'General'],

            ['name' => 'description', 'label' => 'Decription/Ingredients', 'tab' => 'General'],

            [  // Select2
               'label' => "Cuisine",
               'type' => 'select2',
               'name' => 'cuisine_id', // the db column for the foreign key
               'entity' => 'cuisine', // the method that defines the relationship in your Model
               'attribute' => 'nameTree', // foreign key attribute that is shown to user
               'model' => "App\Models\Cuisine", // foreign key model
               'tab' => 'General'
            ],

            ['name' => 'price', 'label' => 'Price', 'tab' => 'General', 'type' => 'number', 'attributes' => ["min" => 1]],

             ['name' => 'discount_price', 'label' => 'Discount on Item', 'tab' => 'General', 'type' => 'number', 'attributes' => ["min" => 1]],

              [ // select_from_array
                'name' => 'is_available',
                'label' => 'Is Available',
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false,
                'default' => 1,
                'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],

            [ // select_from_array
                'name' => 'is_veg',
                'label' => 'Veg or Non Veg',
                'type' => 'select2_from_array',
                'options' => [0 => 'Non Veg', 1 => 'Veg'],
                'allows_null' => false,
                'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],

            [ // select_from_array
                'name' => 'is_featured',
                'label' => 'Is Featured',
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false,
                'default' => 0,
                'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],

            [ // Table
                'name' => 'sizes',
                'label' => 'Available Sizes',
                'type' => 'table',
                'entity_singular' => 'option', // used on the "Add X" button
                'columns' => [
                    'name' => 'Name (Ex. Half/13 Inch etc.)',
                    'price' => 'Price (Rs.)'
                ],
                'tab' => 'Available Sizes'
            ],


        ]);

        $this->crud->removeAllButtons();
        $this->crud->addButtonFromModelFunction('line', 'edit', 'edit', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'delete', 'delete', 'end');
        $this->crud->addButtonFromModelFunction('top', 'add', 'add', 'beginning');


        $this->crud->addButtonFromModelFunction('line', 'additions', 'manageToppings', 'end');


        $this->crud->orderBy('name');

        // add asterisk for fields that are required in MenuItemRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();

        $this->crud->addFilter([ // dropdown filter
          'name' => 'is_veg',
          'type' => 'dropdown',
          'label'=> 'Food Type'
        ], [

          0 => 'Non Veg',
          1 => 'Pure Veg',
        ], function($value) { // if the filter is active
            if($value != 3)
            {
             $this->crud->addClause('where', 'is_veg', $value);
            }
        });
    }

      /**
     * Redirect to the correct URL, depending on which save action has been selected.
     * @param  [type] $itemId [description]
     * @return [type]         [description]
     */
    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = 'admin/menu-items/create?restaurant=' . $this->crud->entry->restaurant->id;
                break;
            case 'save_and_edit':
                $redirectUrl = 'admin/menu-items'.'/'.$itemId.'/edit?restaurant='. $this->crud->entry->restaurant->id;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
                break;
            case 'save_and_back':
            default:
                $redirectUrl = 'admin/menu-items?restaurant=' . $this->crud->entry->restaurant->id;
                break;
        }

        return \Redirect::to($redirectUrl);
    }

 /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst(($this->restaurant ? $this->restaurant->name . ' --> ' : '') . $this->crud->entity_name_plural);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }


    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
