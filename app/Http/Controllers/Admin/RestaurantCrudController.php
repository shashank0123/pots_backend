<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RestaurantRequest as StoreRequest;
use App\Http\Requests\RestaurantUpdateRequest as UpdateRequest;

/**
 * Class RestaurantCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RestaurantCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Restaurant');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/restaurants');
        $this->crud->setEntityNameStrings('Restaurant', 'Restaurants');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'area', 'label' => 'Area'],
            ['name' => 'cityName', 'label' => 'City'],
            ['name' => 'ordersServed', 'label' => 'Orders Served']
        ]);


         $this->crud->addField(['name' => 'name', 'label' => 'Restaurant Name',  'tab' => 'General']);


        $this->crud->addFields([

               [   // Upload
                'name' => 'logo',
                'label' => 'Restaurant Photo',
                'type' => 'upload',
                'upload' => true,
                'driver' => 'uploads' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            ],

             [
                'label' => 'Cuisines',
                'type' => 'select2_multiple',
                'name' => 'cuisines',
                'entity' => 'cuisines',
                'attribute' => 'name',
                'model' => "App\Models\Cuisine",
                'pivot' => true,
                'tab' => 'General'
             ],

             ['name' => 'location', 'label' => 'Google Map Location', 'type' => 'location', 'tab' => 'Location'],

             ['name' => 'latitude',  'label' => 'Latitude:',  'type' => 'number',  'tab' => 'Location', 'attributes' => ['step' => 0.00000000000000001]],

             ['name' => 'longitude', 'label' => 'Longitude:', 'type' => 'number', 'tab' => 'Location', 'attributes' => ['step' => 0.000000000000000001]],

             ['name' => 'area', 'label' => 'Locality', 'tab' => 'Location'],

             [  // Select2
               'label' => 'City',
               'type' => 'select2',
               'name' => 'city_id', // the db column for the foreign key
               'entity' => 'city', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model' => "App\Models\City" // foreign key model
               , 'tab' => 'Location'
             ],


             [ // select_from_array
                'name' => 'is_pure_veg',
                'label' => "Pure Veg Restaurant?",
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false
                , 'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
             ],

               [ // select_from_array
                'name' => 'gst_applicable',
                'label' => "GST Applicable?",
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false
                , 'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
             ],

              [ // select_from_array
                'name' => 'is_featured',
                'label' => "Featured Restaurant?",
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false
                , 'tab' => 'General'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
             ],


             ['name' => 'cost_for_two', 'label' => 'Cost For Two', 'type' => 'number' , 'tab' => 'General'],

             ['name' => 'contact_name', 'label' => 'Contact Person', 'tab' => 'Account'],

             ['name' => 'contact_email', 'label' => 'Contact Email', 'type' => 'email', 'tab' => 'Account'],

             ['name' => 'contact_phone', 'label' => 'Contact Phone', 'type' => 'number', 'attributes' => ["step" => 1, "maxlength"=>10, "min" => 1], 'tab' => 'Account'],

             ['name' => 'username', 'label' => 'Login Username', 'type' => 'text', 'tab' => 'Account'],

             ['name' => 'password', 'label' => 'Login Password', 'type' => 'text', 'tab' => 'Account'],

             ['name' => 'website', 'label' => 'Website Link' , 'tab' => 'General'],

             ['name' => 'commission', 'label' => 'Restaurant Commission', 'type' => 'number' , 'tab' => 'Banking'],

             ['name' => 'bank_name', 'label' => 'Bank Name' , 'tab' => 'Banking'],

             ['name' => 'bank_ifsc', 'label' => 'Bank IFSC Code', 'tab' => 'Banking'],

            ['name' => 'bank_acc_no', 'label' => 'Bank Account No.', 'type' => 'number', 'attributes' => ["step" => 1, "min" => 1], 'tab' => 'Banking'],

            ['name' => 'bank_acc_name', 'label' => 'Bank Account Name', 'tab' => 'Banking'],

            [ // select_from_array
                'name' => 'bank_acc_type',
                'label' => "Savings or Current?",
                'type' => 'select2_from_array',
                'options' => [0 => 'Savings Account', 1 => 'Current Account'],
                'allows_null' => false,
                'default' => 1
                , 'tab' => 'Banking'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],

            ['name' => 'promo_text', 'label' => 'Promo Text (optional)', 'type' => 'textarea', 'tab' => 'Offer/Discount'],

            [ // select_from_array
                'name' => 'discount_type',
                'label' => "Flat or Percentage?",
                'type' => 'select2_from_array',
                'options' => [0 => 'Flat Discount', 1 => 'Percentage Base'],
                'allows_null' => false,
                'default' => 0,
                'tab' => 'Offer/Discount'
            ],

            ['name' => 'discount', 'label' => 'Discount ', 'type' => 'number', 'tab' => 'Offer/Discount', 'attributes' => ['min' => 1]],

            ['name' => 'min_order', 'label' => 'Minimum Order Amount', 'type' => 'number', 'tab' => 'Offer/Discount', 'attributes' => ['min' => 1]],

            ['name' => 'valid_from', 'label' => 'Valid From ', 'type' => 'date_picker', 'tab' => 'Offer/Discount'],

            ['name' => 'valid_through', 'label' => 'Valid Uptil ', 'type' => 'date_picker', 'tab' => 'Offer/Discount'],

            [ // select_from_array
                'name' => '24_hrs',
                'label' => "Open 24 Hrs",
                'type' => 'select2_from_array',
                'options' => [0 => 'No', 1 => 'Yes'],
                'allows_null' => false
                , 'tab' => 'Timings'
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
             ],

             ['name' => 'days_open', 'label' => 'Days ', 'tab' => 'Timings',  'type' => 'select2_from_array',
                'options' => ['mon' => 'Monday', 'tue' => 'Tuesday', 'wed' => 'Wednesday', 'thu' => 'Thursday', 'fri' => 'Friday', 'sat' => 'Saturday', 'sun' => 'Sunday'],
                'allows_null' => false,
                'default' => 'mon',
                'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],

             ['name' => 'timings', 'label' => 'Timings ',  'tab' => 'Timings',  'type' => 'timings', 'entity_singular' => 'time slot', // used on the "Add X" button
                'columns' => [
                    'open_time' => 'Open time',
                    'close_time' => 'Close Time',
                ],
                'max' => 5, // maximum rows allowed in the table
                'min' => 1, // minimum rows allowed in the table],
            ]
                    ]);

       $this->crud->addButtonFromModelFunction('line', 'items', 'manageItems', 'end');

        $this->crud->addButtonFromModelFunction('line', 'orders', 'ordersHistory', 'end');

        $this->crud->orderBy('name');


        // add asterisk for fields that are required in RestaurantRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $this->crud->entry->slug = str_slug($this->crud->entry->name);
        $this->crud->entry->save();
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $this->crud->entry->slug = str_slug($this->crud->entry->name);
        $this->crud->entry->save();
        return $redirect_location;
    }
}
