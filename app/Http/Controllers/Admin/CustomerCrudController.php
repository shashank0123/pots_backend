<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CustomerCrudRequest as StoreRequest;
use App\Http\Requests\CustomerCrudRequest as UpdateRequest;

class CustomerCrudController extends CrudController {

  public function setup()
  {
      $this->crud->setModel("App\User");
      $this->crud->setRoute("admin/customers");
      $this->crud->setEntityNameStrings('Customer', 'Customers');


      $this->crud->addColumns([
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'email', 'label' => 'E-mail Address'],
            ['name' => 'phone', 'label' => 'Phone Number'],
            ['name' => 'orders_count', 'label' => 'Orders Placed'],
            ['name' => 'registered_on', 'label' => 'Registered On'],
        ]);

      $this->crud->denyAccess(['create', 'update', 'reorder', 'delete']);

      $this->crud->enableExportButtons();
  }

  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }

  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}