<?php

namespace App\Http\Controllers\Admin;

use App\Models\MenuItem;
use App\Http\Requests\ToppingRequest as StoreRequest;
use App\Http\Requests\ToppingRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class ToppingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ToppingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Topping');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/toppings');
        $this->crud->setEntityNameStrings('Topping', 'Toppings');

        $this->crud->backlink = config('backpack.base.route_prefix') . '/toppings?item=' . request('item');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'itemName', 'label' => 'Menu Item'],
        ]);


        if(request()->has('item'))
        {

            $this->crud->removeColumn( ['name' => 'itemName', 'label' => 'Menu Item']);
            $this->crud->addField([  // Select2
               'label' => "Item",
               'type' => 'hidden',
               'name' => 'item_id',
               'value' => request('item')

            ]);

            $this->item = MenuItem::findOrFail(request('item'));

            $this->crud->addClause('where', 'item_id', '=', $this->item->id);

             $this->crud->setEntityNameStrings($this->item->name . ' | Topping', $this->item->name . ' | Toppings');

             $this->crud->headlink = config('backpack.base.route_prefix') . '/menu-items?restaurant=' . $this->item->restaurant->id;
             $this->crud->headname = $this->item->name;



            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions?problem=' . $this->problem->id);

        } else {
            $this->crud->addField( [  // Select2
               'label' => "Item",
               'type' => 'select2',
               'name' => 'item_id', // the db column for the foreign key
               'entity' => 'item', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model' => "App\Models\MenuItem", // foreign key model
               'allows_null' => false
            ]
            );
             $this->crud->setEntityNameStrings('Topping', 'Toppings');

            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions');
        }

        $this->crud->addFields([
            ['name' => 'name', 'label' => 'Name' ],

            [ // select_from_array
                'name' => 'select_type',
                'label' => 'Selection Type',
                'type' => 'select2_from_array',
                'options' => [0 => 'Choose One', 1 => 'Choose Multiple'],
                'allows_null' => false
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
             ],


            [ // Table
                'name' => 'options',
                'label' => 'Choices',
                'type' => 'table',
                'entity_singular' => 'option', // used on the "Add X" button
                'columns' => [
                    'name' => 'Name',
                    'desc' => 'Description (optional)',
                    'price' => 'Price (Rs.)'
                ]
            ],
        ]);

        $this->crud->removeAllButtons();
        $this->crud->addButtonFromModelFunction('line', 'edit', 'edit', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'delete', 'delete', 'end');
        $this->crud->addButtonFromModelFunction('top', 'add', 'add', 'beginning');




        // add asterisk for fields that are required in ToppingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();
    }

      /**
     * Redirect to the correct URL, depending on which save action has been selected.
     * @param  [type] $itemId [description]
     * @return [type]         [description]
     */
    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = 'admin/toppings/create?item=' . $this->crud->entry->item->id;
                break;
            case 'save_and_edit':
                $redirectUrl = 'admin/toppings'.'/'.$itemId.'/edit?item='. $this->crud->entry->item->id;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
                break;
            case 'save_and_back':
            default:
                $redirectUrl = 'admin/toppings?item=' . $this->crud->entry->item->id;
                break;
        }

        return \Redirect::to($redirectUrl);
    }

 /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst(($this->item ? $this->item->name . ' --> ' : '') . $this->crud->entity_name_plural);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $this->crud->entry->slug = str_slug($this->crud->entry->name);
        $this->crud->entry->save();
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $this->crud->entry->slug = str_slug($this->crud->entry->name);
        $this->crud->entry->save();
        return $redirect_location;
    }
}
