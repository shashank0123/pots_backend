<?php

namespace App\Http\Controllers\Admin;

use App\Models\Restaurant;
use App\Http\Requests\OrderRequest as StoreRequest;
use App\Http\Requests\OrderRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/orders');
        $this->crud->setEntityNameStrings('Order', 'Orders');

        $this->crud->backlink = config('backpack.base.route_prefix') . '/orders?restaurant=' . request('restaurant');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

         $this->crud->addColumns([
            ['name' => 'id', 'label' => 'Order ID'],
            ['name' => 'customer_name', 'label' => 'Customer Name'],
            ['name' => 'customer_phone', 'label' => 'Customer Phone'],
           // ['name' => 'restaurant_name', 'label' => 'Restaurant'],
            ['name' => 'created_at', 'label' => 'Date & Time'],
            ['name' => 'items_count', 'label' => 'Items Count'],
            ['name' => 'subtotal', 'label' => 'Subtotal (Rs.)'],
           ['name' => 'commission_earned', 'label' => 'Pots Commission (Rs.)'],
            ['name' => 'amount_earned', 'label' => 'Amount Earned (Rs.)'],
        ]);

        $this->crud->addClause('where', 'status', '=', 4);

        $this->crud->orderBy('created_at', 'DESC');

        $this->crud->denyAccess(['create', 'reorder', 'delete', 'update']);

         if(request()->has('restaurant'))
        {
            $this->crud->addField([  // Select2
               'label' => "Restaurant",
               'type' => 'hidden',
               'name' => 'restaurant_id',
               'value' => request('restaurant')

            ]);

            $this->restaurant = Restaurant::findOrFail(request('restaurant'));

            $this->crud->addClause('where', 'restaurant_id', '=', $this->restaurant->id);

             $this->crud->setEntityNameStrings($this->restaurant->name . ' | Order', $this->restaurant->name . ' | Orders');

             $this->crud->headlink = config('backpack.base.route_prefix') . '/restaurants';
             $this->crud->headname = $this->restaurant->name;



            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions?problem=' . $this->problem->id);

        } else {
            $this->crud->addField( [  // Select2
               'label' => "Restaurant",
               'type' => 'select2',
               'name' => 'restaurant_id', // the db column for the foreign key
               'entity' => 'restaurant', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model' => "App\Models\Restaurant", // foreign key model
               'allows_null' => false
            ]
            );
             $this->crud->setEntityNameStrings('Order', 'Orders');

            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions');
        }

      //  $this->crud->addButtonFromModelFunction('line', 'confirm', 'confirmOrder', 'end');


        $this->crud->addButtonFromModelFunction('line', 'invoice', 'invoice', 'end');

        // add asterisk for fields that are required in OrderRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableExportButtons();



         $this->crud->addFilter([ // date filter
          'type' => 'date',
          'name' => 'date',
          'label'=> 'Date'
        ],
        false,
        function($value) { // if the filter is active, apply these constraints
           $this->crud->addClause('whereDate', 'created_at', $value);
        });

         $this->crud->addFilter([ // daterange filter
           'type' => 'date_range',
           'name' => 'from_to',
           'label'=> 'Date range'
         ],
         false,
         function($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('whereDate', 'created_at', '>=', $dates->from);
            $this->crud->addClause('whereDate', 'created_at', '<=', $dates->to);
         });
    }


    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst(($this->restaurant ? $this->restaurant->name . ' --> ' : '') . $this->crud->entity_name_plural);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
