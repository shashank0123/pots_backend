<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $recentOrders = Order::where('status', 4)->latest()->limit(5)->with('customer')->get();
       $earnings = DB::table('orders')
                ->select(DB::raw('month(created_at) as month'), DB::raw('SUM(commission_earned) as profits'), DB::raw('SUM(total) as sales'))
                ->groupBy('month')
                ->get();

        $restaurantEarnings = DB::table('orders')
                ->select('restaurant_id', DB::raw('SUM(commission_earned) as profits'), DB::raw('SUM(total) as sales'))
                ->groupBy('restaurant_id')
                ->get();


        return view('admin.dashboard', compact('recentOrders', 'earnings', 'restaurantEarnings'));
    }
}
