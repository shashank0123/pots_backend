<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\Restaurant;
use Illuminate\Http\Request;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('valid_through', 'DESC')->get();

        return response(['coupons' => $coupons], 200);
    }

    public function apply()
    {
        $restaurant = Restaurant::findOrFail(request('restaurant'));
        $code = request('code');

        if(Coupon::where('code', $code)->exists())
        {
            $coupon = Coupon::where('code', $code)->first();

            if(checkRange($coupon->valid_from, $coupon->valid_through, date('Y-m-d')))
            {

                if($coupon->applied_to_all || $coupon->restaurants->contains($restaurant))
                {

                    if(\Cart::getSubTotal() > $coupon->min_order)
                    {
                        session(['coupon_applied' => true]);
                        $discount = $coupon->discount_type == 0 ? $coupon->discount : \Cart::getSubTotal() * ($coupon->discount / 100);
                        $discount = $discount > 150 ? 150 : $discount;

                        $discountCoupon = new \Darryldecode\Cart\CartCondition(array(
                            'name' => 'discount',
                            'type' => 'discount',
                            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
                            'value' => '-' . $discount ,
                            'order' => 1
                        ));
                        \Cart::condition($discountCoupon);

                       // flash('Coupon was applied successfully')->success();

                         return response(['message' => 'Coupon was applied successfully'], 200);
                    } else {
                        // show error ..
                        return response(['message' => 'Minimum order amount for this coupon should be ' . $coupon->min_order], 500);
                      // flash()->error();
                    }

                } else {
                   // flash('')->error();
                     return response(['message' => 'Given coupon is not applicable to this restaurant'], 500);
                }

            } else {
                // flash('')->error();
                 return response(['message' => 'Given coupon is expired'], 500);
            }

        } else {
           // flash('')->error();
            return response(['message' => 'Given coupon code is invalid'], 500);
        }


        return response(['success'], 200);
    }

    public function applyFoodoor()
    {
        if(auth()->user()->foodoor_cash < 0) {
             return response(['message' => 'You don\'t have enough pots cash'], 500);

        } else {

            session(['coupon_applied' => true]);
            $discount = auth()->user()->foodoor_cash * (10/100) > 100 ? 100 : auth()->user()->foodoor_cash * (10/100);

             $discountCoupon = new \Darryldecode\Cart\CartCondition(array(
                'name' => 'discount',
                'type' => 'discount',
                'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
                'value' => '-' . $discount ,
                'order' => 1
            ));
            \Cart::condition($discountCoupon);

            return response(['message' => 'Pots cash was applied successfully'], 200);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        \Cart::clearCartConditions();
        return response(['message' => 'Coupon was removed!'], 200);
    }
}
