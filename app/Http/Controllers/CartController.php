<?php

namespace App\Http\Controllers;

use App\Models\MenuItem;
use App\Models\Restaurant;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $items =  \Cart::getContent()->toArray();

        $bill['subTotal'] = round(\Cart::getSubTotal(), 2);
        $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
        $bill['deliveryCharges'] = 30;
        $condition = \Cart::getCondition('discount');
        if(isset($condition))
        {
          $bill['discount'] = round($condition->getCalculatedValue(\Cart::getTotal()), 2);
        } else {
             $bill['discount'] = null;
        }
        $bill['total'] = round(\Cart::getTotal(), 2) + $bill['gst'] + $bill['deliveryCharges'];
        usort($items, "compare");
        return response(['items' => $items, 'subtotal' => \Cart::getSubTotal(), 'bill' => $bill] , 200);
    }
    public function store(Restaurant $restaurant)
    {
        $items = \Cart::getContent();
        $menuItem = MenuItem::findOrFail(request('item'));
        if(count($items))
        {
            $firstItem = MenuItem::findOrFail($items->first()->attributes->item_id);
            $restaurant = $firstItem->restaurant;
            if($menuItem->restaurant->id != $restaurant->id)
            {
                return response(['message' => 'You cannot add items from multiple restaurants!'], 500);
            }
        }
        $item = array(
            'id' => uniqid(5),
            'name' => $menuItem->name,
            'price' => floatval($menuItem->special_price),
            'quantity' => request('qty'),
            'attributes' => array('item_id' => $menuItem->id)
        );

        \Cart::add($item);
         $bill['subTotal'] = round(\Cart::getSubTotal(), 2);
        $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
        $bill['deliveryCharges'] = 30;
        $condition = \Cart::getCondition('discount');
        if(isset($condition))
        {
          $bill['discount'] = round($condition->getCalculatedValue(\Cart::getTotal()), 2);
        } else {
             $bill['discount'] = null;
        }
        $bill['total'] = round(\Cart::getTotal(), 2) + $bill['gst'] + $bill['deliveryCharges'];
        $items =  \Cart::getContent()->toArray();
        usort($items, "compare");
        return response(['items' => $items, 'subtotal' => \Cart::getSubTotal(), 'added' =>  $item, 'bill' => $bill], 200);
    }

     public function storeCustom()
    {
        $menuItem = MenuItem::findOrFail(request('item'));
        $items = \Cart::getContent();
        if(count($items))
        {
            $firstItem = MenuItem::findOrFail($items->first()->attributes->item_id);
            $restaurant = $firstItem->restaurant;
            if($menuItem->restaurant->id != $restaurant->id)
            {
                return response(['message' => 'You cannot add items from multiple restaurants!'], 500);
            }
        }
        $item = array(
            'id' => uniqid(5),
            'name' => $menuItem->name,
            'price' => request('price'),
            'quantity' => 1,
            'attributes' => array('item_id' => $menuItem->id, 'size' => request('size'),
                        'choices' => request('choices'), 'toppings' => request('toppings'),
                        'extraCharges' => request('extraPrice'))
        );

        \Cart::add($item);
         $bill['subTotal'] = round(\Cart::getSubTotal(), 2);
        $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
        $bill['deliveryCharges'] = 30;
        $condition = \Cart::getCondition('discount');
        if(isset($condition))
        {
          $bill['discount'] = round($condition->getCalculatedValue(\Cart::getTotal()), 2);
        } else {
             $bill['discount'] = null;
        }
        $bill['total'] = round(\Cart::getTotal(), 2) + $bill['gst'] + $bill['deliveryCharges'];
        $items =  \Cart::getContent()->toArray();
        usort($items, "compare");
        return response(['items' => $items, 'subtotal' => \Cart::getSubTotal(), 'added' =>  $item, 'bill' => $bill], 200);
    }

    public function update($itemId)
    {
        \Cart::update($itemId, array(
          'quantity' => array(
              'relative' => false,
              'value' => request('qty')
          ),
        ));

        $items =  \Cart::getContent()->toArray();
        $ $bill['subTotal'] = round(\Cart::getSubTotal(), 2);
        $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
        $bill['deliveryCharges'] = 30;
        $condition = \Cart::getCondition('discount');
        if(isset($condition))
        {
          $bill['discount'] = round($condition->getCalculatedValue(\Cart::getTotal()), 2);
        } else {
             $bill['discount'] = null;
        }
        $bill['total'] = round(\Cart::getTotal(), 2) + $bill['gst'] + $bill['deliveryCharges'];
        usort($items, "compare");
         return response(['items' => $items , 'subtotal' => \Cart::getSubTotal(), 'bill' => $bill] , 200);
    }

    public function destroy($itemId)
    {
        \Cart::remove($itemId);

        $items =  \Cart::getContent()->toArray();
        $bill['subTotal'] = round(\Cart::getSubTotal(), 2);
        $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
        $bill['deliveryCharges'] = 30;
        $condition = \Cart::getCondition('discount');
        if(isset($condition))
        {
          $bill['discount'] = round($condition->getCalculatedValue(\Cart::getTotal()), 2);
        } else {
             $bill['discount'] = null;
        }
        $bill['total'] = round(\Cart::getTotal(), 2) + $bill['gst'] + $bill['deliveryCharges'];
          usort($items, "compare");
         return response(['items' => $items , 'subtotal' => \Cart::getSubTotal(), 'bill' => $bill] , 200);
    }


    public function clear()
    {
        \Cart::clearCartConditions();
    }

}
