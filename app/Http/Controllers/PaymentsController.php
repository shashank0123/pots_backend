<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Appnings\Payment\Facades\Payment;

class PaymentsController extends Controller
{
    public function pay()
    {
        $order = Order::findOrFail(request('order'));
        $amount = $order->total;

        $parameters = [

            'tid' => '1001' . $order->id,

            'order_id' => $order->id,

            'amount' => $amount,
            'billing_name' => \Auth::user()->name,
            'billing_ email' => \Auth::user()->email,
            'billing_ tel' => \Auth::user()->phone,
            'billing_ country' => 'India',

          ];


          $purchaseOrder = Payment::prepare($parameters);

          return Payment::process($purchaseOrder);
    }

    public function response(Request $request)
    {
        $response = Payment::response($request);

        $orderId = $response['order_id'];
        $order = Order::findOrFail($orderId);

        if($response['order_status'] != 'Success')
        {
            $order->delete();
            return redirect('/checkout');
        } else {
            $order->payment_status = 1;
            $order->status = 0;
            $order->save();

             \Cart::clear();
             \Cart::clearCartConditions();
             $message = 'Thanks for ordering with Pots. Your order no: '. $order->id . ' and bill amount : Rs. '. $order->total .'/- . We are waiting for restaurant confirmation and will update you soon.';
             $response = sendSMS($order->customer_phone, $message);
             $messageToRest = 'You have received a new order of Rs. '. $order->total .'/-. Order Invoice : https://foodoor.in/orders/'. $order->id  .'/invoice';
             sendSMS($order->restaurant->contact_phone, $messageToRest);

            return redirect('/thankyou?order=' . $order->id);
        }

    }

    public function cancel(Request $request)
    {
        $response = Payment::response($request);

        $orderId = $response['order_id'];
        $order = Order::findOrFail($orderId);
        $order->delete();

        flash('You cancelled the payment!')->warning();

        return redirect('/checkout');
    }
}
