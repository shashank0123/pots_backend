<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\PasswordBroker;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function profile()
    {
        return view('account.profile');
    }

    public function addresses()
    {
        return view('account.addresses');
    }

    public function orders()
    {
        return view('account.orders');
    }

    public function offers()
    {
        $coupons = Coupon::orderBy('valid_through', 'DESC')->get();
        return view('account.offers', compact('coupons'));
    }

    public function bookmarks()
    {

        return view('account.bookmarks');
    }

    public function payments()
    {
        return view('account.payments');
    }

     public function updateProfile(Request $request)
    {
        $this->validate($request, ['first_name' => 'required', 'last_name' => 'required']);

        $data = $request->all();
        $data['name'] = $request->get('first_name') . ' ' . $request->get('last_name');


        auth()->user()->update($data);

       flash('Your Profile was updated successfully')->success();

        return back();

    }

     public function updatePhone(Request $request)
    {
        $phone = $request->get('phone');

        auth()->user()->phone = $phone;

        auth()->user()->save();

        return response(['status' => 'success', 'message' => 'Phone updated successfully!'], 200);
    }

    public function updatePassword(Request $request)
    {
         $this->validate($request, [
              'old_password' => 'required',
              'password' => 'required|string|min:6|confirmed'
         ]);

          $old_password = $request->get('old_password');
          $password = $request->get('password');

          if(!\Hash::check($old_password, auth()->user()->password))
          {
            flash('Please enter your current password correct!')->success();

              return back();
          }

          auth()->user()->password = bcrypt($password);
          auth()->user()->save();

           flash('Your password was successfully updated!')->success();

          return back();
    }
}
