<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store()
    {
        \Mail::to('pots.ranchi@gmail.com')->send(new \App\Mail\ContactUs(request('name'), request('phone'), request('email'), request('message')));

        flash('We have received your details. We will get back to you soon!')->success();

         return back();
    }

}
