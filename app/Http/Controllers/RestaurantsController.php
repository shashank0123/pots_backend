<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\OfferBanner;
use Illuminate\Http\Request;

class RestaurantsController extends Controller
{
    public function index()
    {
        if(!session()->has('lat') || !session()->has('lng') || !session()->has('address'))
        {
            return redirect('/');
        }

        $restaurants = Restaurant::latest();

        $restaurants = $restaurants->NearLatLng(session('lat'), session('lng'), config('settings.restaurants_radius', 2))
            ->orderBy('distance', 'ASC');

        if(request()->has('filter'))
        {
            if(request('filter') == 'veg')
            {
                $restaurants->where('is_pure_veg', 1);
            } else if(request('filter') == 'nonveg')
            {
                $restaurants->where('is_pure_veg', 0);
            } else if(request('filter') == 'popular')
            {
                $restaurants->where('is_featured', 1);
            }  else if(request('filter') == 'budget')
            {
                $restaurants->where('cost_for_two', '<', 500);
            }
        }



        $restaurants = $restaurants->paginate(9);

        $offers = OfferBanner::all();

        if(!count($restaurants))
        {
            return view('restaurants.none');
        }

        return view('restaurants.index', compact('restaurants', 'offers'));
    }

    public function show($city, $restaurantSlug)
    {

       // dd(\Cart::getContent());
        $restaurant = Restaurant::where('slug', $restaurantSlug)->first();




        $allitems = $restaurant->items()->where('is_featured', 0);
        $featuredItems = $restaurant->featuredItems();

        if(request()->has('veg') && request('veg') == 1 )
        {
             $allitems = $allitems->where('is_veg', 1);
             $featuredItems = $featuredItems->where('is_veg', 1);
        }
        $searchitems = [];
        if(request()->has('query') && request()->has('query') != '')
        {
                $searchitems = $restaurant->items()->where('name', 'like', '%' . request('query') . '%')->get()->groupBy('cuisine_id');
        }

        $allitems = $allitems->get()->groupBy('cuisine_id');
        $featuredItems = $featuredItems->get();


        return view('restaurants.show', compact('restaurant', 'allitems', 'featuredItems', 'searchitems'));

    }
}
