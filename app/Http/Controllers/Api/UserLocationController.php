<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserLocationController extends Controller
{
    public function store()
    {
        if(!request()->has('lat') || !request()->has('lng') || !request()->has('address'))
        {
            return response(['error', 500]);
        }

        session(['lat' => request('lat'), 'lng' => request('lng'), 'address' => request('address')]);


        return response(['success'], 200);
    }
}
