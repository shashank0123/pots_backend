<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {

        $restaurants = [];

        if(request()->has('query'))
        {
            if(request('query') == '')
            {
                return redirect('/search');
            }
            $restaurants = Restaurant::where('name', 'LIKE', '%' . request('query') . '%')->paginate(12);
        }

        return view('restaurants.search', compact('restaurants'));

    }
}
