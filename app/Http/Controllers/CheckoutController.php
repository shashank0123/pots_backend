<?php

namespace App\Http\Controllers;

use App\Models\MenuItem;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index()
    {

        $items = \Cart::getContent();
        if(count($items))
        {
            $menuItem = MenuItem::findOrFail($items->first()->attributes->item_id);
            $restaurant = $menuItem->restaurant;

            $bill['subTotal'] = \Cart::getSubTotal();
            $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
            $bill['deliveryCharges'] = 30;
            $condition = \Cart::getCondition('discount');
            if(isset($condition))
            {
              $bill['discount'] = $condition->getCalculatedValue(\Cart::getTotal());
            } else {
                 $bill['discount'] = null;
            }
            $bill['total'] = \Cart::getTotal() + $bill['gst'] + $bill['deliveryCharges'];

            return view('checkout.index', compact('restaurant', 'bill'));
        } else {
            return view('checkout.none');
        }

    }
}
