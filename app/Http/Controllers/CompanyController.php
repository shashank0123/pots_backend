<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function about()
    {
        return view('company.about');
    }

    public function contact()
    {
        return view('company.contact');
    }

    public function careers()
    {
        return view('company.careers');
    }

    public function privacy()
    {
        return view('company.privacy');
    }

    public function team()
    {
        return view('company.team');
    }

    public function terms()
    {
        return view('company.terms');
    }
}
