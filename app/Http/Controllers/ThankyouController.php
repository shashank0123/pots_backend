<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class ThankyouController extends Controller
{
    public function index()
    {
        $order = Order::findOrFail(request('order'));

        return view('checkout.success', compact('order'));

    }
}
