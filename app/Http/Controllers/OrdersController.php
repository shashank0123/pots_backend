<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\MenuItem;
use Illuminate\Http\Request;
use App\Events\OrderStatusChanged;

class OrdersController extends Controller
{
    public function store()
    {
        $items = \Cart::getContent();

        if(count($items))
        {
            $menuItem = MenuItem::findOrFail($items->first()->attributes->item_id);
            $restaurant = $menuItem->restaurant;
            $order = new Order;

            $order->addCustomerDetails(request('address_id'));

            $order->addRestaurantDetails($restaurant);

            $order->delivery_alt_no = request('delivery_alt_no');

            $bill['subTotal'] = \Cart::getSubTotal();
            $bill['gst'] = round($bill['subTotal'] * (5/100), 2);
            $bill['deliveryCharges'] = 30;
            $condition = \Cart::getCondition('discount');
            if(isset($condition))
            {
              $bill['discount'] = $condition->getCalculatedValue(\Cart::getTotal());
            } else {
                 $bill['discount'] = null;
            }
            $bill['total'] = \Cart::getTotal() + $bill['gst'] + $bill['deliveryCharges'];

            $order->addBillingDetails($bill);

            $order->payment_mode = request('payment_mode');

            $order->suggestions = request('suggestions');

            $order->save();

            $order->storeItems($items);

            if($order->payment_mode == 0)
            {
                \Cart::clear();
                 \Cart::clearCartConditions();
                 $message = 'Thanks for ordering with Pots. Your order no: '. $order->id . ' and bill amount : Rs. '. $order->total .'/- . We are waiting for restaurant confirmation and will update you soon.';
                 $response = sendSMS($order->customer_phone, $message);
                 $messageToRest = 'You have received a new order of Rs. '. $order->total .'/-. Order Invoice : https://foodoor.in/orders/'. $order->id  .'/invoice';
                 sendSMS($order->restaurant->contact_phone, $messageToRest);
                 return response(['redirect' => '/thankyou?order=' . $order->id], 200);

            } else {
               $order->update(['status' => -2]);
               return response(['redirect' => '/pay?order=' . $order->id], 200);
            }

        } else {
            return response(['error' => 'Empty cart'], 500);
        }
    }

    public function invoice(Order $order)
    {
        $invoice = \PDF::loadView('orders.invoice', compact('order'));

        return $invoice->download('invoice.pdf');
    }

    public function show(Order $order)
    {
        return view('orders.show', compact('order'));
    }

     public function confirm(Order $order)
    {
        $order->status = 1;
        $order->save();

         event(new OrderStatusChanged($order));

        return back();
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ready(Order $order)
    {
        $order->status = 2;
        $order->save();

         event(new OrderStatusChanged($order));

        return back();
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function picked(Order $order)
    {
        $order->status = 3;
        $order->save();

         event(new OrderStatusChanged($order));

        return back();
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delivered(Order $order)
    {
        $order->status = 4;
        $order->save();

         event(new OrderStatusChanged($order));


        return back();
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Order $order)
    {
        $order->status = -1;
        $order->save();

         event(new OrderStatusChanged($order));


        return back();
    }

}
