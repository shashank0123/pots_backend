<?php

namespace App\Http\Controllers;

use App\Models\HomeBanner;
use App\Models\Restaurant;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        if(session()->has('lat') && session()->has('lng') && session()->has('address'))
        {
            return redirect('/restaurants/explore');
        }

        $banners = HomeBanner::all();
        $featured = Restaurant::where('is_featured', 1)->orderBy('name', 'ASC')->get();
        return view('welcome', compact('banners', 'featured'));
    }
}
