<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function phone()
    {
        $otp = mt_rand(10000, 99999);

        $message = 'Your Pots verification OTP is ' . $otp;

        $phone = request('phone');

        $user = User::where('phone', $phone)->first();

        if(!$user)
        {
            return back()->withInput(request()->only('phone'))
                ->withErrors(['phone' => 'No account exists with this mobile number!']);
        }

        $user->otp = $otp;
        $user->save();

        $response = sendSMS($phone, $message);

        flash('Otp sent successfully!')->success();

        return redirect('/password/change?phone=' . $phone);
    }
}
