<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function change()
    {
        return view('auth.passwords.reset');
    }

    public function update()
    {
        request()->validate([
            'otp' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        $phone = request('phone');
        $user = User::where('phone', $phone)->first();

        if(request('otp') == $user->otp)
        {
            flash('Your password was updated successfully!')->success();
            auth()->login($user);
            $user->otp = null;
            $user->save();
            return redirect('/');

        } else {
              return back()
                    ->withErrors(['otp' => 'Incorrect otp']);;
        }
    }


}
