<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RestaurantRequestController extends Controller
{
    public function store()
    {
         \Mail::to('pots.ranchi@gmail.com')->send(new \App\Mail\NewRestaurantEnquiry(request('name'), request('phone'), request('email'), request('address'), request('message')));

        flash('We have received your details. We will get back to you soon!')->success();

         return back();
    }

     public function bulk()
    {
         \Mail::to('pots.ranchi@gmail.com')->send(new \App\Mail\NewBulkOrder(request('name'), request('phone'), request('email'), request('address'), request('message')));

        flash('We have received your details. We will get back to you soon!')->success();

         return back();
    }
}
