<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name');
            $table->string('customer_email')->nullable();
            $table->string('customer_phone');
            $table->text('delivery_location');
            $table->float('delivery_latitude');
            $table->float('delivery_longitude');
            $table->string('delivery_door_no')->nullable();
            $table->string('delivery_road_name')->nullable();
            $table->string('delivery_landmark')->nullable();
            $table->string('delivery_alt_no')->nullable();
            $table->integer('user_id')->default(0);
            $table->string('restaurant_name');
            $table->text('restaurant_location');
            $table->float('restaurant_latitude');
            $table->float('restaurant_longitude');
            $table->integer('restaurant_id')->default(0);
            $table->float('subtotal');
            $table->float('tax');
            $table->float('delivery_charges');
            $table->float('discount')->default(0);
            $table->float('total');
            $table->integer('delivery_boy_id')->default(0);
            $table->string('coupon_code')->nullable();
            $table->integer('status')->default(0);
            $table->integer('payment_mode')->default(0);
            $table->integer('payment_status')->default(0);
            $table->text('suggestions')->nullable();
            $table->string('delivered_at')->nullable();
            $table->integer('cancelled_by')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->integer('is_offline')->default(0);
            $table->float('offline_bill_amount')->nullable();
            $table->text('offline_bill_image')->nullable();
            $table->float('commission_earned')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
