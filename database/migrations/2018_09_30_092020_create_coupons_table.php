<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->double('min_order');
            $table->text('promo_text')->nullable();
            $table->integer('discount_type')->default(0);
            $table->double('discount')->nullable();
            $table->integer('restaurant_id')->default(0);
            $table->integer('applied_to_all')->default(1);
            $table->string('valid_from');
            $table->string('valid_through');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
