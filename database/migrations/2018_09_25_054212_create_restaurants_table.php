<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('area');
            $table->text('location');
            $table->double('latitude');
            $table->double('longitude');
            $table->string('logo')->nullable();
            $table->string('contact_name');
            $table->string('contact_phone');
            $table->string('contact_email')->nullable();
            $table->string('username');
            $table->string('password');
            $table->string('website')->nullable();
            $table->integer('city_id');
            $table->integer('is_featured')->default(0);
            $table->integer('delivery_time')->default(0);
            $table->integer('is_pure_veg')->default(0);
            $table->float('cost_for_two');
            $table->integer('gst_applicable')->default(0);
            $table->integer('24_hrs')->default(0);
            $table->string('bank_name')->nullable();
            $table->string('bank_ifsc')->nullable();
            $table->string('bank_acc_no')->nullable();
            $table->string('bank_acc_name')->nullable();
            $table->integer('bank_acc_type')->default(1);
            $table->text('timings')->nullable();
            $table->text('days_open')->nullable();
            $table->text('promo_text')->nullable();
            $table->text('min_order')->nullable();
            $table->float('commission')->default(20)->nullable();
            $table->integer('discount_type')->default(0);
            $table->double('discount')->nullable();
            $table->string('valid_from')->nullable();
            $table->string('valid_through')->nullable();
            $table->timestamps();
        });

         Schema::create('cuisine_restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id')->unsigned()->index();
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
            $table->integer('cuisine_id')->unsigned()->index();
            $table->foreign('cuisine_id')->references('id')->on('cuisines')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
        Schema::dropIfExists('cuisine_restaurant');
    }
}
